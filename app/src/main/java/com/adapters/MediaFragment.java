package com.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import java.io.IOException;
import java.util.ArrayList;


public class MediaFragment extends Fragment {
    int IMAGE = 0;
    int AUDIO = 1;
    int VIDEO = 2;

    public final int REQUEST_CODE_GALLARY = 50;
    public final int REQUEST_CODE_CAMERA = 51;
    public final int REQUEST_CODE_AUDIO = 52;
    public final int REQUEST_CODE_PICK_VIDEO = 53;
    public Uri imageUri;
    MediaListener mediaListener;
    ImageListener imageListener;

    public void getImage(final ImageListener imageListener) {
        this.imageListener = imageListener;
//  add this dependency compile 'gun0912.ted:tedpermission:1.0.0'
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, new String[]{"Camera", "Gallery"});
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    getImageFromCamera(imageListener);
                } else if (which == 1) {
                    getImageFromGallery(imageListener);
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setTitle("Choose a picture");
        dialog.show();
    }

    public static Bitmap getBitmapFromUri(Context ctx, Uri imageUri) {
        try {
            return MediaStore.Images.Media.getBitmap(ctx.getContentResolver(), imageUri);
        } catch (IOException e) {
            e.printStackTrace();

        }
        return null;
    }

    public void getImageFromCamera(ImageListener imageListener) {
        this.imageListener = imageListener;
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA);
    }

    public void getImageFromGallery(ImageListener imageListener) {
        this.imageListener = imageListener;
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_GALLARY);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
                imageListener.onGetBitMap((Bitmap) data.getExtras().get("data"));
                break;
            case REQUEST_CODE_GALLARY:
                imageListener.onGetBitMap(getBitmapFromUri(getActivity(), data.getData()));
                break;
            case REQUEST_CODE_AUDIO:
                Uri uri = data.getData();
                mediaListener.onGetUri(uri, AUDIO);
                break;
            case REQUEST_CODE_PICK_VIDEO:
                mediaListener.onGetUri(data.getData(), VIDEO);
                break;
        }

    }

    public interface MediaListener {
        void onGetUri(Uri uri, int MediaType);
    }

    public interface ImageListener {
        void onGetBitMap(Bitmap bitmap);
    }

}
