package com.prajaval.core;

import java.io.Serializable;

import com.json.classes.JSONObject;

public class ParticipateItem implements Serializable {

	int iparticiapteID;
	int iContestID;
	String strTitle;
	String strDescription;
	String strYLink;
	String strThumbURL;
	String strImage;
	int iVote;
	int iUserID;
	String strUsername;
	String strUserImage;

	public int getiUserID() {
		return iUserID;
	}

	public void setiUserID(int iUserID) {
		this.iUserID = iUserID;
	}

	public String getStrUserImage() {
		return strUserImage;
	}

	public void setStrUserImage(String strimg) {
		this.strUserImage = strimg;
	}

	public int getiVote() {
		return iVote;
	}

	public String getStrUsername() {
		return strUsername;
	}

	public int getiparticiapteID() {
		return iparticiapteID;
	}

	public void setiparticiapteID(int iparticiapteID) {
		this.iparticiapteID = iparticiapteID;
	}

	// public int getiUserID() {
	// return iUserID;
	// }

	// public void setiUserID(int iUserID) {
	// this.iUserID = iUserID;
	// }

	public int getiContestID() {
		return iContestID;
	}

	public void setiContestID(int iContestID) {
		this.iContestID = iContestID;
	}

	public String getStrTitle() {
		return strTitle;
	}

	public void setStrTitle(String strTitle) {
		this.strTitle = strTitle;
	}

	public String getStrDescription() {
		return strDescription;
	}

	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}

	// public String getStrURL() {
	// return strURL;
	// }

	// public void setStrURL(String strURL) {
	// this.strURL = strURL;
	// }

	public String getStrThumbURL() {
		return strThumbURL;
	}

	public void setStrThumbURL(String strThumbURL) {
		this.strThumbURL = strThumbURL;
	}

	public String getStrYLink() {
		return strYLink;
	}

	public void setiVote(int iVote) {
		this.iVote = iVote;
	}

	public void setStrYLink(String strYLink) {
		this.strYLink = strYLink;
	}

	public String getStrImage() {
		return strImage;
	}

	public void setStrUsername(String strUsername) {
		this.strUsername = strUsername;
	}

	public void setStrImage(String strImage) {
		this.strImage = strImage;
	}

	public static ParticipateItem getParticipateItem(JSONObject object) {

		ParticipateItem item = new ParticipateItem();

		try {
			if (object != null) {

				item.setiparticiapteID(object.trygetInt("id"));
				item.setiUserID(object.trygetInt("userid"));
				item.setiContestID(object.trygetInt("contest_id"));
				item.setStrTitle(object.trygetString("title"));
				item.setStrDescription(object.trygetString("description"));
				item.setStrYLink(object.trygetString("y_link"));
				item.setStrThumbURL(object.trygetString("thumb_url"));
				item.setStrImage(object.trygetString("img"));
				item.setStrUsername(object.trygetString("username"));
				item.setiVote(object.trygetInt("vote"));
				item.setStrUserImage(object.trygetString("u_img"));

			}

		} catch (Exception e) {

		}

		return item;
	}
}
