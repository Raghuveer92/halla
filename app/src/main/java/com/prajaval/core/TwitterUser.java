package com.prajaval.core;

import com.json.classes.JSONObject;

public class TwitterUser {

	public int iTwitterID;
	public String strTwitterDisplayName;
	public String strTwitterScreenName;
	public String strTwitterLocation;
	public String strTwitterDescription;
	public boolean bisContributorsEnabled;
	public String strTwitterProfileImageUrl;
	public String strTwitterprofileImageUrlHttps;
	public String strTwitterurl;

	public int getiTwitterID() {
		return iTwitterID;
	}

	public void setiTwitterID(int iTwitterID) {
		this.iTwitterID = iTwitterID;
	}

	public String getStrTwitterurl() {
		return strTwitterurl;
	}

	public void setStrTwitterurl(String strTwitterurl) {
		this.strTwitterurl = strTwitterurl;
	}

	public String getStrTwitterDescription() {
		return strTwitterDescription;
	}

	public void setStrTwitterDescription(String strTwitterDescription) {
		this.strTwitterDescription = strTwitterDescription;
	}

	public String getStrTwitterDisplayName() {
		return strTwitterDisplayName;
	}

	public void setStrTwitterDisplayName(String strTwitterDisplayName) {
		this.strTwitterDisplayName = strTwitterDisplayName;
	}

	public String getStrTwitterLocation() {
		return strTwitterLocation;
	}

	public void setStrTwitterLocation(String strTwitterLocation) {
		this.strTwitterLocation = strTwitterLocation;
	}

	public String getStrTwitterProfileImageUrl() {
		return strTwitterProfileImageUrl;
	}

	public void setStrTwitterProfileImageUrl(String strTwitterProfileImageUrl) {
		this.strTwitterProfileImageUrl = strTwitterProfileImageUrl;
	}

	public String getStrTwitterprofileImageUrlHttps() {
		return strTwitterprofileImageUrlHttps;
	}

	public void setStrTwitterprofileImageUrlHttps(
			String strTwitterprofileImageUrlHttps) {
		this.strTwitterprofileImageUrlHttps = strTwitterprofileImageUrlHttps;
	}

	public String getStrTwitterScreenName() {
		return strTwitterScreenName;
	}

	public void setStrTwitterScreenName(String strTwitterScreenName) {
		this.strTwitterScreenName = strTwitterScreenName;
	}

	public static TwitterUser GetTwitterUser(JSONObject jObject) {

		TwitterUser twitterUser = new TwitterUser();
		try {
			if (jObject != null) {

				twitterUser.setiTwitterID(jObject.trygetInt("id"));

				twitterUser.setStrTwitterDescription(jObject
						.trygetString("description"));

				twitterUser.setStrTwitterDisplayName(jObject
						.trygetString("name"));

				twitterUser.setStrTwitterLocation(jObject
						.trygetString("location"));
				twitterUser.setStrTwitterScreenName(jObject
						.trygetString("screenName"));

				twitterUser.setStrTwitterProfileImageUrl(jObject
						.trygetString("profileImageUrl"));

				twitterUser.setStrTwitterprofileImageUrlHttps(jObject
						.trygetString("profileImageUrlHttps"));
				twitterUser.setStrTwitterurl(jObject.trygetString("url"));

			}
		} catch (Exception e) {

		}
		return twitterUser;
	}
}
