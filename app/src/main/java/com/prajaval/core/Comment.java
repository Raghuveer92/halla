package com.prajaval.core;

import com.json.classes.JSONObject;

public class Comment {

	int iEntryID; // entry_id
	int iUserID; // user_id

	String strComment;// comment
	String strUserName;// username
	String strUserimage;

	public String getStrUserimage() {
		return strUserimage;
	}

	public void setStrUserimage(String strUserimage) {
		this.strUserimage = strUserimage;
	}

	public int getiEntryID() {
		return iEntryID;
	}

	public int getiUserID() {
		return iUserID;
	}

	public String getStrComment() {
		return strComment;
	}

	public String getStrUserName() {
		return strUserName;
	}

	public void setiEntryID(int iEntryID) {
		this.iEntryID = iEntryID;
	}

	public void setiUserID(int iUserID) {
		this.iUserID = iUserID;
	}

	public void setStrComment(String strComment) {
		this.strComment = strComment;
	}

	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}

	public static Comment GetComment(JSONObject object) {
		Comment comment = new Comment();
		try {
			if (object != null) {

				comment.setiEntryID(object.trygetInt("entry_id"));
				comment.setiUserID(object.trygetInt("user_id"));
				comment.setStrUserName(object.trygetString("username"));
				comment.setStrComment(object.trygetString("comment"));
				comment.setStrUserimage(object.trygetString("img"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return comment;

	}

	public static Comment Getcomment(String jsonobject) {
		Comment comment = new Comment();
		try {
			JSONObject object = new JSONObject(jsonobject);
			if (object != null) {

				comment.setiEntryID(object.trygetInt("entry_id"));
				comment.setiUserID(object.trygetInt("user_id"));

				comment.setStrComment(object.trygetString("description"));

			}
		} catch (Exception e) {

		}
		return comment;

	}
}
