package com.prajaval.core;

import java.io.Serializable;
import java.util.List;

import com.json.classes.JSONArray;
import com.json.classes.JSONObject;

public class User implements Serializable {

	public int iUserID;
	public int iSocial_ID;
	public String socialName;
	public String userName;
	public String email;
	public String userRegistered;
	public String StrUser_location;
	public String StrUser_status;
	public String StrUser_type;
	public String StrUser_image;
	public String StrPoints;
	public List<Entry> entries;
	String password;

	public int getiSocial_ID() {
		return iSocial_ID;
	}

	public String getSocialName() {
		return socialName;
	}

	public void setSocialName(String socialName) {
		this.socialName = socialName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStrPoints() {
		return StrPoints;
	}

	public void setStrPoints(String strPoints) {
		StrPoints = strPoints;
	}

	public List<Entry> getEntries() {
		return entries;
	}

	public void setEntries(List<Entry> entries) {
		this.entries = entries;
	}

	public String getUserName() {
		return userName;
	}

	public int getSocial_ID() {
		return iSocial_ID;
	}

	public String getEmail() {
		return email;
	}

	public String getStrsocial_UserName() {
		return socialName;
	}

	public String getStrUser_image() {
		return StrUser_image;
	}

	public String getStrUser_location() {
		return StrUser_location;
	}

	public String getUserRegistered() {
		return userRegistered;
	}

	public String getStrUser_status() {
		return StrUser_status;
	}

	public String getStrUser_type() {
		return StrUser_type;
	}

	public int getiUserID() {
		return iUserID;
	}

	public void setiSocial_ID(int social_ID) {
		this.iSocial_ID = social_ID;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setStrsocial_UserName(String strsocial_UserName) {
		socialName = strsocial_UserName;
	}

	public void setStrUser_image(String strUser_image) {
		StrUser_image = strUser_image;
	}

	public void setStrUser_location(String strUser_location) {
		StrUser_location = strUser_location;
	}

	public void setUserRegistered(String userRegistered) {
		this.userRegistered = userRegistered;
	}

	public void setStrUser_status(String strUser_status) {
		StrUser_status = strUser_status;
	}

	public void setStrUser_type(String strUser_type) {
		StrUser_type = strUser_type;
	}

	public void setiUserID(int userID) {
		iUserID = userID;
	}

	public static User GetUser(JSONObject jObject, JSONArray array) {

		User user = new User();

		try {
			if (jObject != null) {

				user.setiUserID(jObject.trygetInt("id"));
				user.setStrUser_location(jObject.trygetString("location"));
				user.setEmail(jObject.trygetString("email"));
				user.setiSocial_ID(jObject.trygetInt("social_id"));
				user.setStrsocial_UserName(jObject
						.trygetString("social_username"));
				user.setStrUser_image(jObject.trygetString("pic"));
				user.setStrPoints(jObject.trygetString("points"));
				user.setUserName(jObject.trygetString("name"));

				if (array != null) {
					user.setEntries(Entry.GetEntryList(array));
				}

			}
		} catch (Exception e) {

		}

		return user;
	}
}
