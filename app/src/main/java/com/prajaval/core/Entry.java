package com.prajaval.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.json.classes.JSONArray;
import com.json.classes.JSONException;

public class Entry implements Serializable {

	String title;
	String thumb_url;
	String votes;
	String winner;

	public String getThumb_url() {
		return thumb_url;
	}

	public void setThumb_url(String thumb_url) {
		this.thumb_url = thumb_url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVotes() {
		return votes;
	}

	public void setVotes(String votes) {
		this.votes = votes;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

	public static List<Entry> GetEntryList(JSONArray array) {

		List<Entry> entries = null;
		if (array != null) {
			for (int i = 0; i < array.length(); i++) {
				try {
					Entry entry = new Entry();
					entry.setTitle(array.getJSONObject(i).trygetString("title"));
					entry.setThumb_url(array.getJSONObject(i).trygetString(
							"thumb_url"));

					entry.setVotes(array.getJSONObject(i).trygetString("votes"));

					if (!array.getJSONObject(i).isNull("winner")) {
						entry.setWinner("yes");
					} else {
						entry.setWinner("");
					}
					if (entries == null) {
						entries = new ArrayList<Entry>();
					}
					entries.add(entry);
				} catch (JSONException e) {

					e.printStackTrace();
				}
			}
		}
		return entries;
	}

}
