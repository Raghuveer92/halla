package com.prajaval.core;

import java.io.Serializable;

import com.json.classes.JSONObject;

public class Contest implements Serializable {

	private int iContestId = 0;
	private int int_ContestNoofWinner = 0;
	private int iContestEntries = 0;
	private int iContestVote = 0;
	private int iflag;

	private String str_ContestTitle;
	private String str_ContestDesc;
	private String str_ContestImage;
	private String str_ContestThumbImage;
	private String str_ContestStartDate;
	private String str_ContestEndDate;
	private String str_ContestStatus;
	private String str_ContestPrize;
	private String str_ContestSummary;
	private String str_ContestHint;
	private String str_ContestType;
	private String str_ContestImageCredit;
	private String str_ContestImageCreditUrl;
	private String str_ContestSponsorName;
	private String str_ContestSponsorLogo;
	private String str_ContestSponsorUrl;

	private String str_ContestWinner;

	private String str_ContestNoOfEntries;

	private String str_Contestlocation;

	// get Method

	public String getStr_Contestlocation() {
		return str_Contestlocation;
	}

	public void setStr_Contestlocation(String str_Contestlocation) {
		this.str_Contestlocation = str_Contestlocation;
	}

	public String getStr_ContestThumbImage() {
		return str_ContestThumbImage;
	}

	public void setContestThumbImage(String str_ContestThumbImage) {
		this.str_ContestThumbImage = str_ContestThumbImage;
	}

	public int getIflag() {
		return iflag;
	}

	public int getiContestEntries() {
		return iContestEntries;
	}

	public int getiContestVote() {
		return iContestVote;
	}

	public String getStr_ContestWinner() {
		return str_ContestWinner;
	}

	public int getiContestId() {
		return iContestId;
	}

	public String getStr_ContestTitle() {
		return str_ContestTitle;
	}

	public String getStr_ContestDesc() {
		return str_ContestDesc;
	}

	public String getStr_ContestImage() {
		return str_ContestImage;
	}

	public String getStr_ContestStartDate() {
		return str_ContestStartDate;
	}

	public String getStr_ContestEndDate() {
		return str_ContestEndDate;
	}

	public String getStr_ContestStatus() {
		return str_ContestStatus;
	}

	public String getStr_ContestPrize() {
		return str_ContestPrize;
	}

	public String getStr_ContestSummary() {
		return str_ContestSummary;
	}

	public String getStr_ContestHint() {
		return str_ContestHint;
	}

	public String getStr_ContestType() {
		return str_ContestType;
	}

	public String getStr_ContestImageCredit() {
		return str_ContestImageCredit;
	}

	public String getStr_ContestImageCreditUrl() {
		return str_ContestImageCreditUrl;
	}

	public String getStr_ContestSponsorName() {
		return str_ContestSponsorName;
	}

	public String getStr_ContestSponsorLogo() {
		return str_ContestSponsorLogo;
	}

	public String getStr_ContestSponsorUrl() {
		return str_ContestSponsorUrl;
	}

	public int getInt_ContestNoofWinner() {
		return int_ContestNoofWinner;
	}

	// set method
	public void setiContestEntries(int iContestEntries) {
		this.iContestEntries = iContestEntries;
	}

	public void setiContestVote(int iContestVote) {
		this.iContestVote = iContestVote;
	}

	public void setStr_ContestWinner(String str_Winner) {
		this.str_ContestWinner = str_Winner;
	}

	public void setInt_ContestNoofWinner(int int_ContestNoofWinner) {
		this.int_ContestNoofWinner = int_ContestNoofWinner;
	}

	public void setStr_ContestDesc(String str_ContestDesc) {
		this.str_ContestDesc = str_ContestDesc;
	}

	public void setStr_ContestEndDate(String str_ContestEndDate) {
		this.str_ContestEndDate = str_ContestEndDate;
	}

	public void setStr_ContestHint(String str_ContestHint) {
		this.str_ContestHint = str_ContestHint;
	}

	public void setStr_ContestImage(String str_ContestImage) {
		this.str_ContestImage = str_ContestImage;
	}

	public void setStr_ContestImageCredit(String str_ContestImageCredit) {
		this.str_ContestImageCredit = str_ContestImageCredit;
	}

	public void setStr_ContestImageCreditUrl(String str_ContestImageCreditUrl) {
		this.str_ContestImageCreditUrl = str_ContestImageCreditUrl;
	}

	public void setStr_ContestPrize(String str_ContestPrize) {
		this.str_ContestPrize = str_ContestPrize;
	}

	public void setStr_ContestSponsorLogo(String str_ContestSponsorLogo) {
		this.str_ContestSponsorLogo = str_ContestSponsorLogo;
	}

	public void setStr_ContestSponsorName(String str_ContestSponsorName) {
		this.str_ContestSponsorName = str_ContestSponsorName;
	}

	public void setStr_ContestSponsorUrl(String str_ContestSponsorUrl) {
		this.str_ContestSponsorUrl = str_ContestSponsorUrl;
	}

	public void setStr_ContestStartDate(String str_ContestStartDate) {
		this.str_ContestStartDate = str_ContestStartDate;
	}

	public void setStr_ContestStatus(String str_ContestStatus) {
		this.str_ContestStatus = str_ContestStatus;
	}

	public void setStr_ContestSummary(String str_ContestSummary) {
		this.str_ContestSummary = str_ContestSummary;
	}

	public void setStr_ContestTitle(String str_ContestTitle) {
		this.str_ContestTitle = str_ContestTitle;
	}

	public void setStr_ContestType(String str_ContestType) {
		this.str_ContestType = str_ContestType;
	}

	public void setiContestId(int iContestId) {
		this.iContestId = iContestId;
	}

	public String getStr_ContestNoOfEntries() {
		return str_ContestNoOfEntries;
	}

	public void setStr_ContestNoOfEntries(String str_ContestNoOfEntries) {
		this.str_ContestNoOfEntries = str_ContestNoOfEntries;
	}

	public void setIflag(int iflag) {
		this.iflag = iflag;
	}

	public static Contest GetContest(JSONObject jObject) {

		Contest contest = new Contest();

		try {
			if (jObject != null) {

				contest.setiContestId(jObject.trygetInt("id"));
				contest.setStr_ContestTitle(jObject.trygetString("title"));
				contest.setStr_ContestDesc(jObject.trygetString("description"));
				contest.setStr_ContestImage(jObject.trygetString("image"));
				contest.setStr_ContestStartDate(jObject.trygetString("s_date"));
				contest.setStr_ContestEndDate(jObject.trygetString("e_date"));
				contest.setStr_ContestStatus(jObject.trygetString("status"));
				contest.setStr_ContestPrize(jObject.trygetString("prize"));
				contest.setStr_ContestSummary(jObject.trygetString("summary"));
				contest.setStr_ContestHint(jObject.trygetString("hint"));
				contest.setStr_ContestType(jObject.trygetString("type"));
				contest.setStr_ContestImageCredit(jObject
						.trygetString("img_credit"));
				contest.setStr_ContestImageCreditUrl(jObject
						.trygetString("img_credit_url"));
				contest.setStr_ContestSponsorName(jObject
						.trygetString("s_name"));
				contest.setStr_ContestSponsorLogo(jObject
						.trygetString("s_logo"));
				contest.setStr_ContestSponsorUrl(jObject.trygetString("s_url"));
				contest.setInt_ContestNoofWinner(jObject.trygetInt("no_winner"));
				contest.setiContestEntries(jObject.trygetInt("entries"));
				contest.setiContestVote(jObject.trygetInt("vote"));

				contest.setIflag((jObject.trygetInt("flag")));
				contest.setContestThumbImage(jObject.trygetString("thumb_img"));

				contest.setStr_Contestlocation(jObject.trygetString("location"));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return contest;
	}
}
