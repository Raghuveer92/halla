package com.prajaval.core;

import java.io.Serializable;

import com.json.classes.JSONObject;

@SuppressWarnings("serial")
public class ContestWinnerUser implements Serializable {

	int iWinnerID;
	int iContestID;
	String strTitle;
	String strDesc;
	String strImage;
	String strVote;
	int iWinnerUserID;
	String strUsername;
	String strUser_img;
	String strY_link;
	String strType;
	String strWinner;
	String img;

	public String getUserImage() {
		return img;
	}

	public void setUserImage(String img) {
		this.img = img;
	}

	// get
	public String getStrWinner() {
		return strWinner;
	}

	public String getStrType() {
		return strType;
	}

	public int getiWinnerID() {
		return iWinnerID;
	}

	public int getiWinnerUserID() {
		return iWinnerUserID;
	}

	public int getiContestID() {
		return iContestID;
	}

	public String getStrDesc() {
		return strDesc;
	}

	public String getStrImage() {
		return strImage;
	}

	public String getStrTitle() {
		return strTitle;
	}

	public String getStrUsername() {
		return strUsername;
	}

	public String getStrVote() {
		return strVote;
	}

	public String getStrY_link() {
		return strY_link;
	}

	// set
	public void setStrWinner(String strWinner) {
		this.strWinner = strWinner;
	}

	public void setStrType(String strType) {
		this.strType = strType;
	}

	public void setiContestID(int iContestID) {
		this.iContestID = iContestID;
	}

	public void setiWinnerID(int iWinnerID) {
		this.iWinnerID = iWinnerID;
	}

	public void setStrDesc(String strDesc) {
		this.strDesc = strDesc;
	}

	public void setiWinnerUserID(int iWinnerUserID) {
		this.iWinnerUserID = iWinnerUserID;
	}

	public void setStrImage(String strImage) {
		this.strImage = strImage;
	}

	public void setStrTitle(String strTitle) {
		this.strTitle = strTitle;
	}

	public void setStrUser_img(String strUser_img) {
		this.strUser_img = strUser_img;
	}

	public void setStrUsername(String strUsername) {
		this.strUsername = strUsername;
	}

	public void setStrVote(String strVote) {
		this.strVote = strVote;
	}

	public void setStrY_link(String strY_link) {
		this.strY_link = strY_link;
	}

	public static ContestWinnerUser GetWinnerUser(JSONObject object) {
		ContestWinnerUser user = new ContestWinnerUser();
		try {

			if (object != null) {
				user.setiWinnerID(object.trygetInt("id"));
				user.setiContestID(object.trygetInt("contest_id"));
				user.setStrTitle(object.trygetString("title"));
				user.setStrDesc(object.trygetString("description"));
				user.setStrImage(object.trygetString("img"));
				user.setStrVote(object.trygetString("vote"));
				user.setiWinnerUserID(object.trygetInt("userid"));
				user.setStrUsername(object.trygetString("username"));
				user.setStrUser_img(object.trygetString("u_img"));
				user.setStrType(object.trygetString("type"));
				user.setStrY_link(object.trygetString("y_link"));
				user.setStrWinner(object.trygetString("winner"));
				user.setStrUser_img(object.trygetString("user_img"));
			}
		} catch (Exception e) {

		}
		return user;
	}
}
