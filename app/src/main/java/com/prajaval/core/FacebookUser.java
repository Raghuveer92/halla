package com.prajaval.core;

import com.json.classes.JSONObject;

public class FacebookUser {

	public String id;
	public String name;
	public String fbusername;
	public String first_name;
	public String last_name;
	public String picture;
	public String gender;
	public String birthday;
	public String Email;

	public FacebookUser() {
	}

	public FacebookUser Get(String json) {
		try {
			if (json.toString() != "") {
				JSONObject objJSONObject = new JSONObject(json);

				if (objJSONObject != null) {
					if (objJSONObject.has("id")) {
						this.id = objJSONObject.get("id").toString();
					}
					if (objJSONObject.has("name")) {
						this.name = objJSONObject.get("name").toString();
					}
					if (objJSONObject.has("first_name")) {
						this.first_name = objJSONObject.get("first_name")
								.toString();
					}
					if (objJSONObject.has("last_name")) {
						this.last_name = objJSONObject.get("last_name")
								.toString();
					}
					if (objJSONObject.has("picture")) {
						try {
							JSONObject object = objJSONObject
									.getJSONObject("picture");
							JSONObject objectdata = object
									.getJSONObject("data");
							this.picture = objectdata.trygetString("url");

						} catch (Exception e) {
							e.printStackTrace();
						}

						// this.picture = object.trygetString("url");

					}
					if (objJSONObject.has("gender")) {
						this.gender = objJSONObject.get("gender").toString();
					}
					if (objJSONObject.has("birthday")) {
						this.birthday = objJSONObject.get("birthday")
								.toString();
					}
					if (objJSONObject.has("email")) {
						this.Email = objJSONObject.get("email").toString();
					}
					if (objJSONObject.has("username")) {
						this.fbusername = objJSONObject.get("username")
								.toString();
					}
					return this;
				}
			}
			return null;
		} catch (Exception ex) {
			ex.toString();
		}
		return null;
	}
}
