package com.prajaval.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.prajaval.core.User;
import com.prajaval.database.HallaDataSource;

public class AsyncInserUserDB extends AsyncTask<Void, Void, Void> {

	HallaDataSource database;
	Context context;
	User user;

	public AsyncInserUserDB(Context activity, User user) {
		this.context = activity;
		this.user = user;
	}

	@Override
	protected Void doInBackground(Void... params) {

		database = new HallaDataSource(context);
		database.open();
		long insertRowID = database.insertUser(user);
		Log.d("DB User insert", " " + insertRowID);
		database.close();

		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
	}

}
