package com.prajaval.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.json.classes.JSONArray;
import com.json.classes.JSONException;
import com.json.classes.JSONObject;
import com.prajaval.core.User;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import common.prajaval.common.Common;

public class AsyncLogin extends AsyncTask<Void, Void, User> {

	String SocialID, SocialName, SocialUserName, SocialEmail,
			SocialProfileImage, SocialType, Sociallocation = "";
	double Lattitude, Longitude;
	IUser iUser;
	Context context;
	ProgressDialog dialog;
	GoogleCloudMessaging gcm;
	String regDeviceID = "";
	final String LOGIN = "http://halla.in/api/v1/login";

	public AsyncLogin(Context context, String socialid, String name,
			String socialusername, String email, String profileimage,
			String socialtype, String sociallocation, double lattitude,
			double longitude, IUser iUser) {
		this.context = context;
		SocialID = socialid;
		SocialName = name;
		SocialUserName = socialusername;
		SocialEmail = email;
		SocialProfileImage = profileimage;
		SocialType = socialtype;
		if (sociallocation == null) {
			Sociallocation = "";
		} else {
			Sociallocation = sociallocation;
		}
		Lattitude = lattitude;
		Longitude = longitude;
		this.iUser = iUser;
	}

	@Override
	protected void onPreExecute() {

		dialog = ProgressDialog.show(context, "wait...",
				"Register with halla...", true);
	}

	@Override
	protected User doInBackground(Void... params) {
		String responseUser = "";
		try {


			gcm = GoogleCloudMessaging.getInstance(context);

			for (int i = 0; i < 50; i++) {
				regDeviceID = gcm.register(Common.SENDERID);
				if (!regDeviceID.equals("")) {
					break;
				}
			}

			HttpPost httppost = new HttpPost(LOGIN);
			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			HttpClient httpClient = new DefaultHttpClient();

			try {
				StringBody social_id = new StringBody(SocialID);
				StringBody sname = new StringBody(SocialName);
				StringBody email = new StringBody(SocialEmail);
				StringBody loc = new StringBody(Sociallocation);
				StringBody screenName = new StringBody(SocialUserName);
				StringBody social_type = new StringBody(SocialType);
				StringBody social_pic = new StringBody(SocialProfileImage);
				StringBody lattitude = new StringBody(String.valueOf(Lattitude));
				StringBody longitude = new StringBody(String.valueOf(Longitude));
				StringBody deviceid = new StringBody(regDeviceID);

				entity.addPart("social_id", social_id);
				entity.addPart("name", sname);
				entity.addPart("Email", email);

				entity.addPart("location", loc);
				entity.addPart("social_username", screenName);
				entity.addPart("social_type", social_type);
				entity.addPart("social_pic", social_pic);
				entity.addPart("lat", lattitude);
				entity.addPart("long", longitude);
				entity.addPart("deviceid", deviceid);

			} catch (Exception e) {

			}

			httppost.setEntity(entity);

			try {
				HttpResponse response = httpClient.execute(httppost);
				BufferedReader in = null;
				try {
					// Log.d("status line ", "test " +
					// response.getStatusLine().toString());
					in = new BufferedReader(new InputStreamReader(response
							.getEntity().getContent(), "UTF-8"));
					StringBuffer sb = new StringBuffer("");
					String line = "";
					String NL = System.getProperty("line.separator");
					while ((line = in.readLine()) != null) {
						sb.append(line + NL);
					}
					responseUser = sb.toString();

					in.close();

				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

			} catch (ClientProtocolException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return create_user(responseUser);

	}

	@Override
	protected void onPostExecute(User result) {
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		iUser.ITaskCompleted(result);

	}

	private User create_user(String responseUser) {

		User user = null;
		JSONObject jObject;
		try {
			jObject = new JSONObject(responseUser);
			JSONArray jAData = null;

			JSONObject jData = null;

			if (jObject.has("data")) {
				jData = jObject.getJSONObject("data");
			}

			if (jObject.has("entries")) {
				jAData = jObject.getJSONArray("entries");
			}

			user = User.GetUser(jData, jAData);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return user;
	}

}
