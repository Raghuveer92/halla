package com.prajaval.twittershare;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.prajaval.core.ParticipateItem;
import common.prajaval.common.Common;

@SuppressLint("SetJavaScriptEnabled")
public class TwitterWebView extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.twittershare_activity);
		WebView webView = (WebView) findViewById(R.id.webView);

		Intent intent = getIntent();
		ParticipateItem participateItem = (ParticipateItem) intent
				.getSerializableExtra(Common.PARTICIPATE_ITEM);
		webView.getSettings().setJavaScriptEnabled(true);
		String tweetUrl = "https://twitter.com/intent/tweet?text=Vote for &url="
				+ "http://halla.in/contest_video.php?id="
				+ participateItem.getiparticiapteID();
		webView.setWebViewClient(new MyWebViewClient());
		webView.loadUrl(tweetUrl);

	}

	class MyWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			Toast.makeText(TwitterWebView.this, "Tweet successfully.",
					Toast.LENGTH_SHORT).show();
			finish();
			return true;

		}
	}
}
