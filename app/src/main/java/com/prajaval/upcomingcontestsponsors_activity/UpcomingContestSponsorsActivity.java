package com.prajaval.upcomingcontestsponsors_activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.core.Contest;
import com.prajaval.core.User;

import common.prajaval.common.Common;

public class UpcomingContestSponsorsActivity extends Activity {

	Contest contestUpcoming;

	ImageView img_upcontest_image;
	TextView txt_upcontest_title, txt_upcontest_type, txt_upcontest_desc;
	TextView txt_up_Prizes, txt_up_sponsors, txt_up_Rules;
	ImageView img_upcomingsponsor;
	private DisplayImageOptions options;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.upcomongcontest_activity);
		initialise_widgets();
		Intent intentUpcoming = getIntent();
		contestUpcoming = (Contest) intentUpcoming
				.getSerializableExtra(Common.SELECTED_CONTEST);

		options = new DisplayImageOptions.Builder()

		.resetViewBeforeLoading(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300, true, true, true))
				.build();

		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	private void initialise_widgets() {
		img_upcontest_image = (ImageView) findViewById(R.id.img_upcontest_image);
		img_upcomingsponsor = (ImageView) findViewById(R.id.img_upcomingsponsor);
		txt_upcontest_title = (TextView) findViewById(R.id.txt_upcontest_title);
		txt_upcontest_type = (TextView) findViewById(R.id.txt_upcontest_type);
		txt_upcontest_desc = (TextView) findViewById(R.id.txt_upcontest_desc);

		txt_up_Prizes = (TextView) findViewById(R.id.txt_up_Prizes);
		txt_up_sponsors = (TextView) findViewById(R.id.txt_up_sponsors);
		txt_up_Rules = (TextView) findViewById(R.id.txt_up_Rules);
	}

	@Override
	protected void onResume() {

		super.onResume();
		bind_data(contestUpcoming);
	}

	private void bind_data(Contest contest) {
		try {

			if (contest != null) {

				getActionBar().setTitle(contest.getStr_ContestTitle());

				txt_upcontest_title.setText(contest.getStr_ContestTitle());
				txt_upcontest_desc.setText(contest.getStr_ContestDesc());

				String type = "";
				if (!contest.getStr_ContestType().equals("image")) {
					type = "Video";
				} else {
					type = "Photo";
				}

				txt_upcontest_type.setText(type);
				txt_up_Prizes.setText(contest.getStr_ContestPrize());
				txt_up_sponsors.setText(contest.getStr_ContestSponsorName());

				String rule1 = getResources().getString(R.string.Rules_1);
				String rule2 = getResources().getString(R.string.Rules_2);
				String rule3 = getResources().getString(R.string.Rules_3);
				String rule4 = getResources().getString(R.string.Rules_4);
				String rule5 = getResources().getString(R.string.Rules_5);
				String rule6 = getResources().getString(R.string.Rules_6);
				String rule7 = getResources().getString(R.string.Rules_7);
				String rule8 = getResources().getString(R.string.Rules_8);
				String rule9 = getResources().getString(R.string.Rules_9);

				String string = rule1 + rule2 + rule3 + rule4 + rule5 + rule6
						+ rule7 + rule8 + rule9;

				txt_up_Rules.setText(string);

				ImageLoader.getInstance().displayImage(
						contest.getStr_ContestImage(), img_upcontest_image,
						options, new SimpleImageLoadingListener() {
							@Override
							public void onLoadingStarted(String imageUri,
									View view) {

							}

							@Override
							public void onLoadingFailed(String imageUri,
									View view, FailReason failReason) {
								img_upcontest_image
										.setImageDrawable(getResources()
												.getDrawable(
														R.drawable.img_not_found_large));

							}

							@Override
							public void onLoadingComplete(String imageUri,
									View view, Bitmap loadedImage) {

							}
						});

				ImageLoader.getInstance().displayImage(
						contest.getStr_ContestSponsorLogo(),
						img_upcomingsponsor, options,
						new SimpleImageLoadingListener() {
							@Override
							public void onLoadingStarted(String imageUri,
									View view) {

							}

							@Override
							public void onLoadingFailed(String imageUri,
									View view, FailReason failReason) {
								img_upcomingsponsor
										.setImageDrawable(getResources()
												.getDrawable(
														R.drawable.img_not_found_large));

							}

							@Override
							public void onLoadingComplete(String imageUri,
									View view, Bitmap loadedImage) {

							}
						});

			}

		} catch (Exception e) {

		}
	}

	@Override
	protected void onPause() {

		super.onPause();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);
		outState.putSerializable(Common.USER_VALUE, Common.USER);
		outState.putSerializable(Common.SELECTED_CONTEST, contestUpcoming);

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		contestUpcoming = (Contest) savedInstanceState
				.getSerializable(Common.SELECTED_CONTEST);
		Common.USER = (User) savedInstanceState
				.getSerializable(Common.USER_VALUE);
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}
}
