package com.prajaval.locationsponsors_activity;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.camera.CameraActivity;
import com.prajaval.core.Contest;
import com.prajaval.core.User;
import com.prajaval.locationcontest.gridviews_entries.LocationContestEntryList;
import com.prajaval.video.VideoActivity;

import common.prajaval.common.Common;

public class LocationContestSponsors_Activity extends FragmentActivity {

	Contest locationcontest;
	DisplayImageOptions options;

	ImageView img_loccontest_image;
	TextView txt_loccontest_title, txt_loccontest_desc, txt_loccontest_type;
	TextView txt_loccontest_number_entry, txt_loccontest_days_left;
	TextView txtlocPrizes, txtlocsponsors, txtlocRules;

	ImageButton btn_locUpload;
	ImageView img_locsponsor;
	LinearLayout linearlayoutEntryloc;

	RelativeLayout RLlocUpload;

	@Override
	protected void onCreate(Bundle bundle) {

		super.onCreate(bundle);
		Intent intent = getIntent();
		setContentView(R.layout.locationcontestsponsors_activity);

		initialise_widgets();

		locationcontest = (Contest) intent
				.getSerializableExtra(Common.SELECTED_LOCATION_CONTEST);

		options = new DisplayImageOptions.Builder()
				.resetViewBeforeLoading(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300)).build();

		btn_locUpload.setOnClickListener(btnUploadClick);

		linearlayoutEntryloc.setOnClickListener(entrylayoutclick);

		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	OnClickListener entrylayoutclick = new OnClickListener() {

		@Override
		public void onClick(View v) {

			try {
				if (locationcontest != null) {

					if (locationcontest.getiContestEntries() > 0) {
						Intent intent = new Intent(
								LocationContestSponsors_Activity.this,
								LocationContestEntryList.class);
						intent.putExtra(Common.SELECTED_CONTEST,
								locationcontest);
						startActivity(intent);
					} else {
						Toast.makeText(LocationContestSponsors_Activity.this,
								"No entry in this contest.", Toast.LENGTH_SHORT)
								.show();
					}

				} else {

					Toast.makeText(
							LocationContestSponsors_Activity.this,
							"Oops.! Something went wrong.\nCheck your data connection.",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {

			}

		}
	};

	OnClickListener btnUploadClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (locationcontest.getIflag() > 0) {
				SelectImage();
			} else {

				Toast.makeText(
						LocationContestSponsors_Activity.this,
						"This particular contest is not availabe for your location.",
						Toast.LENGTH_LONG).show();

			}
		}
	};

	private void initialise_widgets() {
		img_loccontest_image = (ImageView) findViewById(R.id.img_loccontest_image);
		txt_loccontest_title = (TextView) findViewById(R.id.txt_loccontest_title);
		txt_loccontest_desc = (TextView) findViewById(R.id.txt_loccontest_desc);
		txt_loccontest_type = (TextView) findViewById(R.id.txt_loccontest_type);

		txt_loccontest_number_entry = (TextView) findViewById(R.id.txt_loccontest_number_entry);
		txt_loccontest_days_left = (TextView) findViewById(R.id.txt_loccontest_days_left);

		txtlocPrizes = (TextView) findViewById(R.id.txtlocPrizes);
		txtlocsponsors = (TextView) findViewById(R.id.txtlocsponsors);
		txtlocRules = (TextView) findViewById(R.id.txtlocRules);

		btn_locUpload = (ImageButton) findViewById(R.id.btn_locUpload);

		img_locsponsor = (ImageView) findViewById(R.id.img_locsponsor);

		linearlayoutEntryloc = (LinearLayout) findViewById(R.id.linearlayoutEntryloc);
		RLlocUpload = (RelativeLayout) findViewById(R.id.RLlocUpload);

	}

	private void bind_data(Contest contest) {

		if (contest != null) {

			getActionBar().setTitle(contest.getStr_ContestTitle());

			txt_loccontest_title.setText(contest.getStr_ContestTitle());
			txt_loccontest_desc.setText(contest.getStr_ContestDesc());

			String type = "";
			if (!contest.getStr_ContestType().equals("image")) {
				type = "Video";
			} else {
				type = "Photo";
			}

			txt_loccontest_type.setText(type);

			txt_loccontest_number_entry.setText(""
					+ contest.getiContestEntries());
			txt_loccontest_days_left.setText(DaysLeft(contest));
			txtlocPrizes.setText(contest.getStr_ContestPrize());
			txtlocsponsors.setText(contest.getStr_ContestSponsorName());

			String rule1 = getResources().getString(R.string.Rules_1);
			String rule2 = getResources().getString(R.string.Rules_2);
			String rule3 = getResources().getString(R.string.Rules_3);
			String rule4 = getResources().getString(R.string.Rules_4);
			String rule5 = getResources().getString(R.string.Rules_5);
			String rule6 = getResources().getString(R.string.Rules_6);
			String rule7 = getResources().getString(R.string.Rules_7);
			String rule8 = getResources().getString(R.string.Rules_8);
			String rule9 = getResources().getString(R.string.Rules_9);

			String string = rule1 + rule2 + rule3 + rule4 + rule5 + rule6
					+ rule7 + rule8 + rule9;

			txtlocRules.setText(string);

			ImageLoader.getInstance().displayImage(
					contest.getStr_ContestImage(), img_loccontest_image,
					options, new SimpleImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {

							img_loccontest_image
									.setImageDrawable(getResources()
											.getDrawable(
													R.drawable.img_not_found_large));

						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {

						}
					});

			ImageLoader.getInstance().displayImage(
					contest.getStr_ContestSponsorLogo(), img_locsponsor,
					options, new SimpleImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {
							img_locsponsor
									.setImageDrawable(getResources()
											.getDrawable(
													R.drawable.img_not_found_thumb));

						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {

						}
					});

			
		}

	}

	@SuppressLint("SimpleDateFormat")
	private String DaysLeft(Contest contest) {

		long diffDays = 0;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		Date date = new Date();
		String sDate = new SimpleDateFormat("yyyy-MM-dd").format(date);

		String eDate = contest.getStr_ContestEndDate();
		try {
			Date d1 = formatter.parse(sDate);
			Date d2 = formatter.parse(eDate);

			long diff = d2.getTime() - d1.getTime();
			diffDays = diff / (24 * 60 * 60 * 1000);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return String.valueOf(diffDays);
	}

	@Override
	protected void onResume() {
		bind_data(locationcontest);
		super.onResume();
	}

	private void SelectImage() {
		final CharSequence[] items = { "camera", "gallery" };

		AlertDialog.Builder builder = new Builder(
				LocationContestSponsors_Activity.this);
		builder.setCancelable(true);
		builder.setTitle("Upload your entry");
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {
				try {

					if (items[item].equals("camera")) {

						if (locationcontest.getStr_ContestType()
								.equals("image")) {
							Intent intent = new Intent(
									LocationContestSponsors_Activity.this,
									CameraActivity.class);
							intent.putExtra(Common.SELECTED_CONTEST,
									locationcontest);
							intent.putExtra(Common.MESSAGE, "camera");
							startActivity(intent);
						} else {
							// video
							Intent intent = new Intent(
									LocationContestSponsors_Activity.this,
									VideoActivity.class);
							intent.putExtra(Common.SELECTED_CONTEST,
									locationcontest);
							intent.putExtra(Common.MESSAGE, "camera");
							startActivity(intent);
						}

						dialog.dismiss();

					} else if (items[item].equals("gallery")) {

						if (locationcontest.getStr_ContestType()
								.equals("image")) {
							Intent intent = new Intent(
									LocationContestSponsors_Activity.this,
									CameraActivity.class);
							intent.putExtra(Common.SELECTED_CONTEST,
									locationcontest);
							intent.putExtra(Common.MESSAGE, "gallery");
							startActivity(intent);
						} else {
							// video
						}
						dialog.dismiss();

					}
				} catch (Exception e) {
					e.toString();
				}

			}
		});
		builder.show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);

		outState.putSerializable(Common.SELECTED_LOCATION_CONTEST,
				locationcontest);
		outState.putSerializable(Common.USER_VALUE, Common.USER);

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		locationcontest = (Contest) savedInstanceState
				.getSerializable(Common.SELECTED_LOCATION_CONTEST);
		Common.USER = (User) savedInstanceState
				.getSerializable(Common.USER_VALUE);
		super.onRestoreInstanceState(savedInstanceState);
	}

}
