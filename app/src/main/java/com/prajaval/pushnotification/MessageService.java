package com.prajaval.pushnotification;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.achieveee.hallaexpress.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.prajaval.splashscreen.SplashScreen;

@SuppressLint("NewApi")
public class MessageService extends IntentService {

	public static final int NOTIFICATION_ID = 1;
	NotificationCompat.Builder builder;

	String title, message, subtitle, tickerText;

	public static final String MessageService = "MessageService";

	public MessageService() {
		super("MessageService");

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		try {

			Bundle extras = intent.getExtras();

			title = intent.getExtras().getString("title");
			message = intent.getExtras().getString("message");
			subtitle = intent.getExtras().getString("subtitle");
			tickerText = intent.getExtras().getString("tickerText");
			GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

			String messageType = gcm.getMessageType(intent);

			if (!extras.isEmpty()) { // has effect of unparcelling Bundle

				if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
						.equals(messageType)) {

				} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
						.equals(messageType)) {
					// sendNotification("Deleted messages on server: "
					// + extras.toString());
					// If it's a regular GCM message, do some work.
				}

				else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
						.equals(messageType)) {

					sendNotification();

				}
			}
			// Release the wake lock provided by the WakefulBroadcastReceiver.
			PushNotification.completeWakefulIntent(intent);

		} catch (Exception e) {

		}
	}

	@SuppressLint("NewApi")
	private void sendNotification() {
		Intent notificationIntent = new Intent(getApplicationContext(),
				SplashScreen.class);

		PendingIntent contentIntent = PendingIntent.getActivity(
				getApplicationContext(), 0, notificationIntent,
				PendingIntent.FLAG_CANCEL_CURRENT);

		NotificationManager nm = (NotificationManager) getApplicationContext()
				.getSystemService(Context.NOTIFICATION_SERVICE);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				getApplicationContext());

		NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
		inboxStyle.addLine(message);

		builder.setContentIntent(contentIntent).setSmallIcon(R.drawable.halla)
				.setTicker(tickerText).setWhen(System.currentTimeMillis())
				.setAutoCancel(true).setContentTitle(title)
				.setContentText(subtitle).setDefaults(-1).setStyle(inboxStyle);

		// builder.setOngoing(true); //persistent status bar

		Notification n = builder.build();

		nm.notify(100, n);

	}

}
