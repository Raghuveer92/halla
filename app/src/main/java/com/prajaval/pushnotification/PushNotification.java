package com.prajaval.pushnotification;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class PushNotification extends WakefulBroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		try {
			ComponentName comp = new ComponentName(context.getPackageName(),
					MessageService.class.getName());
			// Start the service, keeping the device awake while it is
			// launching.
			startWakefulService(context, (intent.setComponent(comp)));
			setResultCode(Activity.RESULT_OK);
		} catch (Exception e) {
			e.toString();
		}

	}

}
