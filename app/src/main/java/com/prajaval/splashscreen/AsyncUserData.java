package com.prajaval.splashscreen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.json.classes.JSONArray;
import com.json.classes.JSONObject;
import com.prajaval.core.User;

import common.prajaval.common.Common;

public class AsyncUserData extends AsyncTask<Void, Void, Void> {

	public final String LOGIN_BY_ID = "http://halla.in/api/v1/user";

	int UserID;
	double lattitude;
	double longitude;
	Context context;
	GoogleCloudMessaging gcm;
	String regDeviceID = "";

	public AsyncUserData(Context con, int UserID, double lat, double lon) {
		this.UserID = UserID;
		lattitude = lat;
		longitude = lon;
		context = con;
	}

	@Override
	protected Void doInBackground(Void... params) {

		try {
			HttpPost httppost = new HttpPost(LOGIN_BY_ID);
			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			HttpClient httpClient = new DefaultHttpClient();

			try {

				gcm = GoogleCloudMessaging.getInstance(context);

				for (int i = 0; i < 50; i++) {
					regDeviceID = gcm.register(Common.SENDERID);
					if (!regDeviceID.equals("")) {
						break;
					}
				}

				StringBody userID = new StringBody(String.valueOf(UserID));
				StringBody lat = new StringBody(String.valueOf(lattitude));
				StringBody lon = new StringBody(String.valueOf(longitude));
				StringBody deviceid = new StringBody(regDeviceID);

				entity.addPart("user_id", userID);
				entity.addPart("lat", lat);
				entity.addPart("long", lon);
				entity.addPart("deviceid", deviceid);

			} catch (Exception e) {
				e.printStackTrace();
			}

			httppost.setEntity(entity);

			try {
				HttpResponse response = httpClient.execute(httppost);
				BufferedReader in = null;
				try {
					in = new BufferedReader(new InputStreamReader(response
							.getEntity().getContent(), "UTF-8"));
					StringBuffer sb = new StringBuffer("");
					String line = "";
					String NL = System.getProperty("line.separator");
					while ((line = in.readLine()) != null) {
						sb.append(line + NL);
					}
					in.close();
					Common.USER = GetuserProfile(sb.toString());
				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Map<String, String> lstlogin = new HashMap<String, String>();
		//
		// lstlogin.put("user_id", String.valueOf(UserID));
		// lstlogin.put("lat", String.valueOf(lattitude));
		// lstlogin.put("long", String.valueOf(longitude));
		//
		// String json = CommonURL.Execute(LOGIN_BY_ID, lstlogin, null);
		//
		// Common.USER = GetuserProfile(json);
		return null;
	}

	private User GetuserProfile(String Json) {
		User user = new User();

		try {
			JSONObject jData = null;
			JSONArray jAData = null;
			JSONObject jObject = new JSONObject(Json);
			if (jObject.has("data")) {
				jData = jObject.getJSONObject("data");
			}

			if (jObject.has("entries")) {
				jAData = jObject.getJSONArray("entries");
			}

			user = User.GetUser(jData, jAData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;

	}

}
