package com.prajaval.splashscreen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;

import com.achieveee.hallaexpress.R;
import com.prajaval.app_dev.FirstScreenActivity;
import com.prajaval.core.User;
import com.prajaval.database.HallaDataSource;
import com.prajaval.location.AsyncGetLocation;
import com.prajaval.location.ILocation;
import com.prajaval.menudrawer.MainMenuDrawer;

import common.prajaval.common.Common;

public class SplashScreen extends FragmentActivity implements LocationListener {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 11;
    HallaDataSource dataSource;

    int UserID = 0;
    public final String FACEBOOK = "Facebook";
    public final String PREF_HALLA_USERID = "USERID";

    @Override
    protected void onCreate(Bundle bundle) {

        super.onCreate(bundle);

        setContentView(R.layout.splashscreen_activity);

        try {

            SharedPreferences facebookSP = getSharedPreferences(FACEBOOK,
                    MODE_PRIVATE);

            if (!facebookSP.getString(PREF_HALLA_USERID, "").equals("")) {

                UserID = Integer.parseInt(facebookSP.getString(
                        PREF_HALLA_USERID, ""));

            } else {
                dataSource = new HallaDataSource(SplashScreen.this);
                dataSource.open();
                User user = dataSource.GetUser_FromDB();
                Common.USER = user;
                if (user != null) {
                    UserID = user.getiUserID();
                }

                dataSource.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= 23) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                    // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                AsyncGetLocation asyncGetLocation = new AsyncGetLocation(
                        SplashScreen.this, SplashScreen.this, ilocation);
                asyncGetLocation.execute();
            }
        } else {
            AsyncGetLocation asyncGetLocation = new AsyncGetLocation(
                    SplashScreen.this, SplashScreen.this, ilocation);
            asyncGetLocation.execute();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AsyncGetLocation asyncGetLocation = new AsyncGetLocation(
                            SplashScreen.this, SplashScreen.this, ilocation);
                    asyncGetLocation.execute();
                } else {
                    if (UserID != 0) {
                        // redirect to menu.

                        Intent intent = new Intent(SplashScreen.this,
                                MainMenuDrawer.class);
                        SplashScreen.this.finish();
                        startActivity(intent);
                    } else {

                        Intent intent = new Intent(SplashScreen.this,
                                FirstScreenActivity.class);
                        SplashScreen.this.finish();
                        startActivity(intent);

                    }
                }
                return;
            }
        }
    }


    ILocation ilocation = new ILocation() {

        @Override
        public void ITaskCompleted(Location location) {
            try {

                if (location != null) {
                    // update location here.
                    Common.LOCATION = location;

                    new AsyncUserData(SplashScreen.this, UserID,
                            location.getLatitude(), location.getLongitude())
                            .execute();
                }

                final Handler handler = new Handler();

                final Runnable r = new Runnable() {
                    public void run() {

                        if (UserID != 0) {
                            // redirect to menu.

                            Intent intent = new Intent(SplashScreen.this,
                                    MainMenuDrawer.class);
                            SplashScreen.this.finish();
                            startActivity(intent);
                        } else {

                            Intent intent = new Intent(SplashScreen.this,
                                    FirstScreenActivity.class);
                            SplashScreen.this.finish();
                            startActivity(intent);

                        }
                    }
                };

                handler.postDelayed(r, 3000);

            } catch (Exception e) {

            }
        }
    };

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            // Update location variable here.
        }
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }


}
