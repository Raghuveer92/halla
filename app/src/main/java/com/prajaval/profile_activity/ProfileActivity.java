package com.prajaval.profile_activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.core.Entry;
import com.prajaval.core.User;
import com.prajaval.utility.SquareImageView;
import com.squareup.picasso.Picasso;

import common.prajaval.common.Common;

public class ProfileActivity extends Activity {

    Entry entry;
    SquareImageView img_entry, img_entryprofile;
    TextView txt_entrytitle, txt_entry_username;
    private DisplayImageOptions options;
    private DisplayImageOptions optionsuser;

    public static final String PROFILE = "Profile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);

        img_entry = (SquareImageView) findViewById(R.id.img_entry);
        txt_entrytitle = (TextView) findViewById(R.id.txt_entrytitle);
        txt_entry_username = (TextView) findViewById(R.id.txt_entry_username);
        img_entryprofile = (SquareImageView) findViewById(R.id.img_entryprofile);

        try {

            Intent intent = getIntent();
            entry = (Entry) intent.getSerializableExtra(Common.ENTRY);

            options = new DisplayImageOptions.Builder()

                    .resetViewBeforeLoading(true).cacheOnDisk(true)
                    .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(5)).build();

            optionsuser = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.user)
                    .showImageOnFail(R.drawable.user)
                    .resetViewBeforeLoading(true).cacheOnDisk(true)
                    .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(50)).build();

             binddata();

            getActionBar().setDisplayHomeAsUpEnabled(true);

        } catch (Exception e) {

        }

    }

    @Override
    protected void onResume() {
        binddata();
        super.onResume();
    }

    private void binddata() {

        try {

            getActionBar().setTitle(entry.getTitle());
            txt_entrytitle.setText(entry.getTitle());
            txt_entry_username.setText(Common.USER.getUserName());
            if (!TextUtils.isEmpty(entry.getThumb_url()))
            Picasso.with(this).load(entry.getThumb_url()).into(img_entry);
//			ImageLoader.getInstance().displayImage(entry.getThumb_url(),
//					img_entry, options, new SimpleImageLoadingListener() {
//						@Override
//						public void onLoadingStarted(String imageUri, View view) {
//
//						}
//
//						@Override
//						public void onLoadingFailed(String imageUri, View view,
//								FailReason failReason) {
//
//						}
//
//						@Override
//						public void onLoadingComplete(String imageUri,
//								View view, Bitmap loadedImage) {
//
//						}
//					});
            if (!TextUtils.isEmpty(Common.USER.getStrUser_image()))
                Picasso.with(this).load(Common.USER.getStrUser_image()).into(img_entryprofile);
//            ImageLoader.getInstance().displayImage(
//                    Common.USER.getStrUser_image(), img_entryprofile,
//                    optionsuser, new SimpleImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted(String imageUri, View view) {
//
//                        }
//
//                        @Override
//                        public void onLoadingFailed(String imageUri, View view,
//                                                    FailReason failReason) {
//
//                        }
//
//                        @Override
//                        public void onLoadingComplete(String imageUri,
//                                                      View view, Bitmap loadedImage) {
//
//                        }
//                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        try {
            outState.putSerializable(Common.ENTRY, entry);
            outState.putSerializable(Common.USER_VALUE, Common.USER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        try {
            entry = (Entry) savedInstanceState.getSerializable(Common.ENTRY);
            Common.USER = (User) savedInstanceState
                    .getSerializable(Common.USER_VALUE);
            // binddata();
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onRestoreInstanceState(savedInstanceState);
    }
}
