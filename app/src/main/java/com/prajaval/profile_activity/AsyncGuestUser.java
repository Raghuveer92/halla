package com.prajaval.profile_activity;

import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.json.classes.JSONArray;
import com.json.classes.JSONObject;
import com.prajaval.async.IUser;
import com.prajaval.core.User;
import com.prajaval.utility.CustomProgressDialog;

import common.prajaval.common.CommonURL;

public class AsyncGuestUser extends AsyncTask<Void, Void, User> {

	public final String LOGIN_BY_ID = "http://halla.in/api/user/";

	int UserID;

	Context context;
	IUser iUser;

	ProgressDialog dialog;

	public AsyncGuestUser(Context con, int UserID, IUser iUser) {
		this.UserID = UserID;
		context = con;
		this.iUser = iUser;

	}

	@Override
	protected void onPreExecute() {
		dialog = CustomProgressDialog.ctor(context);
		dialog.show();
	}

	@Override
	protected User doInBackground(Void... params) {

		User user = null;
		try {

			Map<String, String> lstlogin = new HashMap<String, String>();

			lstlogin.put("user_id", String.valueOf(UserID));

			String json = CommonURL.Execute(LOGIN_BY_ID + UserID, null, null);

			user = GetuserProfile(json);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return user;
	}

	@Override
	protected void onPostExecute(User result) {
		iUser.ITaskCompleted(result);
		if (dialog != null) {
			dialog.hide();
		}
	}

	private User GetuserProfile(String Json) {
		User user = new User();

		try {
			JSONObject jData = null;
			JSONArray jAData = null;
			JSONObject jObject = new JSONObject(Json);
			if (jObject.has("data")) {
				jData = jObject.getJSONObject("data");
			}

			if (jObject.has("entries")) {
				jAData = jObject.getJSONArray("entries");
			}

			user = User.GetUser(jData, jAData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;

	}

}
