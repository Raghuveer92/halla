package com.prajaval.profile_activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.async.IUser;
import com.prajaval.core.User;
import com.prajaval.fragment.profilefragment.ProfileGridViewAdapter;
import com.prajaval.utility.SquareImageView;
import common.prajaval.common.Common;

public class GuestProfileActivity extends Activity {

	SquareImageView img_guestuser_profile_image;
	TextView txt_guestuser_profile_name, txt_guestuser_points,
			txt_guestcontest_number_entered, txt_guestcontest_won;

	GridView gv_guestuser_gallery;
	private DisplayImageOptions options;

	int UserID;
	private User user;
	ProfileGridViewAdapter profileGridViewAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.guestprofile_activity);

		initialise_widgets();

		Intent intent = getIntent();
		UserID = (Integer) intent.getIntExtra(Common.USER_ID, 0);

		options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.user)
				.showImageOnFail(R.drawable.user).resetViewBeforeLoading(true)
				.cacheOnDisk(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.ARGB_8888).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(120)).build();

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	protected void onResume() {
		callservice();
		super.onResume();
	}

	private void callservice() {
		if (UserID > 0) {
			new AsyncGuestUser(GuestProfileActivity.this, UserID, iUser)
					.execute();
		}

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("temp", user);
		outState.putSerializable(Common.USER_VALUE, Common.USER);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		user = (User) savedInstanceState.getSerializable("temp");
		Common.USER = (User) savedInstanceState
				.getSerializable(Common.USER_VALUE);
		super.onRestoreInstanceState(savedInstanceState);
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	IUser iUser = new IUser() {

		@Override
		public void ITaskCompleted(User user) {
			if (user != null) {

				bind(user);
			}
		}

		@Override
		public void onFail() {

		}

		private void bind(User user) {

			try {

				if (user != null) {

					txt_guestuser_profile_name.setText(user.getUserName());

					ImageLoader.getInstance().displayImage(
							user.getStrUser_image(),
							img_guestuser_profile_image, options,
							new SimpleImageLoadingListener() {
								@Override
								public void onLoadingStarted(String imageUri,
										View view) {

								}

								@Override
								public void onLoadingFailed(String imageUri,
										View view, FailReason failReason) {
									img_guestuser_profile_image
											.setImageDrawable(getResources()
													.getDrawable(
															R.drawable.user));
								}

								@Override
								public void onLoadingComplete(String imageUri,
										View view, Bitmap loadedImage) {

								}
							});

					txt_guestuser_points.setText(user.getStrPoints());
					txt_guestcontest_won.setText(getWon(user));
					txt_guestcontest_number_entered.setText(getEntered(user));

					if (user.getEntries() != null) {
						if (user.getEntries().size() > 0) {
							profileGridViewAdapter = new ProfileGridViewAdapter(
									GuestProfileActivity.this,
									user.getEntries());
							gv_guestuser_gallery
									.setAdapter(profileGridViewAdapter);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	};

	private String getWon(User user) {
		int won = 0;
		if (user.entries != null) {
			for (com.prajaval.core.Entry eleEntry : user.getEntries()) {
				if (eleEntry.getWinner().equals("yes")) {
					won++;
				}
			}

		} else {
			won = 0;
		}

		return String.valueOf(won);
	}

	private String getEntered(User user) {
		int entered = 0;
		if (user.entries != null) {
			entered = user.entries.size();
		} else {
			entered = 0;
		}

		return String.valueOf(entered);
	}

	private void initialise_widgets() {

		img_guestuser_profile_image = (SquareImageView) findViewById(R.id.img_guestuser_profile_image);
		txt_guestuser_profile_name = (TextView) findViewById(R.id.txt_guestuser_profile_name);

		txt_guestuser_points = (TextView) findViewById(R.id.txt_guestuser_points);
		txt_guestcontest_won = (TextView) findViewById(R.id.txt_guestcontest_won);
		txt_guestcontest_number_entered = (TextView) findViewById(R.id.txt_guestcontest_number_entered);

		gv_guestuser_gallery = (GridView) findViewById(R.id.gv_guestuser_gallery);
	}
}
