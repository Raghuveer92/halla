package com.prajaval.opencontestsponsors_activity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.achieveee.hallaexpress.R;
import com.prajaval.core.Contest;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	private static final String[] CONTENT = new String[] { "Prizes", "Rules",
			"Sponsores" };
	Contest contest;
	Context context;
	FragmentManager fragmentManager;

	public TabsPagerAdapter(FragmentManager fm, Contest contest, Context context) {
		super(fm);
		this.context = context;
		this.contest = contest;
	}

	@Override
	public Fragment getItem(int position) {
		String test = CONTENT[position % CONTENT.length];

		if (test.equals("Prizes")) {
			return PrizesFragment.newInstance(contest.getStr_ContestPrize());
		} else if (test.equals("Rules")) {

			String rule1 = context.getResources().getString(R.string.Rules_1);
			String rule2 = context.getResources().getString(R.string.Rules_2);
			String rule3 = context.getResources().getString(R.string.Rules_3);
			String rule4 = context.getResources().getString(R.string.Rules_4);
			String rule5 = context.getResources().getString(R.string.Rules_5);
			String rule6 = context.getResources().getString(R.string.Rules_6);
			String rule7 = context.getResources().getString(R.string.Rules_7);
			String rule8 = context.getResources().getString(R.string.Rules_8);
			String rule9 = context.getResources().getString(R.string.Rules_9);

			String string = rule1 + rule2 + rule3 + rule4 + rule5 + rule6
					+ rule7 + rule8 + rule9;
			return PrizesFragment.newInstance(string);
		} else if (test.equals("Sponsores")) {
			return PrizesFragment.newInstance(contest
					.getStr_ContestSponsorName());
		}
		return null;

	}

	@Override
	public CharSequence getPageTitle(int position) {
		String selected = CONTENT[position % CONTENT.length].toUpperCase();
		return selected;
	}

	@Override
	public int getCount() {
		return CONTENT.length;
	}

}
