package com.prajaval.opencontestsponsors_activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;

public class PrizesFragment extends Fragment {

	private static final String KEY_CONTENT = "TestFragment:Content";

	String prizes = "???";

	public static PrizesFragment newInstance(String content) {
		PrizesFragment fragment = new PrizesFragment();

		fragment.prizes = content;

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.prizes_fragment, container,
				false);

		TextView txtPrizes = (TextView) rootView.findViewById(R.id.txtPrizes);
		txtPrizes.setText(prizes);
		txtPrizes.setPadding(20, 20, 20, 20);
		return rootView;
	}

}
