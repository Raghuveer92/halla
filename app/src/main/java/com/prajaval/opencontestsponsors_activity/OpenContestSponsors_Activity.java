package com.prajaval.opencontestsponsors_activity;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.MainActivity;
import com.prajaval.app_dev.framwordk_classes.BaseActivity;
import com.prajaval.camera.CameraActivity;
import com.prajaval.core.Contest;
import com.prajaval.core.User;
import com.prajaval.opencontest.gridviews_entries.ParticipateEntryListActivity;
import com.prajaval.sponsorurl.SponsorWebView;
import com.prajaval.video.VideoActivity;

import common.prajaval.common.Common;
import de.hdodenhof.circleimageview.CircleImageView;

public class OpenContestSponsors_Activity extends BaseActivity {

	private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 10;
	ImageView img_contest_image;
	TextView txt_contest_title, txt_contest_desc;
	TextView txt_contest_number_entry, txt_contest_days_left;
	TextView txtPrizes, txtsponsors, txtRules;
	DisplayImageOptions options;
	CircleImageView btn_Upload;
	ImageView img_sponsor;
	Contest contest;
	public static final String OPEN_CONTEST = "Open Contest";

	@Override
	protected void onResume() {

		super.onResume();
		bind_contest_data(contest);

	}

	@Override
	protected void onPause() {

		super.onPause();
	}

	@Override
	protected void onCreate(Bundle bundle) {

		super.onCreate(bundle);
		setContentView(R.layout.opencontestsponsors_activity);
		Intent intent = getIntent();

		contest = (Contest) intent
				.getSerializableExtra(Common.SELECTED_CONTEST);

		options = new DisplayImageOptions.Builder()
				.resetViewBeforeLoading(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300)).build();

		initiatewidgets();

		btn_Upload.setOnClickListener(btnUploadClickListener);
		img_sponsor.setOnClickListener(imgSponsorClick);
		findViewById(R.id.ll_entry).setOnClickListener(entriesOnClickListener);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);

		outState.putSerializable(Common.SELECTED_CONTEST, contest);
		outState.putSerializable(Common.USER_VALUE, Common.USER);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		contest = (Contest) savedInstanceState
				.getSerializable(Common.SELECTED_CONTEST);
		Common.USER = (User) savedInstanceState
				.getSerializable(Common.USER_VALUE);
		super.onRestoreInstanceState(savedInstanceState);
	}

	OnClickListener btnUploadClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			SelectImage();
		}
	};

	OnClickListener entriesOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			try {
				if (contest != null) {

					if (contest.getiContestEntries() > 0) {
						Intent intent = new Intent(
								OpenContestSponsors_Activity.this,
								ParticipateEntryListActivity.class);
						intent.putExtra(Common.SELECTED_CONTEST, contest);
						startActivity(intent);
					} else {
						Toast.makeText(OpenContestSponsors_Activity.this,
								"No entry in this contest.", Toast.LENGTH_SHORT)
								.show();
					}

				} else {

					Toast.makeText(
							OpenContestSponsors_Activity.this,
							"Oops.! Something went wrong.\nCheck your data connection.",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {

			}

		}
	};

	private void SelectImage() {
		final CharSequence[] items = { "camera", "gallery" };

		AlertDialog.Builder builder = new Builder(
				OpenContestSponsors_Activity.this);
		builder.setCancelable(true);
		builder.setTitle("Upload your entry");
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {
				try {
					dialog.dismiss();
					if (items[item].equals("camera")) {

						if (contest.getStr_ContestType().equals("image")) {
							Intent intent = new Intent(
									OpenContestSponsors_Activity.this,
									CameraActivity.class);
							intent.putExtra(Common.SELECTED_CONTEST, contest);
							intent.putExtra(Common.MESSAGE, "camera");
							startActivity(intent);
						} else {
							// video
							Intent intent = new Intent(
									OpenContestSponsors_Activity.this,
									VideoActivity.class);
							intent.putExtra(Common.SELECTED_CONTEST, contest);
							intent.putExtra(Common.MESSAGE, "camera");
							startActivity(intent);
						}



					} else if (items[item].equals("gallery")) {
//						dialog.dismiss();
						if (contest.getStr_ContestType().equals("image")) {
							getImageFromGallary();

						} else {
							// video
						}


					}
				} catch (Exception e) {
					e.toString();
				}

			}
		});
		builder.show();
	}

	private void getImageFromGallary() {
		if (Build.VERSION.SDK_INT >= 23){
			// Here, thisActivity is the current activity
			if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
					!= PackageManager.PERMISSION_GRANTED) {

				// Should we show an explanation?
				if (ActivityCompat.shouldShowRequestPermissionRationale(this,
						Manifest.permission.READ_EXTERNAL_STORAGE)) {

					// Show an expanation to the user *asynchronously* -- don't block
					// this thread waiting for the user's response! After the user
					// sees the explanation, try again to request the permission.

				} else {

					ActivityCompat.requestPermissions(this,
							new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
							MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

					// MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
					// app-defined int constant. The callback method gets the
					// result of the request.
				}
			}else{
				ActivityCompat.requestPermissions(this,
						new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
						MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
			}
		}else {
			Intent intent = new Intent(
					OpenContestSponsors_Activity.this,
					CameraActivity.class);
			intent.putExtra(Common.SELECTED_CONTEST, contest);
			intent.putExtra(Common.MESSAGE, "gallery");
			startActivity(intent);
		}


	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode){
		case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0
					&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				Intent intent = new Intent(
						OpenContestSponsors_Activity.this,
						CameraActivity.class);
				intent.putExtra(Common.SELECTED_CONTEST, contest);
				intent.putExtra(Common.MESSAGE, "gallery");
				startActivity(intent);
			} else {

				// permission denied, boo! Disable the
				// functionality that depends on this permission.
			}
			return;
		}
		}
	}

	private void initiatewidgets() {
		img_contest_image = (ImageView) findViewById(R.id.img_contest_image);
		txt_contest_title = (TextView) findViewById(R.id.txt_contest_title);
		txt_contest_desc = (TextView) findViewById(R.id.txt_contest_desc);
		txt_contest_number_entry = (TextView) findViewById(R.id.txt_contest_number_entry);
		txt_contest_days_left = (TextView) findViewById(R.id.txt_contest_days_left);
		btn_Upload = (CircleImageView) findViewById(R.id.btn_upload);
		txtPrizes = (TextView) findViewById(R.id.txtPrizes);
		txtsponsors = (TextView) findViewById(R.id.txtsponsors);
		txtRules = (TextView) findViewById(R.id.txtRules);
		img_sponsor = (ImageView) findViewById(R.id.img_sponsor);

	}

	OnClickListener imgSponsorClick = new OnClickListener() {

		@Override
		public void onClick(View v) {

			Intent intent = new Intent(OpenContestSponsors_Activity.this,
					SponsorWebView.class);
			intent.putExtra(getResources().getString(R.string.SPONSORS),
					contest.getStr_ContestSponsorUrl());
			startActivity(intent);
		}
	};

	@SuppressLint("SimpleDateFormat")
	private String DaysLeft(Contest contest) {

		long diffDays = 0;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		Date date = new Date();
		String sDate = new SimpleDateFormat("yyyy-MM-dd").format(date);

		String eDate = contest.getStr_ContestEndDate();
		try {
			Date d1 = formatter.parse(sDate);
			Date d2 = formatter.parse(eDate);

			long diff = d2.getTime() - d1.getTime();
			diffDays = diff / (24 * 60 * 60 * 1000);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return String.valueOf(diffDays);
	}

	private void bind_contest_data(Contest contest) {
		if (contest != null) {


			txt_contest_title.setText(contest.getStr_ContestTitle());
			txt_contest_desc.setText(contest.getStr_ContestDesc());

			String type = "";
			if (!contest.getStr_ContestType().equals("image")) {
				type = "Video";
			} else {
				type = "Photo";
			}
			initToolBar(type+" Contest",R.id.toolbar);


			txt_contest_number_entry.setText("" + contest.getiContestEntries());
			txt_contest_days_left.setText(DaysLeft(contest));
			txtPrizes.setText(contest.getStr_ContestPrize());
			txtsponsors.setText(contest.getStr_ContestSponsorName());

			String rule1 = getResources().getString(R.string.Rules_1);
			String rule2 = getResources().getString(R.string.Rules_2);
			String rule3 = getResources().getString(R.string.Rules_3);
			String rule4 = getResources().getString(R.string.Rules_4);
			String rule5 = getResources().getString(R.string.Rules_5);
			String rule6 = getResources().getString(R.string.Rules_6);
			String rule7 = getResources().getString(R.string.Rules_7);
			String rule8 = getResources().getString(R.string.Rules_8);
			String rule9 = getResources().getString(R.string.Rules_9);

			String string = rule1 + rule2 + rule3 + rule4 + rule5 + rule6
					+ rule7 + rule8 + rule9;

			txtRules.setText(string);

			ImageLoader.getInstance().displayImage(
					contest.getStr_ContestImage(), img_contest_image, options,
					new SimpleImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {

							img_contest_image
									.setImageDrawable(getResources()
											.getDrawable(
													R.drawable.img_not_found_large));

						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {

						}
					});

			ImageLoader.getInstance().displayImage(
					contest.getStr_ContestSponsorLogo(), img_sponsor, options,
					new SimpleImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {
							img_sponsor
									.setImageDrawable(getResources()
											.getDrawable(
													R.drawable.img_not_found_thumb));

						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {

						}
					});

		} else {
			Toast.makeText(OpenContestSponsors_Activity.this,
					"Oops.! something went wrong.", Toast.LENGTH_SHORT).show();

		}
	}

}
