package com.prajaval.menudrawer;

import java.util.ArrayList;
import java.util.Stack;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.app_dev.framwordk_classes.BaseActivity;
import com.prajaval.core.NavDrawerItem;
import com.prajaval.core.User;
import com.prajaval.database.HallaDataSource;
import com.prajaval.fragment.locationcontest.LocationContest;
import com.prajaval.fragment.opencontest.OpenContestFragment;
import com.prajaval.fragment.previouscontest.PreviousContest;
import com.prajaval.fragment.profilefragment.ProfileFragment;
import com.prajaval.fragment.upcomingcontest.UpcomingContest;
import com.squareup.picasso.Picasso;

import common.prajaval.common.Common;

public class MainMenuDrawer extends BaseActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private Stack<Fragment> fragmentStack;
    private FragmentManager fragmentManager;
    // nav drawer title
    private CharSequence mDrawerTitle;

    // used to store app title
    private CharSequence mTitle;

    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    LinearLayout drawerll;

    ImageView img_menuUser;
    TextView txt_menuusername, txt_menuuseremail;
    private DisplayImageOptions optionuser;
    private Toolbar toolbar;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenudrawer_activity);
        initToolBar("Halla");
        Initialise_widgets();
        fragmentManager = getFragmentManager();
        fragmentStack = new Stack<Fragment>();
        mTitle = mDrawerTitle = getTitle();
        setDrawer();
        FilledDrawerItems();

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);
        if (savedInstanceState == null) {
            // on first time display view for first nav item
            displayView(0);
            mDrawerList.setSelection(0);
        }

        optionuser = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new RoundedBitmapDisplayer(100)).build();

    }

    private void setDrawer() {

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, R.drawable.ic_drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();
    }

    public void initToolBar(String title) {
        initToolBar(title, R.id.toolbar);
    }

    @Override
    protected void onHomePressed() {
        mDrawerLayout.openDrawer(Gravity.LEFT);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_drawer;
    }

    private void binddata(User user) {

        if (user != null) {

            txt_menuusername.setText(user.getUserName());
            txt_menuuseremail.setText(user.getEmail());
            if (!TextUtils.isEmpty(user.getStrUser_image())) {
                Picasso.with(this).load(user.getStrUser_image()).into(img_menuUser);
            }

        }

    }

    private void FilledDrawerItems() {
        navDrawerItems = new ArrayList<NavDrawerItem>();

        // adding nav drawer items to array
        // Home
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], R.drawable.open_contest));
        // Find People
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], R.drawable.previous_contest));
        // Photos
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], R.drawable.locaion_contest));
        // Communities, Will add a counter here
        // navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
        // .getResourceId(3, -1), true, "22"));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], R.drawable.upcomming_contest));
        // Pages
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], R.drawable.profile_contest));
        // What's hot, We will add a counter here
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], R.drawable.logout));

        // Recycle the typed array
        navMenuIcons.recycle();

    }

    private void Initialise_widgets() {
        drawerll = (LinearLayout) findViewById(R.id.drawerll);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav drawer icons from resources
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);

        img_menuUser = (ImageView) findViewById(R.id.img_menuUser);
        txt_menuusername = (TextView) findViewById(R.id.txt_menuusername);
        txt_menuuseremail = (TextView) findViewById(R.id.txt_menuuseremail);
    }

    @Override
    protected void onResume() {
        binddata(Common.USER);
        super.onResume();
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onStart() {

        super.onStart();
    }

    public void setAllUnSelect() {
        for (NavDrawerItem navDrawerItem : navDrawerItems) {
            navDrawerItem.setSelect(false);
        }
    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
            adapter.currentPossition = position;
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */
    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new OpenContestFragment();
                break;
            case 1:
                fragment = new PreviousContest();
                break;
            case 2:
                fragment = new LocationContest();
                break;
            case 3:
                fragment = new UpcomingContest();
                break;
            case 4:
                fragment = new ProfileFragment();
                break;
            case 5:
                // fragment = new WhatsHotFragment();
                LogOut();
                break;

            default:
                break;
        }

        if (position != 5) {
            if (fragment != null) {
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.add(R.id.frame_container, fragment);
                fragmentStack.push(fragment);
                ft.commit();
                UpdateDrawer(position);

            } else {
                // error in creating fragment
                Log.e("MainActivity", "Error in creating fragment");
                mDrawerLayout.closeDrawer(drawerll);
            }
        }

    }

    @Override
    public void onBackPressed() {

        if (fragmentStack.size() > 1) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            fragmentStack.lastElement().onPause();
            ft.remove(fragmentStack.pop());
            fragmentStack.lastElement().onResume();
            ft.show(fragmentStack.lastElement());
            if ((fragmentStack.size() - 1) != 5) {
                UpdateDrawer(fragmentStack.size() - 1);
            }

            ft.commit();
        } else {

            Exit();
        }
    }

    void UpdateDrawer(int position) {
//        mDrawerList.setItemChecked(position, true);
//        setAllUnSelect();
//        mDrawerList.setSelection(position);
//        navDrawerItems.get(position).setSelect(true);
//        adapter.notifyDataSetChanged();
//        setTitle(navMenuTitles[position]);
        mDrawerLayout.closeDrawer(drawerll);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        initToolBar(title.toString());
    }

    private void Exit() {

        AlertDialog.Builder alertbox = new AlertDialog.Builder(
                MainMenuDrawer.this);

        alertbox.setMessage(Html
                .fromHtml("<small><b>ARE YOU SURE?</b></small>"));
        alertbox.setPositiveButton(Html.fromHtml("<b>Exit</b>"),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        Intent filter = new Intent("CLOSE");

                        sendBroadcast(filter);
                        MainMenuDrawer.this.finish();
                        System.exit(0);

                    }

                });

        alertbox.setNeutralButton(Html.fromHtml("<b>Cancel</b>"),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        alertbox.show();

    }

    private void LogOut() {

        AlertDialog.Builder alertbox = new AlertDialog.Builder(
                MainMenuDrawer.this);

        // if database having user id == open off line storage.

        alertbox.setMessage("Do you want to Logout?");
        alertbox.setPositiveButton("Logout",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        HallaDataSource dataSource = new HallaDataSource(
                                MainMenuDrawer.this);
                        dataSource.open();
                        int deleteID = dataSource.DeleteUser();

                        dataSource.close();

                        Intent filter = new Intent("CLOSE");

                        sendBroadcast(filter);
                        MainMenuDrawer.this.finish();
                        System.exit(0);

                    }

                });

        alertbox.setNeutralButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        alertbox.show();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
//        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
//        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(Common.USER_VALUE, Common.USER);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Common.USER = (User) savedInstanceState
                .getSerializable(Common.USER_VALUE);
        super.onRestoreInstanceState(savedInstanceState);
    }

}
