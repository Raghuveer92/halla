package com.prajaval.menudrawer;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.prajaval.core.NavDrawerItem;

public class NavDrawerListAdapter extends BaseAdapter {
	private Context context;
	public int currentPossition=0;
	private ArrayList<com.prajaval.core.NavDrawerItem> navDrawerItems;

	public NavDrawerListAdapter(Context context,
			ArrayList<NavDrawerItem> navDrawerItems) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.mainmenu_drawer_list_item,
					null);
		}
		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
		TextView txtCount = (TextView) convertView.findViewById(R.id.counter);
		if(currentPossition==position){
			txtTitle.setTextColor(ContextCompat.getColor(context,R.color.black));
			imgIcon.setColorFilter(ContextCompat.getColor(context,R.color.black));
			convertView.setBackgroundColor(ContextCompat.getColor(context,R.color.bg_color));
		}else {
			txtTitle.setTextColor(ContextCompat.getColor(context,R.color.et_gray));
			imgIcon.setColorFilter(ContextCompat.getColor(context,R.color.et_gray));
			convertView.setBackgroundColor(ContextCompat.getColor(context,R.color.white));
		}

		NavDrawerItem navDrawerItem = navDrawerItems.get(position);
		imgIcon.setImageResource(navDrawerItem.getIcon());
		txtTitle.setText(navDrawerItem.getTitle());

		// displaying count
		// check whether it set visible or not
		if (navDrawerItem.getCounterVisibility()) {
			txtCount.setText(navDrawerItem.getCount());
		} else {
			// hide the counter view
			txtCount.setVisibility(View.GONE);
		}
		if(navDrawerItem.isSelect()){
			txtTitle.setTextColor(Color.BLACK);
		}else {
			txtTitle.setTextColor(Color.parseColor("#484747"));
		}
		return convertView;
	}
}
