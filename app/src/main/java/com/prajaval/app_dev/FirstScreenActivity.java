package com.prajaval.app_dev;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.achieveee.hallaexpress.R;
import com.prajaval.app_dev.framwordk_classes.BaseActivity;
import com.prajaval.app_dev.framwordk_classes.CustomPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class FirstScreenActivity extends BaseActivity implements CustomPagerAdapter.PagerAdapterInterface {
    int[] slideImges = {R.drawable.first, R.drawable.second, R.drawable.third, R.drawable.fourth, R.drawable.fifth};
    private LinearLayout slide_circle_container;
    private List<View> circleViews = new ArrayList<>();
    private List<SlideImageFragment> slideImageFragments = new ArrayList<>();
    List<String> tab = new ArrayList<>();
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_screen);
        slide_circle_container = (LinearLayout) findViewById(R.id.lay_slide_circles);
        viewPager = (ViewPager) findViewById(R.id.viewpager_home);
        setSliderShow(slideImges);
        setOnClickListener(R.id.tv_skip);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_skip:

                Intent intent = new Intent(this,
                        RegisterActivity.class);
                finish();
                startActivity(intent);

                break;
        }
    }

    private void setSliderShow(int[] slideImges) {
        for (int x = 0; x < slideImges.length; x++) {
            View view = LayoutInflater.from(this).inflate(R.layout.row_slide_circle, null);
            slide_circle_container.addView(view);
            circleViews.add(view);
            SlideImageFragment slideImageFragment = SlideImageFragment.getInstance(slideImges[x]);
            slideImageFragments.add(slideImageFragment);
            tab.add("");
        }
        CustomPagerAdapter pagerAdapter = new CustomPagerAdapter(getSupportFragmentManager(), tab, this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(1);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                unSelectAll();
                circleViews.get(position).findViewById(R.id.lay_row_slider).setBackgroundResource(R.drawable.slide_circle_select);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if (circleViews.size() > 0) {
            circleViews.get(0).findViewById(R.id.lay_row_slider).setBackgroundResource(R.drawable.slide_circle_select);
        }


    }

    private void unSelectAll() {
        for (int x = 0; x < circleViews.size(); x++) {
            circleViews.get(x).findViewById(R.id.lay_row_slider).setBackgroundResource(R.drawable.slide_circle_unselect);
        }
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return slideImageFragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tab.get(position);
    }
}
