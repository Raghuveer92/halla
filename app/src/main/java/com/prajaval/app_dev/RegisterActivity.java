package com.prajaval.app_dev;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.achieveee.hallaexpress.R;
import com.prajaval.app_dev.framwordk_classes.BaseActivity;
import com.prajaval.app_dev.framwordk_classes.CustomPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import common.prajaval.common.Common;


public class RegisterActivity extends BaseActivity implements CustomPagerAdapter.PagerAdapterInterface {
    List<String> tab;
    private SignUpFragment signUpFragment;
    private SignInFragment signInFragment;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initToolBar(getString(R.string.welcome_to_halla), R.id.toolbar);
        tab = new ArrayList<>();
        initTab();
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_login);
        viewPager = (ViewPager) findViewById(R.id.pager_login);
        CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager(), tab, this);
        viewPager.setAdapter(customPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initTab() {
        tab.add(getString(R.string.login));
        tab.add(getString(R.string.sign_up));
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        switch (position) {
            case 0:
                if (signInFragment == null) {
                    signInFragment = new SignInFragment();
                }
                return signInFragment;
            case 1:
                if (signUpFragment == null) {
                    signUpFragment = new SignUpFragment();
                }
                return signUpFragment;
        }
        return new SignUpFragment();
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tab.get(position);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "On AR");

        if(viewPager.getCurrentItem() == 0){
            signInFragment.onActivityResult(requestCode, resultCode, data);
        }else{
            signUpFragment.onActivityResult(requestCode, resultCode, data);
        }


    }
}
