package com.prajaval.app_dev;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by nitin on 14/04/16.
 */
public class FBLoginUtil {
    private static final String TAG = "FBLoginUtil";
    private CallbackManager callbackManager;
    private Activity ctx;
    private FBLoginCallback callback;

    public FBLoginUtil(Activity ctx, FBLoginCallback callback) {
        this.ctx = ctx;
        this.callback = callback;
        onCreate();
    }

    private void onCreate() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(TAG, "onSuccess" + loginResult.getAccessToken().toString());
                loginResult.getAccessToken().toString();
                requestUserFBData(loginResult);
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "onCancel");
            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
                Log.i(TAG, "onError");
            }
        });
    }

    public void initiateFbLogin() {

        LoginManager.getInstance().logInWithReadPermissions(ctx, Arrays.asList("public_profile", "email"));
//        accessToken = AccessToken.getCurrentAccessToken();
    }

    private void requestUserFBData(final LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i(TAG, response.toString());
                        // Get facebook data from login
                        final FBProfile bFacebookData = getFacebookData(object);
                        final JSONObject jsonObject = new JSONObject();

//                        {
//                            "oauthProvider": "facebook",
//                                "accessToken": "ASDasdasdasdasdas",
//                                "profileId": "1231232312312"
//                        }

                        try {
//                            bFacebookData.putString("oauthProvider", "facebook");
//                            bFacebookData.putString("accessToken", loginResult.getAccessToken().getToken());
//                            bFacebookData.putString("profileId", bFacebookData.getString("idFacebook", ""));
                            ctx.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onSuccess(bFacebookData);
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                            ctx.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onFailure();
                                }
                            });
                        }


                    }
                }
        );
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
        request.setParameters(parameters);
        request.executeAsync();
    }

    private FBProfile getFacebookData(JSONObject object) {

        try {
            Bundle bundle = new Bundle();
            String id = object.getString("id");
            FBProfile p = new FBProfile();
            bundle.putString("idFacebook", id);
            p.profileId = id;
            String firstName = "", lastName = "", gender = "", location = "", email = "";
            if (object.has("first_name")) {
                firstName = object.getString("first_name");
            }
            if (object.has("last_name")) {
                lastName = object.getString("last_name");
            }
            if (object.has("email")) {
                email = object.getString("email");
            }
            if (object.has("gender")) {
                gender = object.getString("gender");
            }
            if (object.has("location")) {
                location = object.optJSONObject("location").getString("name");
            }
            p.fName = firstName;
            p.lName = lastName;
            p.gender = gender;
            p.location = location;
            p.email = email;

            return p;
        } catch (Exception e) {

        }
        return null;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public interface FBLoginCallback {
        void onSuccess(FBProfile bundle);

        void onFailure();
    }

    public static class FBProfile {
        String fName, lName, gender, location, accessToken, profileId, email;

    }
}
