package com.prajaval.app_dev;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.achieveee.hallaexpress.R;
import com.facebook.login.DefaultAudience;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.prajaval.app_dev.framwordk_classes.BaseFragment;
import com.prajaval.app_dev.framwordk_classes.Util;
import com.prajaval.async.AsyncInserUserDB;
import com.prajaval.async.AsyncLogin;
import com.prajaval.async.IUser;
import com.prajaval.core.User;
import com.prajaval.menudrawer.MainMenuDrawer;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

import java.util.List;

import common.prajaval.common.Common;

/**
 * Created by nbansal2211 on 10/09/16.
 */
public abstract class BaseAuthFragment extends BaseFragment {
    protected SimpleFacebook mSimpleFacebook;
    protected String email;
    protected GoogleUtil googleUtil;
    private static final String APP_ID = "225898524453049";
    private static final String APP_NAMESPACE = "halla";
    private FBLoginUtil fbLoginUtil;


    @Override
    public void initViews() {
        setOnClickListener(R.id.iv_facebook, R.id.iv_google);
        fbLoginUtil = new FBLoginUtil(getActivity(), new FBLoginUtil.FBLoginCallback() {
            @Override
            public void onSuccess(FBLoginUtil.FBProfile bundle) {
                RegisterWithHalla(bundle);
            }

            @Override
            public void onFailure() {
                showToast("Failed to Facebook login..!");
            }
        });
        googleUtil = GoogleUtil.getInstance((AppCompatActivity) getActivity(), new GoogleUtil.GoogleLoginCallBack() {
            @Override
            public void onSuccess(GoogleSignInAccount googleSignInAccount) {

                Log.i(TAG, "Success login from google");
                String photoUrl = "";
                if (googleSignInAccount.getPhotoUrl() != null) {
                    photoUrl = googleSignInAccount.getPhotoUrl() + "";
                }

                AsyncLogin asyncLogin = new AsyncLogin(getActivity(),
                        googleSignInAccount.getId(),
                        googleSignInAccount.getDisplayName(),
                        googleSignInAccount.getDisplayName(),
                        googleSignInAccount.getEmail(),
                        photoUrl,
                        "google",
                        " ", 0.0d, 0.0d, iUser);
                asyncLogin.execute();
            }

            @Override
            public void onFailed() {
                Log.i(TAG, "Failed to login from google");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_facebook:
                fbLoginUtil.initiateFbLogin();
                break;
            case R.id.iv_google:
                googleLogin();
                break;
        }
    }

    private void googleLogin() {
        googleUtil.signInWithGoogle();

    }

    private void setFacebookPermissions() {
        Permission[] permissions = new Permission[]{
                Permission.EMAIL, Permission.PUBLISH_ACTION};

        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(APP_ID).setNamespace(APP_NAMESPACE)
                .setPermissions(permissions)
                .setDefaultAudience(DefaultAudience.FRIENDS)
                .setAskForAllPermissionsAtOnce(false).build();

        SimpleFacebook.setConfiguration(configuration);
    }


    private void RegisterWithHalla(FBLoginUtil.FBProfile profile) {
        if (profile != null) {

            if (!TextUtils.isEmpty(profile.email)) {
                email = profile.email;
                sendOtherData(profile);
            } else {
                showDialog(profile);
            }
        }
    }

    private void sendOtherData(FBLoginUtil.FBProfile profile) {
        String name = profile.fName + " " + profile.lName;
        String location_name = "";
        if (!TextUtils.isEmpty(profile.location)) {
            location_name = profile.location;
        }

        String SocialUserName = name;
        String id = profile.profileId;
        String social_type = "facebook";
        String socialpicture = "http://graph.facebook.com/" + id
                + "/picture?type=large";

        double lattitude = 0.0;
        double longitude = 0.0;
        if (Common.LOCATION != null) {
            lattitude = Common.LOCATION.getLatitude();
            longitude = Common.LOCATION.getLongitude();
        }

        AsyncLogin asyncLogin = new AsyncLogin(getActivity(), id,
                name, SocialUserName, email, socialpicture, social_type,
                location_name, lattitude, longitude, iUser);
        asyncLogin.execute();

    }

    private void showDialog(final FBLoginUtil.FBProfile profile) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Enter your email");

        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setCancelable(false);

        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        email = input.getText().toString();
                        if (TextUtils.isEmpty(email)) {
                            input.setError(getString(R.string.error_email));
                        } else if (!Util.isValidEmail(email)) {
                            input.setError(getString(R.string.error_invailid_email));
                        } else {
                            dialog.dismiss();
                            sendOtherData(profile);
                        }
                    }
                });

        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }


    IUser iUser = new IUser() {

        @Override
        public void ITaskCompleted(User user) {

            if (user != null) {

                Common.USER = user;

                // insert in db
                new AsyncInserUserDB(getActivity(), user).execute();

                // redirect to main menu
                Intent intent = new Intent(getActivity(),
                        MainMenuDrawer.class);
                getActivity().finish();
                startActivity(intent);

            }

        }

        @Override
        public void onFail() {
            showToast("Failed to login..!");
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "sign_in fragment onResult()");
        if (resultCode != getActivity().RESULT_OK) return;
        if (requestCode == Common.GOOGLE_SIGHN_IN) {
            googleUtil.onActivityResult(data);
        } else {
            fbLoginUtil.onActivityResult(requestCode, resultCode, data);
//            mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        }
    }
}
