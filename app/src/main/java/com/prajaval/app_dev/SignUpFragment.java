package com.prajaval.app_dev;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.prajaval.app_dev.framwordk_classes.BaseFragment;
import com.prajaval.app_dev.framwordk_classes.Util;
import com.prajaval.async.AsyncInserUserDB;
import com.prajaval.async.AsyncLogin;
import com.prajaval.async.IUser;
import com.prajaval.core.User;
import com.prajaval.facebooklogin.FaceBookLogin;
import com.prajaval.menudrawer.MainMenuDrawer;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

import java.util.List;

import common.prajaval.common.Common;


/**
 * Created by raghu on 28/8/16.
 */
public class SignUpFragment extends BaseAuthFragment {
    @Override
    public void initViews() {
        super.initViews();
        setOnClickListener(R.id.btn_sign_in);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_sign_in:
                submitData();
                break;
        }
    }

    private void submitData() {
        String name = getTextInputLayout(R.id.til_name);
        String email = getTextInputLayout(R.id.til_email);
        String pass = getTextInputLayout(R.id.til_pass);
        if (TextUtils.isEmpty(name)) {
            setTextInputError(R.id.til_name, getString(R.string.error_name));
            return;
        }
        if (TextUtils.isEmpty(email)) {
            setTextInputError(R.id.til_email, getString(R.string.error_email));
            return;
        }
        if (!Util.isValidEmail(email)) {
            setTextInputError(R.id.til_email, getString(R.string.error_invailid_email));
            return;
        }
        if (TextUtils.isEmpty(pass)) {
            setTextInputError(R.id.til_pass, getString(R.string.error_pass));
            return;
        }
        User user = new User();
        user.setUserName(name);
        user.setEmail(email);
        user.setPassword(pass);
        AsyncSignup asyncSignup = new AsyncSignup(getActivity(), user, new IUser() {
            @Override
            public void ITaskCompleted(User user) {
                if (user != null) {

                    Common.USER = user;

                    // insert in db
                    new AsyncInserUserDB(getActivity(), user).execute();

                    // redirect to main menu
                    Intent intent = new Intent(getActivity(),
                            MainMenuDrawer.class);
                    getActivity().finish();
                    startActivity(intent);

                }
            }

            @Override
            public void onFail() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showToast(getString(R.string.fail_signup));
                    }
                });

            }
        });
        asyncSignup.execute();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_sin_up;
    }


    private void setTextInputError(int tilId, String string) {
        ((TextInputLayout) findView(tilId)).setError(string);
    }

    private String getTextInputLayout(int textInputLayoutId) {
        return ((TextInputLayout) findView(textInputLayoutId)).getEditText().getText().toString().trim();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
