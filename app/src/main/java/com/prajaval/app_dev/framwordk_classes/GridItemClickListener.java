package com.prajaval.app_dev.framwordk_classes;

import android.view.View;

public interface GridItemClickListener {

	void onGridItemClicked(View v, int position, long itemId);

}