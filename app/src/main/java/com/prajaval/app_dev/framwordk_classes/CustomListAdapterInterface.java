package com.prajaval.app_dev.framwordk_classes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public interface CustomListAdapterInterface {
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater);

}
