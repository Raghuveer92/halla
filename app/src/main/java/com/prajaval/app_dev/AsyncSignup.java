package com.prajaval.app_dev;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.json.classes.JSONArray;
import com.json.classes.JSONException;
import com.json.classes.JSONObject;
import com.prajaval.async.IUser;
import com.prajaval.core.User;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import common.prajaval.common.Common;

public class AsyncSignup extends AsyncTask<Void, Void, User> {
	User user;
	IUser iUser;
	Context context;
	ProgressDialog dialog;
	GoogleCloudMessaging gcm;


	String regDeviceID = "";
	final String LOGIN = "http://halla.in/api/v1/signupmail/";

	public AsyncSignup(Context context,User user,IUser iUser) {
		this.context = context;
		this.user=user;
		this.iUser=iUser;
	}

	@Override
	protected void onPreExecute() {

		dialog = ProgressDialog.show(context, "wait...",
				"Register with halla...", true);
	}

	@Override
	protected User doInBackground(Void... params) {
		String responseUser = "";
		try {


			gcm = GoogleCloudMessaging.getInstance(context);

			for (int i = 0; i < 50; i++) {
				regDeviceID = gcm.register(Common.SENDERID);
				if (!regDeviceID.equals("")) {
					break;
				}
			}

			HttpPost httppost = new HttpPost(LOGIN);
			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			HttpClient httpClient = new DefaultHttpClient();

			try {
				StringBody sname = new StringBody(user.getUserName());
				StringBody email = new StringBody(user.getEmail());
				StringBody pass = new StringBody(user.getPassword());
				StringBody lattitude = new StringBody("1.00");
				StringBody longitude = new StringBody("1.00");
				StringBody deviceid = new StringBody(regDeviceID);

				entity.addPart("name", sname);
				entity.addPart("pass", pass);
				entity.addPart("Email", email);
				entity.addPart("lat", lattitude);
				entity.addPart("long", longitude);
				entity.addPart("deviceid", deviceid);

			} catch (Exception e) {

			}

			httppost.setEntity(entity);

			try {
				HttpResponse response = httpClient.execute(httppost);
				BufferedReader in = null;
				try {
					// Log.d("status line ", "test " +
					// response.getStatusLine().toString());
					in = new BufferedReader(new InputStreamReader(response
							.getEntity().getContent(), "UTF-8"));
					StringBuffer sb = new StringBuffer("");
					String line = "";
					String NL = System.getProperty("line.separator");
					while ((line = in.readLine()) != null) {
						sb.append(line + NL);
					}
					responseUser = sb.toString();
					in.close();

				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					iUser.onFail();
				}

			} catch (ClientProtocolException e) {
				iUser.onFail();

				e.printStackTrace();
			} catch (IOException e) {
				iUser.onFail();

				e.printStackTrace();
			}

		} catch (Exception e) {
			iUser.onFail();

			e.printStackTrace();
		}
		return create_user(responseUser);

	}

	@Override
	protected void onPostExecute(User result) {
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		iUser.ITaskCompleted(result);

	}

	private User create_user(String responseUser) {
		User user = null;
		JSONObject jObject;
		try {
			jObject = new JSONObject(responseUser);
			JSONArray jAData = null;

			JSONObject jData = null;

			if (jObject.has("data")) {
				jData = jObject.getJSONObject("data");
//				if(jData!=null){
//					user.setiUserID(Integer.parseInt(jData.get("id").toString()));
//					user.setSocialName(jData.getString("social_username"));
//					user.setUserName(jData.getString("name"));
//					user.setEmail(jData.getString("email"));
//					user.setStrPoints(jData.getString("points"));
//				}
			}



			user = User.GetUser(jData, jAData);

		} catch (JSONException e) {
			e.printStackTrace();
			iUser.onFail();

		}
		return user;
	}

}
