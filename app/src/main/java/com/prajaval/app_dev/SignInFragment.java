package com.prajaval.app_dev;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.Tag;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.prajaval.MainActivity;
import com.prajaval.app_dev.framwordk_classes.BaseFragment;
import com.prajaval.app_dev.framwordk_classes.Util;
import com.prajaval.app_dev.intagram.InstagramApp;
import com.prajaval.async.AsyncInserUserDB;
import com.prajaval.async.AsyncLogin;
import com.prajaval.async.IUser;
import com.prajaval.core.User;
import com.prajaval.facebooklogin.FaceBookLogin;
import com.prajaval.menudrawer.MainMenuDrawer;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

import java.util.List;

import common.prajaval.common.Common;


/**
 * Created by raghu on 28/8/16.
 */
public class SignInFragment extends BaseAuthFragment {
    private SimpleFacebook mSimpleFacebook;
    private String email;
    private GoogleUtil googleUtil;

    @Override
    public void initViews() {
        super.initViews();
        setOnClickListener(R.id.btn_login);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_login:
                submitData();
                break;
        }
    }

    private void submitData() {
        String email = getTextInputLayout(R.id.til_email);
        String pass = getTextInputLayout(R.id.til_pass);
        if (TextUtils.isEmpty(email)) {
            setTextInputError(R.id.til_email, getString(R.string.error_email));
            return;
        }
        if (!Util.isValidEmail(email)) {
            setTextInputError(R.id.til_email, getString(R.string.error_invailid_email));
            return;
        }
        if (TextUtils.isEmpty(pass)) {
            setTextInputError(R.id.til_pass, getString(R.string.error_pass));
            return;
        }
        User user = new User();
        user.setEmail(email);
        user.setPassword(pass);
        AsyncSignin asyncSignup = new AsyncSignin(getActivity(), user, new IUser() {
            @Override
            public void ITaskCompleted(User user) {
                if (user != null) {
                    if (user != null) {

                        Common.USER = user;

                        // insert in db
                        new AsyncInserUserDB(getActivity(), user).execute();

                        // redirect to main menu
                        Intent intent = new Intent(getActivity(),
                                MainMenuDrawer.class);
                        getActivity().finish();
                        startActivity(intent);

                    }
                }
            }

            @Override
            public void onFail() {
                showToast("fail to login");
            }
        });
        asyncSignup.execute();
    }


    IUser iUser = new IUser() {

        @Override
        public void ITaskCompleted(User user) {

            if (user != null) {

                Common.USER = user;

                // insert in db
                new AsyncInserUserDB(getActivity(), user).execute();

                // redirect to main menu
                Intent intent = new Intent(getActivity(),
                        MainMenuDrawer.class);
                getActivity().finish();
                startActivity(intent);

            }

        }

        @Override
        public void onFail() {
            showToast("Fail to login..!");
        }
    };

    private void intagramLogin() {
        final InstagramApp mApp = new InstagramApp(getActivity(), Common.CLIENT_ID,
                Common.CLIENT_SECRET, Common.CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {
// tvSummary.setText("Connected as " + mApp.getUserName());
// userInfoHashmap = mApp.
                mApp.fetchUserName();
            }

            @Override
            public void onFail(String error) {
                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_sin_in;
    }


    private void setTextInputError(int tilId, String string) {
        ((TextInputLayout) findView(tilId)).setError(string);
    }

    private String getTextInputLayout(int textInputLayoutId) {
        return ((TextInputLayout) findView(textInputLayoutId)).getEditText().getText().toString().trim();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
