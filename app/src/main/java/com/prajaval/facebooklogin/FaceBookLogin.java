package com.prajaval.facebooklogin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.prajaval.async.AsyncInserUserDB;
import com.prajaval.async.AsyncLogin;
import com.prajaval.async.IUser;
import com.prajaval.core.User;
import com.prajaval.menudrawer.MainMenuDrawer;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.Permission.Type;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

import java.util.List;

import common.prajaval.common.Common;

public class FaceBookLogin extends FragmentActivity {
    private SimpleFacebook mSimpleFacebook;

    private ProgressDialog dialog;
    TextView textloginpage;
    Button btnFacebookLogin;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.facebooklogin_activity);

        textloginpage = (TextView) findViewById(R.id.textloginpage);
        btnFacebookLogin = (Button) findViewById(R.id.btnFacebookLogin);

        textloginpage.setText(Html.fromHtml("<small>" + "get " + "</small>"
                + "<b>" + " NOTICED" + "</b>" + "<br/>" + "<small>" + "get "
                + "</small>" + "<b>" + " REWARDED" + "</b>" + "<br/>"
                + "<small>" + "for capturing what you " + "</small>" + "<b>"
                + " LOVE" + "</b>" + "<br/>"));
        btnFacebookLogin.setOnClickListener(btnFacebookLoginclick);
    }

    OnClickListener btnFacebookLoginclick = new OnClickListener() {

        @Override
        public void onClick(View v) {

            mSimpleFacebook = SimpleFacebook.getInstance(FaceBookLogin.this);
            mSimpleFacebook.login(onLoginListener);
        }
    };

    OnLoginListener onLoginListener = new OnLoginListener() {

        @Override
        public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
            dialog = ProgressDialog.show(FaceBookLogin.this, "",
                    "Authentication with facebook..", false);
            getProfile();
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onFail(String reason) {
            Toast.makeText(getBaseContext(), reason, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onException(Throwable throwable) {
            Toast.makeText(getBaseContext(), throwable.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onResume() {

        super.onResume();
        // mSimpleFacebook = SimpleFacebook.getInstance(this);
    }

    private void getProfile() {
        mSimpleFacebook.getProfile(new OnProfileListener() {

            @Override
            public void onThinking() {

            }

            @Override
            public void onException(Throwable throwable) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                Toast.makeText(FaceBookLogin.this,
                        "Exception while facebook authentication.",
                        Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFail(String reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(FaceBookLogin.this, reason, Toast.LENGTH_LONG)
                        .show();

            }

            @Override
            public void onComplete(Profile response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                RegisterWithHalla(response);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);

    }

    private void RegisterWithHalla(Profile profile) {
        if (profile != null) {

            String name = profile.getName();
            String email = "";
            if (!TextUtils.isEmpty(profile.getEmail())) {
                email = profile.getEmail();
            }
            String location_name = "";
            if (profile.getLocation() != null && !TextUtils.isEmpty(profile.getLocation().getName())) {
                location_name = profile.getLocation().getName();
            }

            String SocialUserName = profile.getName();
            String id = profile.getId();
            String social_type = "facebook";
            String socialpicture = "http://graph.facebook.com/" + id
                    + "/picture?type=large";

            double lattitude = 0.0;
            double longitude = 0.0;
            if (Common.LOCATION != null) {
                lattitude = Common.LOCATION.getLatitude();
                longitude = Common.LOCATION.getLongitude();
            }

            AsyncLogin asyncLogin = new AsyncLogin(FaceBookLogin.this, id,
                    name, SocialUserName, email, socialpicture, social_type,
                    location_name, lattitude, longitude, iUser);
            asyncLogin.execute();
        }
    }

    IUser iUser = new IUser() {

        @Override
        public void ITaskCompleted(User user) {

            if (user != null) {

                Common.USER = user;

                // insert in db
                new AsyncInserUserDB(FaceBookLogin.this, user).execute();

                // redirect to main menu
                Intent intent = new Intent(FaceBookLogin.this,
                        MainMenuDrawer.class);
                FaceBookLogin.this.finish();
                startActivity(intent);

            }

        }

        @Override
        public void onFail() {

        }
    };

}
