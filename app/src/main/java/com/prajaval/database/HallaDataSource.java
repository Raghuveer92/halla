package com.prajaval.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.prajaval.core.Contest;
import com.prajaval.core.User;

public class HallaDataSource {

	private SQLiteDatabase database;

	private HallaSQLiteOpenHelper dbHelper;

	private String[] UserColumns = { HallaSQLiteOpenHelper.COLUMN_ID,
			HallaSQLiteOpenHelper.COLUNM_USERID,
			HallaSQLiteOpenHelper.COLUNM_SOCIALID,
			HallaSQLiteOpenHelper.COLUNM_SOCIALUSERNAME,
			HallaSQLiteOpenHelper.COLUMN_USERNAME,
			HallaSQLiteOpenHelper.COLUMN_EMAIL,
			HallaSQLiteOpenHelper.COLUMN_USER_REGISTERED,
			HallaSQLiteOpenHelper.COLUMN_USER_STATUS,
			HallaSQLiteOpenHelper.COLUMN_USER__TYPE,
			HallaSQLiteOpenHelper.COLUMN_USER_IMAGE,
			HallaSQLiteOpenHelper.COLUMN_USER_POINTS };

	private String[] PreviousContestColumns = {
			HallaSQLiteOpenHelper.COLUMN_ID,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_ID,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_NO_OF_WINNER,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_ENTRIES,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_VOTE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_FLAG,
			HallaSQLiteOpenHelper.COLUNM_CONTEST_TITLE,
			HallaSQLiteOpenHelper.COLUNM_CONTEST_DESC,
			HallaSQLiteOpenHelper.COLUNM_CONTEST_IMAGE,
			HallaSQLiteOpenHelper.COLUNM_CONTEST_THUMBIMAGE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_STARTDATE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_ENDDATE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_STATUS,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_PRIZE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_SUMMARY,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_HINT,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_TYPE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_IMAGECREDIT,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_CREDITURL,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_SPONSORNAME,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_SPONSORLOGO,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_SPONSORURL,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_WINNER,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_NO_OF_ENTRIES };

	private String[] OpensContestColumns = { HallaSQLiteOpenHelper.COLUMN_ID,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_ID,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_NO_OF_WINNER,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_ENTRIES,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_VOTE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_FLAG,
			HallaSQLiteOpenHelper.COLUNM_CONTEST_TITLE,
			HallaSQLiteOpenHelper.COLUNM_CONTEST_DESC,
			HallaSQLiteOpenHelper.COLUNM_CONTEST_IMAGE,
			HallaSQLiteOpenHelper.COLUNM_CONTEST_THUMBIMAGE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_STARTDATE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_ENDDATE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_STATUS,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_PRIZE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_SUMMARY,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_HINT,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_TYPE,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_IMAGECREDIT,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_CREDITURL,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_SPONSORNAME,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_SPONSORLOGO,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_SPONSORURL,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_WINNER,
			HallaSQLiteOpenHelper.COLUMN_CONTEST_NO_OF_ENTRIES };

	public HallaDataSource(Context context) {
		dbHelper = new HallaSQLiteOpenHelper(context);
	}

	public void open() {
		try {
			database = dbHelper.getWritableDatabase();
		} catch (Exception e) {
			e.toString();
		}

	}

	public void close() {
		dbHelper.close();
	}

	// -------------- user ---------------------
	public long insertUser(User user) {

		ContentValues values = new ContentValues();
		values.put(HallaSQLiteOpenHelper.COLUNM_USERID,
				String.valueOf(user.getiUserID()));
		values.put(HallaSQLiteOpenHelper.COLUNM_SOCIALID,
				String.valueOf(user.getSocial_ID()));
		values.put(HallaSQLiteOpenHelper.COLUNM_SOCIALUSERNAME,
				user.getStrsocial_UserName());
		values.put(HallaSQLiteOpenHelper.COLUMN_USERNAME, user.getUserName());
		values.put(HallaSQLiteOpenHelper.COLUMN_EMAIL, user.getEmail());
		values.put(HallaSQLiteOpenHelper.COLUMN_USER_REGISTERED,
				user.getUserRegistered());
		values.put(HallaSQLiteOpenHelper.COLUMN_USER_STATUS,
				user.getStrUser_status());
		values.put(HallaSQLiteOpenHelper.COLUMN_USER__TYPE,
				user.getStrUser_type());
		values.put(HallaSQLiteOpenHelper.COLUMN_USER_IMAGE,
				user.getStrUser_image());
		values.put(HallaSQLiteOpenHelper.COLUMN_USER_POINTS,
				user.getStrPoints());

		long id = 0;
		int i = 0;
		try {
			if (IfUserExist()) {
				i = database.update(
						HallaSQLiteOpenHelper.TABLE_USER,
						values,
						HallaSQLiteOpenHelper.COLUNM_USERID + "="
								+ user.getiUserID(), null);
			} else {
				id = database.insert(HallaSQLiteOpenHelper.TABLE_USER, null,
						values);
			}
		} catch (Exception io) {
			io.toString();
			io.printStackTrace();
		}
		return id;
	}

	private boolean IfUserExist() {

		Cursor cursor = database.query(HallaSQLiteOpenHelper.TABLE_USER,
				UserColumns, null, null, null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				return true;
			} else {
				return false;
			}

		}
		return false;
	}

	public User GetUser_FromDB() {
		User user = new User();
		try {

			Cursor cursor = database.query(HallaSQLiteOpenHelper.TABLE_USER,
					UserColumns, null, null, null, null, null);

			if (cursor != null) {
				cursor.moveToFirst();
				user = Cursor_to_User(cursor);
			}
			cursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return user;
	}

	public int DeleteUser() {

		try {
			return database
					.delete(HallaSQLiteOpenHelper.TABLE_USER, null, null);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return 0;
	}

	private User Cursor_to_User(Cursor cursor) {
		User user = new User();

		// [id, iUser_ID, iSocial_ID, social_username, user_name,
		// email, user_registered, user_status,
		// user_type, user_points]

		String userid = cursor.getString(1);
		String socialid = cursor.getString(2);
		String susername = cursor.getString(3);
		String username = cursor.getString(4);
		String email = cursor.getString(5);
		String user_reg = cursor.getString(6);
		String user_status = cursor.getString(7);
		String user_type = cursor.getString(8);
		String user_image = cursor.getString(9);
		String user_points = cursor.getString(10);

		user.setiUserID(Integer.parseInt(userid));
		user.setiSocial_ID((Integer.parseInt(socialid)));
		user.setStrsocial_UserName(susername);
		user.setUserName(username);
		user.setEmail(email);
		user.setUserRegistered(user_reg);
		user.setStrUser_status(user_status);
		user.setStrUser_type(user_type);
		user.setStrUser_image(user_image);
		user.setStrPoints(user_points);
		return user;
	}

	// -------- end user -----------------------

	// ----------previous contest ---------------

	public boolean CheckIsDataAlreadyInDBorNot(String fieldValue) {

		String Query = "Select * from "
				+ HallaSQLiteOpenHelper.TABLE_PREVIOUS_CONTEST + " where "
				+ HallaSQLiteOpenHelper.COLUMN_CONTEST_ID + "=" + fieldValue;
		Cursor cursor = database.rawQuery(Query, null);
		int i = cursor.getCount();
		
		if (cursor.getCount() <= 0) {
			return true;
		}
		return false;
	}

	public void insert_previous_contest_list(List<Contest> list) {

		try {
			if (list != null) {

				if (list.size() > 0) {

					for (int i = 0; i < list.size(); i++) {
						insert_previous_contest(list.get(i));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public long insert_previous_contest(Contest contest) {

		ContentValues values = new ContentValues();
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_ID,
				String.valueOf(contest.getiContestId()));
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_NO_OF_WINNER,
				String.valueOf(contest.getInt_ContestNoofWinner()));
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_ENTRIES,
				contest.getiContestEntries());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_VOTE,
				contest.getiContestVote());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_FLAG,
				contest.getIflag());
		values.put(HallaSQLiteOpenHelper.COLUNM_CONTEST_TITLE,
				contest.getStr_ContestTitle());
		values.put(HallaSQLiteOpenHelper.COLUNM_CONTEST_DESC,
				contest.getStr_ContestDesc());
		values.put(HallaSQLiteOpenHelper.COLUNM_CONTEST_IMAGE,
				contest.getStr_ContestImage());
		values.put(HallaSQLiteOpenHelper.COLUNM_CONTEST_THUMBIMAGE,
				contest.getStr_ContestThumbImage());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_STARTDATE,
				contest.getStr_ContestStartDate());

		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_ENDDATE,
				contest.getStr_ContestEndDate());

		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_STATUS,
				contest.getStr_ContestStatus());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_PRIZE,
				contest.getStr_ContestPrize());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_SUMMARY,
				contest.getStr_ContestSummary());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_HINT,
				contest.getStr_ContestHint());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_TYPE,
				contest.getStr_ContestType());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_IMAGECREDIT,
				contest.getStr_ContestImageCredit());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_CREDITURL,
				contest.getStr_ContestImageCreditUrl());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_SPONSORNAME,
				contest.getStr_ContestSponsorName());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_SPONSORLOGO,
				contest.getStr_ContestSponsorLogo());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_SPONSORURL,
				contest.getStr_ContestSponsorUrl());

		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_WINNER,
				contest.getStr_ContestWinner());
		values.put(HallaSQLiteOpenHelper.COLUMN_CONTEST_NO_OF_ENTRIES,
				contest.getStr_ContestNoOfEntries());
		long id = 0;
		try {

			if (CheckIsDataAlreadyInDBorNot(String.valueOf(contest
					.getiContestId()))) {
				id = database.insert(
						HallaSQLiteOpenHelper.TABLE_PREVIOUS_CONTEST, null,
						values);

				Log.d("previous contest in DB", "" + id);

			} else {
				Log.d("Data in previous DB",
						"prev_contest id " + contest.getiContestId());
			}

		} catch (Exception io) {
			io.toString();
			io.printStackTrace();
		}
		return id;
	}

	public List<Contest> Get_previous_contest_from_db() {

		Contest contest;
		List<Contest> list_prevcontest = new ArrayList<Contest>();

		try {

			Cursor cursor = database.query(
					HallaSQLiteOpenHelper.TABLE_PREVIOUS_CONTEST,
					PreviousContestColumns, null, null, null, null, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						contest = new Contest();
						contest = Cursor_to_Contest(cursor);
						list_prevcontest.add(contest);
					} while (cursor.moveToNext());
				}

				Log.d("get previous contest", "" + list_prevcontest.size());

			}
			cursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list_prevcontest;

	}

	private Contest Cursor_to_Contest(Cursor cursor) {

		Contest contest = new Contest();

		try {
			String COLUMN_CONTEST_ID = cursor.getString(1);
			String COLUMN_CONTEST_NO_OF_WINNER = cursor.getString(2);
			String COLUMN_CONTEST_ENTRIES = cursor.getString(3);
			String COLUMN_CONTEST_VOTE = cursor.getString(4);
			String COLUMN_CONTEST_FLAG = cursor.getString(5);
			String COLUNM_CONTEST_TITLE = cursor.getString(6);
			String COLUNM_CONTEST_DESC = cursor.getString(7);
			String COLUNM_CONTEST_IMAGE = cursor.getString(8);
			String COLUNM_CONTEST_THUMBIMAGE = cursor.getString(9);
			String COLUMN_CONTEST_STARTDATE = cursor.getString(10);

			String COLUMN_CONTEST_ENDDATE = cursor.getString(11);
			String COLUMN_CONTEST_STATUS = cursor.getString(12);
			String COLUMN_CONTEST_PRIZE = cursor.getString(13);
			String COLUMN_CONTEST_SUMMARY = cursor.getString(14);
			String COLUMN_CONTEST_HINT = cursor.getString(15);
			String COLUMN_CONTEST_TYPE = cursor.getString(16);
			String COLUMN_CONTEST_IMAGECREDIT = cursor.getString(17);
			String COLUMN_CONTEST_CREDITURL = cursor.getString(18);

			String COLUMN_CONTEST_SPONSORNAME = cursor.getString(19);
			String COLUMN_CONTEST_SPONSORLOGO = cursor.getString(20);
			String COLUMN_CONTEST_SPONSORURL = cursor.getString(21);
			String COLUMN_CONTEST_WINNER = cursor.getString(22);
			String COLUMN_CONTEST_NO_OF_ENTRIES = cursor.getString(23);

			contest.setiContestId(Integer.parseInt(COLUMN_CONTEST_ID));
			contest.setInt_ContestNoofWinner((Integer
					.parseInt(COLUMN_CONTEST_NO_OF_WINNER)));
			contest.setStr_ContestNoOfEntries(COLUMN_CONTEST_ENTRIES);

			contest.setiContestVote(Integer.parseInt(COLUMN_CONTEST_VOTE));
			contest.setIflag(Integer.parseInt(COLUMN_CONTEST_FLAG));
			contest.setStr_ContestTitle(COLUNM_CONTEST_TITLE);
			contest.setStr_ContestDesc(COLUNM_CONTEST_DESC);

			contest.setStr_ContestImage(COLUNM_CONTEST_IMAGE);
			contest.setContestThumbImage(COLUNM_CONTEST_THUMBIMAGE);
			contest.setStr_ContestStartDate(COLUMN_CONTEST_STARTDATE);
			contest.setStr_ContestEndDate(COLUMN_CONTEST_ENDDATE);
			contest.setStr_ContestStatus(COLUMN_CONTEST_STATUS);
			contest.setStr_ContestPrize(COLUMN_CONTEST_PRIZE);
			contest.setStr_ContestSummary(COLUMN_CONTEST_SUMMARY);
			contest.setStr_ContestHint(COLUMN_CONTEST_HINT);
			contest.setStr_ContestType(COLUMN_CONTEST_TYPE);
			contest.setStr_ContestImageCredit(COLUMN_CONTEST_IMAGECREDIT);
			contest.setStr_ContestImageCreditUrl(COLUMN_CONTEST_CREDITURL);

			contest.setStr_ContestSponsorName(COLUMN_CONTEST_SPONSORNAME);
			contest.setStr_ContestSponsorLogo(COLUMN_CONTEST_SPONSORLOGO);
			contest.setStr_ContestSponsorUrl(COLUMN_CONTEST_SPONSORURL);
			contest.setStr_ContestWinner(COLUMN_CONTEST_WINNER);
			contest.setStr_ContestNoOfEntries(COLUMN_CONTEST_NO_OF_ENTRIES);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return contest;
	}
	// ---------end contest --------------------
}
