package com.prajaval.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class HallaSQLiteOpenHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "Halla";
	private static final int DATABASE_VERSION = 1;

	public static final String COLUMN_ID = "id";
	// Table User and column names
	public static final String TABLE_USER = "user";
	public static final String COLUNM_USERID = "iUser_ID";
	public static final String COLUNM_SOCIALID = "iSocial_ID";
	public static final String COLUNM_SOCIALUSERNAME = "social_username";
	public static final String COLUMN_USERNAME = "user_name";
	public static final String COLUMN_EMAIL = "email";
	public static final String COLUMN_USER_REGISTERED = "user_registered";

	public static final String COLUMN_USER_STATUS = "user_status";
	public static final String COLUMN_USER__TYPE = "user_type";
	public static final String COLUMN_USER_IMAGE = "user_image";
	public static final String COLUMN_USER_POINTS = "user_points";

	// Table Open contest and column names
	public static final String TABLE_OPEN_CONTEST = "open_contest";
	public static final String TABLE_PREVIOUS_CONTEST = "previous_contest";

	public static final String COLUNM_CONTEST_TITLE = "contest_title";
	public static final String COLUNM_CONTEST_DESC = "contest_desc";
	public static final String COLUNM_CONTEST_IMAGE = "contest_image";
	public static final String COLUNM_CONTEST_THUMBIMAGE = "contest_thumimage";
	public static final String COLUMN_CONTEST_STARTDATE = "contest_startdate";
	public static final String COLUMN_CONTEST_ENDDATE = "contest_enddate";
	public static final String COLUMN_CONTEST_STATUS = "contest_status";

	public static final String COLUMN_CONTEST_PRIZE = "contest_prize";
	public static final String COLUMN_CONTEST_SUMMARY = "contest_summary";
	public static final String COLUMN_CONTEST_HINT = "contest_hint";
	public static final String COLUMN_CONTEST_TYPE = "contest_type";

	public static final String COLUMN_CONTEST_IMAGECREDIT = "contest_imagecredit";
	public static final String COLUMN_CONTEST_CREDITURL = "contest_crediturl";
	public static final String COLUMN_CONTEST_SPONSORNAME = "contest_sponsorname";
	public static final String COLUMN_CONTEST_SPONSORLOGO = "contest_sponsorlogo";
	public static final String COLUMN_CONTEST_SPONSORURL = "contest_sponsorurl";
	public static final String COLUMN_CONTEST_WINNER = "contest_winner";
	public static final String COLUMN_CONTEST_NO_OF_ENTRIES = "contest_noofentries";

	public static final String COLUMN_CONTEST_ID = "contest_id";
	public static final String COLUMN_CONTEST_NO_OF_WINNER = "contest_noofwinners";
	public static final String COLUMN_CONTEST_ENTRIES = "contest_entries";
	public static final String COLUMN_CONTEST_VOTE = "contest_vote";
	public static final String COLUMN_CONTEST_FLAG = "contest_flag";

	public HallaSQLiteOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase database) {

		database.execSQL(CREATE_TABLE_USER);
		database.execSQL(CREATE_TABLE_OPEN_CONTEST);

		database.execSQL(CREATE_TABLE_PREVIOUS_CONTEST);

		Log.d("create table", CREATE_TABLE_USER);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PREVIOUS_CONTEST);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_OPEN_CONTEST);
	}

	private static final String CREATE_TABLE_USER = "create table "
			+ TABLE_USER + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUNM_USERID
			+ " text not null, " + COLUNM_SOCIALID + " text , "
			+ COLUNM_SOCIALUSERNAME + " text , " + COLUMN_USERNAME
			+ " text not null, " + COLUMN_EMAIL + " text not null, "
			+ COLUMN_USER_REGISTERED + " text null, " + COLUMN_USER_STATUS
			+ " text null, " + COLUMN_USER__TYPE + " text null, "
			+ COLUMN_USER_IMAGE + " text , " + COLUMN_USER_POINTS
			+ " text null);";

	private static final String CREATE_TABLE_PREVIOUS_CONTEST = "create table "
			+ TABLE_PREVIOUS_CONTEST + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_CONTEST_ID
			+ " text not null, " + COLUMN_CONTEST_NO_OF_WINNER + " text , "
			+ COLUMN_CONTEST_ENTRIES + " text , " + COLUMN_CONTEST_VOTE
			+ " text not null, " + COLUMN_CONTEST_FLAG + " text not null, "
			+ COLUNM_CONTEST_TITLE + " text null, " + COLUNM_CONTEST_DESC
			+ " text null, " + COLUNM_CONTEST_IMAGE + " text null, "
			+ COLUNM_CONTEST_THUMBIMAGE + " text , " + COLUMN_CONTEST_STARTDATE
			+ " text null , " + COLUMN_CONTEST_ENDDATE + " text null , "
			+ COLUMN_CONTEST_STATUS + " text null , " + COLUMN_CONTEST_PRIZE
			+ " text null , " + COLUMN_CONTEST_SUMMARY + " text null , "
			+ COLUMN_CONTEST_HINT + " text null , " + COLUMN_CONTEST_TYPE
			+ " text null , " + COLUMN_CONTEST_IMAGECREDIT + " text null , "
			+ COLUMN_CONTEST_CREDITURL + " text null , "
			+ COLUMN_CONTEST_SPONSORNAME + " text null , "
			+ COLUMN_CONTEST_SPONSORLOGO + " text null , "
			+ COLUMN_CONTEST_SPONSORURL + " text null , "
			+ COLUMN_CONTEST_WINNER + " text null , "
			+ COLUMN_CONTEST_NO_OF_ENTRIES + " text null);";

	private static final String CREATE_TABLE_OPEN_CONTEST = "create table "
			+ TABLE_OPEN_CONTEST + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_CONTEST_ID
			+ " text not null, " + COLUMN_CONTEST_NO_OF_WINNER + " text , "
			+ COLUMN_CONTEST_ENTRIES + " text , " + COLUMN_CONTEST_VOTE
			+ " text not null, " + COLUMN_CONTEST_FLAG + " text not null, "
			+ COLUNM_CONTEST_TITLE + " text null, " + COLUNM_CONTEST_DESC
			+ " text null, " + COLUNM_CONTEST_IMAGE + " text null, "
			+ COLUNM_CONTEST_THUMBIMAGE + " text , " + COLUMN_CONTEST_STARTDATE
			+ " text null , " + COLUMN_CONTEST_ENDDATE + " text null , "
			+ COLUMN_CONTEST_STATUS + " text null , " + COLUMN_CONTEST_PRIZE
			+ " text null , " + COLUMN_CONTEST_SUMMARY + " text null , "
			+ COLUMN_CONTEST_HINT + " text null , " + COLUMN_CONTEST_TYPE
			+ " text null , " + COLUMN_CONTEST_IMAGECREDIT + " text null , "
			+ COLUMN_CONTEST_CREDITURL + " text null , "
			+ COLUMN_CONTEST_SPONSORNAME + " text null , "
			+ COLUMN_CONTEST_SPONSORLOGO + " text null , "
			+ COLUMN_CONTEST_SPONSORURL + " text null , "
			+ COLUMN_CONTEST_WINNER + " text null , "
			+ COLUMN_CONTEST_NO_OF_ENTRIES + " text null);";

}
