package com.prajaval.fragment.opencontest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.core.Contest;
import com.squareup.picasso.Picasso;

@SuppressLint("InflateParams")
public class OpenContestListAdapter extends BaseAdapter {

    Context context;
    List<Contest> listContest;
    DisplayImageOptions options;

    public OpenContestListAdapter(Context context, List<Contest> listcontest) {
        this.context = context;
        this.listContest = listcontest;

        options = new DisplayImageOptions.Builder()

                .resetViewBeforeLoading(true).cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.ARGB_8888).considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .displayer(new RoundedBitmapDisplayer(3)).build();
    }

    @Override
    public int getCount() {

        return listContest.size();
    }

    @Override
    public Contest getItem(int position) {

        return listContest.get(position);
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolderOpenContest viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.card_open_contest,
                    null);

            viewHolder = new ViewHolderOpenContest();

            viewHolder.imgOpenContest = (ImageView) convertView
                    .findViewById(R.id.img_contest);
            viewHolder.txtOpenContestName = (TextView) convertView
                    .findViewById(R.id.txt_contest_name);
            viewHolder.txt_contest_details = (TextView) convertView
                    .findViewById(R.id.txt_contest_details);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolderOpenContest) convertView.getTag();
        }

        // Binding
        Contest contests = listContest.get(position);

        viewHolder.txtOpenContestName.setText(contests.getStr_ContestTitle());
        viewHolder.txt_contest_details.setText(DaysLeft(contests));
//		viewHolder.txt_sponsors.setText(contests.getStr_ContestSponsorName());
//
//		if (!contests.getStr_ContestType().equals("image")) {
//			viewHolder.img_opencontest_type.setImageDrawable(context
//					.getResources().getDrawable(R.drawable.video));
//		} else {
//
//			viewHolder.img_opencontest_type.setImageDrawable(context
//					.getResources().getDrawable(R.drawable.btn_camera));
//		}
        if (!TextUtils.isEmpty(contests.getStr_ContestThumbImage()))
            Picasso.with(context).load(contests.getStr_ContestThumbImage()).into(viewHolder.imgOpenContest);
		ImageLoader.getInstance().displayImage(
				contests.getStr_ContestThumbImage(), viewHolder.imgOpenContest,
				options, new SimpleImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {

					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {

						viewHolder.imgOpenContest.setImageDrawable(context
								.getResources().getDrawable(
										R.drawable.img_not_found_thumb));

					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {

					}
				});

        return convertView;
    }

    @SuppressLint("SimpleDateFormat")
    private String DaysLeft(Contest contest) {

        long diffDays = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Date date = new Date();
        String sDate = new SimpleDateFormat("yyyy-MM-dd").format(date);

        String eDate = contest.getStr_ContestEndDate();
        try {
            Date d1 = formatter.parse(sDate);
            Date d2 = formatter.parse(eDate);

            long diff = d2.getTime() - d1.getTime();
            diffDays = diff / (24 * 60 * 60 * 1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return contest.getiContestEntries() + " Entries | " + diffDays
                + " days to go";
    }

    private class ViewHolderOpenContest {

        ImageView imgOpenContest;
        TextView txtOpenContestName;
        TextView txt_contest_details;
//        TextView txt_sponsors;
    }
}
