package com.prajaval.fragment.opencontest;

import java.util.List;

import com.prajaval.core.Contest;

public interface IOpenContestList {

	void OpenContestList(List<Contest> _opencontestlist);
}
