package com.prajaval.fragment.opencontest;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.json.classes.JSONArray;
import com.prajaval.core.Contest;
import com.prajaval.utility.CustomProgressDialog;
import common.prajaval.common.CommonURL;

public class OpenContestController extends AsyncTask<Void, Void, List<Contest>> {

	final String CONTEST_LIST = "http://halla.in/api/contest";
	Context context;
	IOpenContestList openContestList;
	List<Contest> listOpenContest;
	ProgressDialog dialog;

	public OpenContestController(Context context,
			IOpenContestList openContestList) {
		this.context = context;
		this.openContestList = openContestList;

	}

	@Override
	protected void onPreExecute() {
		dialog = CustomProgressDialog.ctor(this.context);
		dialog.show();

	}

	@Override
	protected List<Contest> doInBackground(Void... params) {

		String jsonContestList = CommonURL.Execute(CONTEST_LIST, null, null);

		if (jsonContestList != null) {
			return listOpenContest = GetContestList(jsonContestList);
		}

		return null;
	}

	@Override
	protected void onPostExecute(List<Contest> result) {

		openContestList.OpenContestList(result);
		if (dialog != null) {
			dialog.hide();
		}
	}

	private List<Contest> GetContestList(String JsonString) {
		List<Contest> listOpenContest = new ArrayList<Contest>();
		try {

			JSONArray jsonArray = new JSONArray(JsonString);
			for (int i = 0; i < jsonArray.length(); i++) {
				Contest contest = Contest
						.GetContest(jsonArray.getJSONObject(i));
				if (listOpenContest == null) {
					listOpenContest = new ArrayList<Contest>();
				}
				listOpenContest.add(contest);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

		return listOpenContest;
	}

}
