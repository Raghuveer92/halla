package com.prajaval.fragment.opencontest;

import java.util.List;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.prajaval.core.Contest;
import com.prajaval.core.User;
import com.prajaval.opencontestsponsors_activity.OpenContestSponsors_Activity;

import common.prajaval.common.Common;

public class OpenContestFragment extends Fragment implements
        ListView.OnItemClickListener, IOpenContestList {

    OpenContestListAdapter openContestListAdapter;
    ListView lvOpenContest;

    public OpenContestFragment() {

    }

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        OpenContestController openContestController = new OpenContestController(
                getActivity(), this);
        openContestController.execute();

        try {
            if (savedInstanceState != null) {
                if (savedInstanceState.getSerializable(Common.USER_VALUE) != null)
                    Common.USER = (User) savedInstanceState
                            .getSerializable(Common.USER_VALUE);
            }
        } catch (Exception e) {
        }

    }

    ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.opencontest_fragment,
                container, false);

        lvOpenContest = (ListView) rootView.findViewById(R.id.lv_opencontest);

        return rootView;
    }

    @Override
    public void OpenContestList(List<Contest> _opencontestlist) {

        try {
            if (_opencontestlist != null) {
                openContestListAdapter = new OpenContestListAdapter(
                        getActivity(), _opencontestlist);
                lvOpenContest.setAdapter(openContestListAdapter);
                lvOpenContest.setOnItemClickListener(this);
            } else {
                Toast.makeText(
                        getActivity(),
                        "Oops.! Something went wrong. \nCheck your Internet Connection.",
                        Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {

        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,
                            int position, long arg3) {

        try {
            Contest contest = openContestListAdapter.getItem(position);

            Intent intent = new Intent(getActivity(),
                    OpenContestSponsors_Activity.class);
            intent.putExtra(Common.SELECTED_CONTEST, contest);
            startActivity(intent);
        } catch (Exception e) {

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(Common.USER_VALUE, Common.USER);
        super.onSaveInstanceState(outState);

        // save data.

    }

    @Override
    public void onStart() {

        super.onStart();
    }

    @Override
    public void onStop() {

        super.onStop();
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();
    }
}
