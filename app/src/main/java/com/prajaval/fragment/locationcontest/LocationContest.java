package com.prajaval.fragment.locationcontest;

import java.util.List;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.prajaval.core.Contest;
import com.prajaval.core.User;
import com.prajaval.fragment.opencontest.IOpenContestList;
import com.prajaval.locationsponsors_activity.LocationContestSponsors_Activity;
import com.prajaval.opencontestsponsors_activity.OpenContestSponsors_Activity;

import common.prajaval.common.Common;

public class LocationContest extends Fragment implements IOpenContestList,
		OnItemClickListener {

	ImageView imglocation;
	TextView txttemp;
	ListView lv_locationcontest;
	List<Contest> listLocation;
	LocationContestListAdapter adapter;

	public LocationContest() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		LocationContestController locationContestController = new LocationContestController(
				getActivity(), this);
		locationContestController.execute();

		try {
			if (savedInstanceState != null) {
				if (savedInstanceState.getSerializable(Common.USER_VALUE) != null)
					Common.USER = (User) savedInstanceState
							.getSerializable(Common.USER_VALUE);

			}

		} catch (Exception e) {

		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.locationcontest_fragment,
				container, false);
		lv_locationcontest = (ListView) rootView
				.findViewById(R.id.lv_locationcontest);
		lv_locationcontest.setOnItemClickListener(this);
		txttemp = (TextView) rootView.findViewById(R.id.txttemp);
		imglocation = (ImageView) rootView.findViewById(R.id.imglocation);

		return rootView;

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		try {

			Contest contest = adapter.getItem(arg2);
			Intent intent = new Intent(getActivity(),
					LocationContestSponsors_Activity.class);
			intent.putExtra(Common.SELECTED_LOCATION_CONTEST, contest);
			startActivity(intent);
		} catch (Exception e) {

		}
	}

	@Override
	public void OpenContestList(List<Contest> _opencontestlist) {

		if (_opencontestlist != null) {

			if (_opencontestlist.size() > 0) {
				lv_locationcontest.setVisibility(View.VISIBLE);
				txttemp.setVisibility(View.GONE);
				imglocation.setVisibility(View.GONE);

				listLocation = _opencontestlist;
				adapter = new LocationContestListAdapter(getActivity(),
						listLocation);
				lv_locationcontest.setAdapter(adapter);
			} else {
				lv_locationcontest.setVisibility(View.GONE);
				txttemp.setVisibility(View.VISIBLE);
				imglocation.setVisibility(View.VISIBLE);

			}

		} else {
			lv_locationcontest.setVisibility(View.GONE);
			txttemp.setVisibility(View.GONE);
			imglocation.setVisibility(View.GONE);
			Toast.makeText(
					getActivity(),
					"Oops.! Something went wrong. \nCheck your Internet Connection.",
					Toast.LENGTH_LONG).show();

		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable(Common.USER_VALUE, Common.USER);
		super.onSaveInstanceState(outState);
	}
}
