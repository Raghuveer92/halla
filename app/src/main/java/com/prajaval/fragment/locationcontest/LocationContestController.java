package com.prajaval.fragment.locationcontest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.json.classes.JSONArray;
import com.prajaval.core.Contest;
import com.prajaval.fragment.opencontest.IOpenContestList;
import com.prajaval.utility.CustomProgressDialog;

import common.prajaval.common.Common;

public class LocationContestController extends
		AsyncTask<Void, Void, List<Contest>> {

	final String LOCATION_CONTEST_LIST = "http://halla.in/api//v1/location_contests";
	Context context;
	IOpenContestList locationContestList;

	ProgressDialog dialog;

	public LocationContestController(Context context,
			IOpenContestList LocationContest) {

		this.context = context;
		this.locationContestList = LocationContest;

	}

	@Override
	protected void onPreExecute() {
		dialog = CustomProgressDialog.ctor(this.context);
		dialog.show();
	}

	@Override
	protected List<Contest> doInBackground(Void... params) {

		List<Contest> list = new ArrayList<Contest>();
		try {

			HttpPost httppost = new HttpPost(LOCATION_CONTEST_LIST);
			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			HttpClient httpClient = new DefaultHttpClient();

			try {

				StringBody userID = new StringBody(String.valueOf(Common.USER
						.getiUserID()));

				entity.addPart("userId", userID);

			} catch (Exception e) {
				e.printStackTrace();
			}

			httppost.setEntity(entity);

			try {
				HttpResponse response = httpClient.execute(httppost);
				BufferedReader in = null;
				try {
					in = new BufferedReader(new InputStreamReader(response
							.getEntity().getContent(), "UTF-8"));
					StringBuffer sb = new StringBuffer("");
					String line = "";
					String NL = System.getProperty("line.separator");
					while ((line = in.readLine()) != null) {
						sb.append(line + NL);
					}
					in.close();
					list = GetContestList(sb.toString());
				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	protected void onPostExecute(List<Contest> result) {
		locationContestList.OpenContestList(result);
		if (dialog != null) {
			dialog.hide();
		}

	}

	private List<Contest> GetContestList(String JsonString) {
		List<Contest> listOpenContest = new ArrayList<Contest>();
		try {

			JSONArray jsonArray = new JSONArray(JsonString);
			for (int i = 0; i < jsonArray.length(); i++) {
				Contest contest = Contest
						.GetContest(jsonArray.getJSONObject(i));
				if (listOpenContest == null) {
					listOpenContest = new ArrayList<Contest>();
				}
				listOpenContest.add(contest);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

		return listOpenContest;
	}

}
