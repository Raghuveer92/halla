package com.prajaval.fragment.locationcontest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.core.Contest;
import com.prajaval.utility.SquareImageView;

@SuppressLint("InflateParams")
public class LocationContestListAdapter extends BaseAdapter {

	Context context;
	List<Contest> listContest;
	DisplayImageOptions options;

	public LocationContestListAdapter(Context context, List<Contest> listcontest) {
		this.context = context;
		this.listContest = listcontest;

		options = new DisplayImageOptions.Builder()

		.resetViewBeforeLoading(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.ARGB_8888).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300))
				.displayer(new RoundedBitmapDisplayer(3)).build();
	}

	@Override
	public int getCount() {

		return listContest.size();
	}

	@Override
	public Contest getItem(int position) {

		return listContest.get(position);
	}

	@Override
	public long getItemId(int position) {

		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolderOpenContest viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(
					R.layout.locationcontestlistview_item, null);

			viewHolder = new ViewHolderOpenContest();

			viewHolder.img_locationcontest = (SquareImageView) convertView
					.findViewById(R.id.img_locationcontest);
			viewHolder.txt_locationcontest_name = (TextView) convertView
					.findViewById(R.id.txt_locationcontest_name);
			viewHolder.txt_location = (TextView) convertView
					.findViewById(R.id.txt_location);
			viewHolder.txt_locationcontest_details = (TextView) convertView
					.findViewById(R.id.txt_locationcontest_details);
			viewHolder.txt_locationsponsors = (TextView) convertView
					.findViewById(R.id.txt_locationsponsors);
			viewHolder.img_locationcontest_type = (ImageView) convertView
					.findViewById(R.id.img_locationcontest_type);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolderOpenContest) convertView.getTag();
		}

		// Binding
		Contest contests = listContest.get(position);

		viewHolder.txt_locationcontest_name.setText(contests
				.getStr_ContestTitle());
		viewHolder.txt_locationcontest_details.setText(DaysLeft(contests));
		viewHolder.txt_locationsponsors.setText(contests
				.getStr_ContestSponsorName());
		viewHolder.txt_location.setText(contests.getStr_Contestlocation());
		if (!contests.getStr_ContestType().equals("image")) {
			viewHolder.img_locationcontest_type.setImageDrawable(context
					.getResources().getDrawable(R.drawable.video));
		} else {

			viewHolder.img_locationcontest_type.setImageDrawable(context
					.getResources().getDrawable(R.drawable.btn_camera));
		}

		ImageLoader.getInstance().displayImage(
				contests.getStr_ContestThumbImage(),
				viewHolder.img_locationcontest, options,
				new SimpleImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {

					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {

						viewHolder.img_locationcontest.setImageDrawable(context
								.getResources().getDrawable(
										R.drawable.img_not_found_thumb));

					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {
						viewHolder.img_locationcontest
								.setImageBitmap(loadedImage);
					}
				});

		return convertView;
	}

	@SuppressLint("SimpleDateFormat")
	private String DaysLeft(Contest contest) {

		long diffDays = 0;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		Date date = new Date();
		String sDate = new SimpleDateFormat("yyyy-MM-dd").format(date);

		String eDate = contest.getStr_ContestEndDate();
		try {
			Date d1 = formatter.parse(sDate);
			Date d2 = formatter.parse(eDate);

			long diff = d2.getTime() - d1.getTime();
			diffDays = diff / (24 * 60 * 60 * 1000);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return contest.getiContestEntries() + " Entries, " + diffDays
				+ " days to go";
	}

	private class ViewHolderOpenContest {

		SquareImageView img_locationcontest;
		ImageView img_locationcontest_type;
		TextView txt_locationcontest_name;
		TextView txt_location;
		TextView txt_locationcontest_details;
		TextView txt_locationsponsors;
	}
}
