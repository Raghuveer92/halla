package com.prajaval.fragment.upcomingcontest;

import java.util.List;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.prajaval.core.Contest;
import com.prajaval.core.User;
import com.prajaval.fragment.opencontest.IOpenContestList;
import com.prajaval.upcomingcontestsponsors_activity.UpcomingContestSponsorsActivity;

import common.prajaval.common.Common;

public class UpcomingContest extends Fragment implements OnItemClickListener {

	ListView lv_upcomingcontest;
	List<Contest> upcominglist;
	UpcomingContestListAdapter upcomingContestListAdapter;
	TextView txtNoUpcomingContest;
	ImageView img_noUpcmg;

	public UpcomingContest() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		call_upcomingcontest();

		try {
			if (savedInstanceState != null) {
				if (savedInstanceState.getSerializable(Common.USER_VALUE) != null)
					Common.USER = (User) savedInstanceState
							.getSerializable(Common.USER_VALUE);
			}
		} catch (Exception e) {
		}
	}

	private void call_upcomingcontest() {

		UpcomingContestController upcomingContestController = new UpcomingContestController(
				getActivity(), iUpcomingContestlist);
		upcomingContestController.execute();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable(Common.USER_VALUE, Common.USER);
		super.onSaveInstanceState(outState);

	}

	IOpenContestList iUpcomingContestlist = new IOpenContestList() {

		@Override
		public void OpenContestList(List<Contest> _upcominglist) {
			try {
				if (_upcominglist != null) {

					if (_upcominglist.size() > 0) {
						upcominglist = _upcominglist;
						upcomingContestListAdapter = new UpcomingContestListAdapter(
								getActivity(), upcominglist);
						lv_upcomingcontest
								.setAdapter(upcomingContestListAdapter);

						txtNoUpcomingContest.setVisibility(View.GONE);
						img_noUpcmg.setVisibility(View.GONE);
						lv_upcomingcontest.setVisibility(View.VISIBLE);
					} else {
						txtNoUpcomingContest.setVisibility(View.VISIBLE);
						img_noUpcmg.setVisibility(View.VISIBLE);
						lv_upcomingcontest.setVisibility(View.GONE);
					}

				} else {
					txtNoUpcomingContest.setVisibility(View.VISIBLE);
					img_noUpcmg.setVisibility(View.VISIBLE);
					lv_upcomingcontest.setVisibility(View.GONE);

					Toast.makeText(
							getActivity(),
							"Oops.! Something went wrong. \nCheck your Internet Connection.",
							Toast.LENGTH_LONG).show();

				}
			} catch (Exception e) {

			}
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.upcomingcontest_fragment,
				container, false);
		lv_upcomingcontest = (ListView) rootView
				.findViewById(R.id.lv_upcomingcontest);
		txtNoUpcomingContest = (TextView) rootView
				.findViewById(R.id.txtNoUpcomingContest);
		img_noUpcmg = (ImageView) rootView.findViewById(R.id.img_noUpcmg);
		lv_upcomingcontest.setOnItemClickListener(this);

		return rootView;
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long arg3) {

		try {
			Contest contest = (Contest) upcomingContestListAdapter
					.getItem(position);

			Intent intent = new Intent(getActivity(),
					UpcomingContestSponsorsActivity.class);
			intent.putExtra(Common.SELECTED_CONTEST, contest);
			startActivity(intent);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
