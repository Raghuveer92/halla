package com.prajaval.fragment.upcomingcontest;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.json.classes.JSONArray;
import com.prajaval.core.Contest;
import com.prajaval.fragment.opencontest.IOpenContestList;
import com.prajaval.utility.CustomProgressDialog;

import common.prajaval.common.CommonURL;

public class UpcomingContestController extends
		AsyncTask<Void, Void, List<Contest>> {

	final String UPCOMING_CONTEST = "http://halla.in/api/v1/upcoming_contest";

	Context context;
	IOpenContestList iOpenContestList;
	ProgressDialog dialog;

	public UpcomingContestController(Context context, IOpenContestList iOpen) {
		this.context = context;
		this.iOpenContestList = iOpen;
		
	}

	@Override
	protected void onPreExecute() {
		dialog = CustomProgressDialog.ctor(context);
		dialog.show();
	}

	@Override
	protected List<Contest> doInBackground(Void... params) {
		List<Contest> list = new ArrayList<Contest>();
		String json_contest = CommonURL
				.Execute(UPCOMING_CONTEST, null, context);

		if (json_contest != null) {
			return list = get_prevcontest(json_contest);
		}

		return null;
	}

	@Override
	protected void onPostExecute(List<Contest> result) {

		iOpenContestList.OpenContestList(result);
		if (dialog != null) {
			dialog.hide();
		}
		super.onPostExecute(result);
	}

	private List<Contest> get_prevcontest(String JsonString) {

		List<Contest> list = new ArrayList<Contest>();

		if (JsonString != null) {
			try {
				JSONArray jsonArray = new JSONArray(JsonString);
				for (int i = 0; i < jsonArray.length(); i++) {
					Contest contest = Contest.GetContest(jsonArray
							.getJSONObject(i));

					list.add(contest);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return list;
	}

}
