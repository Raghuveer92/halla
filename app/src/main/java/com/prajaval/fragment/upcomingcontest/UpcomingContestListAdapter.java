package com.prajaval.fragment.upcomingcontest;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.core.Contest;
import com.prajaval.utility.SquareImageView;

public class UpcomingContestListAdapter extends BaseAdapter {

	Context context;
	List<Contest> listContest;
	DisplayImageOptions options;

	public UpcomingContestListAdapter(Context context, List<Contest> listcontest) {
		this.context = context;
		this.listContest = listcontest;

		options = new DisplayImageOptions.Builder()

		.resetViewBeforeLoading(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300))
				.displayer(new RoundedBitmapDisplayer(3)).build();

	}

	@Override
	public int getCount() {

		return listContest.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listContest.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolderOpenContest viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.upcomingcontestlist_item,
					null);

			viewHolder = new ViewHolderOpenContest();

			viewHolder.img_upcomingcontest_contestimage = (SquareImageView) convertView
					.findViewById(R.id.img_upcomingcontest_contestimage);
			viewHolder.img_upcomingcontest_contesttype = (ImageView) convertView
					.findViewById(R.id.img_upcomingcontest_contesttype);
			viewHolder.txt_upcomingcontest_contestname = (TextView) convertView
					.findViewById(R.id.txt_upcomingcontest_contestname);
			viewHolder.txt_upcomingcontest_contestprizes = (TextView) convertView
					.findViewById(R.id.txt_upcomingcontest_contestprizes);
			viewHolder.txt_upcomingcontest_contestdesc = (TextView) convertView
					.findViewById(R.id.txt_upcomingcontest_contestdesc);

			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolderOpenContest) convertView.getTag();
		}

		// Binding
		Contest contests = listContest.get(position);

		viewHolder.txt_upcomingcontest_contestname.setText(contests
				.getStr_ContestTitle());
		viewHolder.txt_upcomingcontest_contestprizes.setText(contests
				.getStr_ContestPrize());
		viewHolder.txt_upcomingcontest_contestdesc.setText(contests
				.getStr_ContestDesc());

		if (!contests.getStr_ContestType().equals("image")) {
			viewHolder.img_upcomingcontest_contesttype.setImageDrawable(context
					.getResources().getDrawable(R.drawable.video));
		} else {

			viewHolder.img_upcomingcontest_contesttype.setImageDrawable(context
					.getResources().getDrawable(R.drawable.btn_camera));
		}

		ImageLoader.getInstance().displayImage(contests.getStr_ContestImage(),
				viewHolder.img_upcomingcontest_contestimage, options,
				new SimpleImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {

					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {

					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {

					}
				});

		return convertView;
	}

	private class ViewHolderOpenContest {

		SquareImageView img_upcomingcontest_contestimage;
		ImageView img_upcomingcontest_contesttype;
		TextView txt_upcomingcontest_contestname;
		TextView txt_upcomingcontest_contestprizes;
		TextView txt_upcomingcontest_contestdesc;
	}
}
