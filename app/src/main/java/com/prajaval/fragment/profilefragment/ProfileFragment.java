package com.prajaval.fragment.profilefragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.app_dev.framwordk_classes.CustomListAdapter;
import com.prajaval.app_dev.framwordk_classes.CustomListAdapterInterface;
import com.prajaval.core.Entry;
import com.prajaval.core.User;
import com.prajaval.profile_activity.ProfileActivity;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import common.prajaval.common.Common;
import in.srain.cube.views.GridViewWithHeaderAndFooter;

public class ProfileFragment extends Fragment implements OnItemClickListener, CustomListAdapterInterface {

    ImageView img_user_profile_image;
    TextView txt_user_profile_name, txt_user_points, txt_contest_won,
            txt_contest_number_entered;

    GridViewWithHeaderAndFooter gridView;
    private CustomListAdapter<Entry> adapter;
    private List<Entry> enteries = new ArrayList<>();
    private DisplayImageOptions options;

    private TextView txt_user_profile_add;
    private View headerView;
    private FrameLayout headerLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }


    private void bind_user() {

        if (Common.USER != null) {

            if (!TextUtils.isEmpty(Common.USER.getUserName())) {
                txt_user_profile_name.setText(Common.USER.getUserName());
            }
            if (!TextUtils.isEmpty(Common.USER.getStrUser_location())) {
                txt_user_profile_add.setText(Common.USER.getStrUser_location());
            }
            if (!TextUtils.isEmpty(Common.USER.getStrUser_image())) {
                Picasso.with(getActivity()).load(Common.USER.getStrUser_image()).into(img_user_profile_image);
            }

            String strPoints = Common.USER.getStrPoints();
            if (TextUtils.isEmpty(strPoints)){
                strPoints="0";
            }
            txt_user_points.setText(strPoints + " " + getString(R.string.points));
            txt_contest_won.setText("(" + getWon() + ") " + getString(R.string.won));
            txt_contest_number_entered.setText(getEntered() + " " + getString(R.string.entries));
            if (Common.USER.getEntries() != null && Common.USER.getEntries().size() > 0) {
                gridView.setVisibility(View.VISIBLE);
                gridView.addHeaderView(headerView);
                enteries.clear();
                enteries.addAll(Common.USER.getEntries());
                adapter = new CustomListAdapter<>(getActivity(), R.layout.profile_gridview_item, enteries, this);
                gridView.setAdapter(adapter);
            } else {
//                gridView.setVisibility(View.GONE);
                headerLayout.addView(headerView);
            }
        } else {

            Toast.makeText(
                    getActivity(),
                    "Oops.! Something went wrong. \nCheck your Internet Connection.",
                    Toast.LENGTH_LONG).show();

        }

    }

    private String getWon() {
        int won = 0;
        if (Common.USER.entries != null) {
            for (com.prajaval.core.Entry eleEntry : Common.USER.getEntries()) {
                if (eleEntry.getWinner().equals("yes")) {
                    won++;
                }
            }

        } else {
            won = 0;
        }

        return String.valueOf(won);
    }

    private String getEntered() {
        int entered = 0;
        if (Common.USER.entries != null) {
            entered = Common.USER.entries.size();
        } else {
            entered = 0;
        }

        return String.valueOf(entered);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.profile_fragment, container,
                false);
        headerView = LayoutInflater.from(getActivity()).inflate(R.layout.view_profile_header, null);
        gridView = (GridViewWithHeaderAndFooter) rootView.findViewById(R.id.gv_enteries);
        headerLayout = (FrameLayout) rootView.findViewById(R.id.headerFrame);
        initialise_widget(rootView);

//        gridView.setOnItemClickListener(this);

        bind_user();

        return rootView;

    }

    private void initialise_widget(View rootView) {

        txt_user_profile_name = (TextView) headerView
                .findViewById(R.id.txt_user_profile_name);
        txt_user_profile_add = (TextView) headerView
                .findViewById(R.id.tv_add);
        txt_user_points = (TextView) headerView
                .findViewById(R.id.txt_user_points);
        txt_contest_won = (TextView) headerView
                .findViewById(R.id.txt_contest_won);
        txt_contest_number_entered = (TextView) headerView
                .findViewById(R.id.txt_contest_number_entered);

        img_user_profile_image = (ImageView) headerView
                .findViewById(R.id.img_user_profile_image);
        // lv_previouscontest.setOnItemClickListener(this);
        // lv_previouscontest.setOnScrollListener(onPreviousContestListScroll);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,
                            int position, long arg3) {

        Entry entry = enteries.get(position);

        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.putExtra(Common.ENTRY, entry);
        startActivity(intent);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;

        if (view == null) {
            view = inflater
                    .inflate(resourceID, null);

            holder = new Holder();
            holder.img_profile_grid_item = (ImageView) view
                    .findViewById(R.id.img_profile_grid_item);
            holder.tvTitle = (TextView) view.findViewById(R.id.tv_title);
            holder.tvSubTitle = (TextView) view.findViewById(R.id.tv_sub_title);

            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }

        Entry entry = enteries.get(position);

        ImageLoader.getInstance().displayImage(entry.getThumb_url(),
                holder.img_profile_grid_item, options,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view,
                                                FailReason failReason) {

                        holder.img_profile_grid_item.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.img_not_found_thumb));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view,
                                                  Bitmap loadedImage) {

                    }
                });
        holder.tvTitle.setText(entry.getTitle());
        holder.tvSubTitle.setText(entry.getWinner());
        return view;
    }

    class Holder {
        ImageView img_profile_grid_item;
        TextView tvTitle, tvSubTitle;
    }
}
