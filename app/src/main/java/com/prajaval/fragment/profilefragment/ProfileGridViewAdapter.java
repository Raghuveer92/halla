package com.prajaval.fragment.profilefragment;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.app_dev.ListAsGridBaseAdapter;
import com.prajaval.core.Entry;
import com.prajaval.utility.SquareImageView;

import in.srain.cube.views.GridViewWithHeaderAndFooter;

public class ProfileGridViewAdapter extends ListAsGridBaseAdapter {

	List<Entry> listEntry;

	Context context;
	private DisplayImageOptions options;

	public ProfileGridViewAdapter(Context con, List<Entry> listEntry) {
		super(con);
		context = con;
		this.listEntry = listEntry;

		options = new DisplayImageOptions.Builder().resetViewBeforeLoading(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.ARGB_8888).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300)).build();

	}

	@Override
	public int getItemCount() {
		return listEntry.size();
	}

	@Override
	protected View getItemView(int position, View view, ViewGroup parent) {
		final Holder holder;

		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater
					.inflate(R.layout.profile_gridview_item, null);

			holder = new Holder();
			holder.img_profile_grid_item = (ImageView) view
					.findViewById(R.id.img_profile_grid_item);
			holder.tvTitle= (TextView) view.findViewById(R.id.tv_title);
			holder.tvSubTitle= (TextView) view.findViewById(R.id.tv_sub_title);

			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

		Entry entry = listEntry.get(position);

		ImageLoader.getInstance().displayImage(entry.getThumb_url(),
				holder.img_profile_grid_item, options,
				new SimpleImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {

					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
												FailReason failReason) {
						holder.img_profile_grid_item.setImageDrawable(context
								.getResources().getDrawable(
										R.drawable.img_not_found_thumb));
					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
												  Bitmap loadedImage) {

					}
				});
		holder.tvTitle.setText(entry.getTitle());
		holder.tvSubTitle.setText(entry.getWinner());
		return view;	}

	@Override
	public Object getItem(int position) {

		return listEntry.get(position);
	}

	@Override
	public long getItemId(int position) {

		return 0;
	}

	public class Holder {
		ImageView img_profile_grid_item;
		TextView tvTitle,tvSubTitle;
	}

}
