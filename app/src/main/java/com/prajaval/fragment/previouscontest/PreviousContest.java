package com.prajaval.fragment.previouscontest;

import java.util.List;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.prajaval.core.Contest;
import com.prajaval.core.User;
import com.prajaval.previouscontest.gridviews_entries.PreviousContestEntriesActivity;

import common.prajaval.common.Common;

public class PreviousContest extends Fragment implements IPreviousContestList,
		OnItemClickListener {

	private final int PREV_CONTEST_FIRST_COUNT = 0;

	ListView lv_previouscontest;

	List<Contest> listPrevContest;
	PreviousContestListAdapter previousContestListAdapter;

	public PreviousContest() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		call_previouscontest(PREV_CONTEST_FIRST_COUNT, true);

		try {
			if (savedInstanceState != null) {
				if (savedInstanceState.getSerializable(Common.USER_VALUE) != null)
					Common.USER = (User) savedInstanceState
							.getSerializable(Common.USER_VALUE);
			}
		} catch (Exception e) {
		}

	}

	private void call_previouscontest(int count, boolean isfirst) {

		PreviousContestController previousContestController = new PreviousContestController(
				getActivity(), count, this, isfirst);
		previousContestController.execute();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.previouscontest_fragment,
				container, false);
		lv_previouscontest = (ListView) rootView
				.findViewById(R.id.lv_previouscontest);

		lv_previouscontest.setOnItemClickListener(this);
		lv_previouscontest.setOnScrollListener(onPreviousContestListScroll);
		return rootView;
	}

	@Override
	public void PrevContestTaskCompleted(List<Contest> _listPrevContest) {

		try {
			if (_listPrevContest != null) {
				if (_listPrevContest.size() > 0) {
					if (listPrevContest == null) {

						listPrevContest = _listPrevContest;

						bind_listview();

					} else {

						for (int i = 0; i < _listPrevContest.size(); i++) {
							listPrevContest.add(_listPrevContest.get(i));
						}

						// run on UI thread.
						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								previousContestListAdapter
										.notifyDataSetChanged();
							}
						});

					}
				} else {
					Toast.makeText(
							getActivity(),
							"Oops.! Something went wrong. \nCheck your Internet Connection.",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(
						getActivity(),
						"Oops.! Something went wrong. \nCheck your Internet Connection.",
						Toast.LENGTH_LONG).show();
			}

		} catch (Exception e) {

		}

	}

	private void bind_listview() {
		previousContestListAdapter = new PreviousContestListAdapter(
				getActivity(), listPrevContest);
		lv_previouscontest.setAdapter(previousContestListAdapter);

	}

	@Override
	public void PrevContestTaskCompleted(Contest PrevContest) {

	}

	OnScrollListener onPreviousContestListScroll = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {

			if (scrollState == SCROLL_STATE_IDLE) {
				if (view.getLastVisiblePosition() >= view.getCount() - 1
						- lv_previouscontest.getCount()) {

					int totalItemCount = lv_previouscontest.getCount();

					if (totalItemCount % 10 == 0) {
						call_previouscontest(totalItemCount + 10, false);
					}
				}

			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {

		}
	};

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long arg3) {

		try {
			Contest contest = previousContestListAdapter.getItem(position);

			Intent intent = new Intent(getActivity(),
					PreviousContestEntriesActivity.class);
			intent.putExtra(Common.SELECTED_CONTEST, contest);
			startActivity(intent);

		} catch (Exception e) {

		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable(Common.USER_VALUE, Common.USER);
		super.onSaveInstanceState(outState);

		// save data.

	}
}
