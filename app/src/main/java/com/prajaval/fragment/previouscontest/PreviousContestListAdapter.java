package com.prajaval.fragment.previouscontest;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.core.Contest;
import com.prajaval.utility.SquareImageView;
import com.squareup.picasso.Picasso;

public class PreviousContestListAdapter extends BaseAdapter {

	Context context;
	List<Contest> listContest;
	DisplayImageOptions options;

	public PreviousContestListAdapter(Context context, List<Contest> listcontest) {

		this.context = context;
		this.listContest = listcontest;

		options = new DisplayImageOptions.Builder()

		.resetViewBeforeLoading(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300))
				.displayer(new RoundedBitmapDisplayer(3)).build();

	}

	@Override
	public int getCount() {

		return listContest.size();
	}

	@Override
	public Contest getItem(int position) {

		return listContest.get(position);
	}

	@Override
	public long getItemId(int arg0) {

		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolderOpenContest viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.previouscontestlist_item,
					null);

			viewHolder = new ViewHolderOpenContest();

			viewHolder.img_prevcontest_contestimage = (ImageView) convertView
					.findViewById(R.id.img_prevcontest_contestimage);
			viewHolder.img_precontest_contesttype = (ImageView) convertView
					.findViewById(R.id.img_precontest_contesttype);
			viewHolder.txt_prevcontest_contestname = (TextView) convertView
					.findViewById(R.id.txt_prevcontest_contestname);
			viewHolder.txt_prevcontest_contestprizes = (TextView) convertView
					.findViewById(R.id.txt_prevcontest_contestprizes);
			viewHolder.txt_prevcontest_contestdesc = (TextView) convertView
					.findViewById(R.id.txt_prevcontest_contestdesc);

			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolderOpenContest) convertView.getTag();
		}

		// Binding
		Contest contests = listContest.get(position);

		viewHolder.txt_prevcontest_contestname.setText(contests
				.getStr_ContestTitle());
		viewHolder.txt_prevcontest_contestprizes.setText(contests
				.getStr_ContestPrize());
		viewHolder.txt_prevcontest_contestdesc.setText(contests
				.getStr_ContestDesc());

		if (!contests.getStr_ContestType().equals("image")) {
			viewHolder.img_precontest_contesttype.setImageDrawable(context
					.getResources().getDrawable(R.drawable.video));
		} else {

			viewHolder.img_precontest_contesttype.setImageDrawable(context
					.getResources().getDrawable(R.drawable.btn_camera));
		}
		Picasso.with(context).load(contests.getStr_ContestImage()).into(viewHolder.img_prevcontest_contestimage);
//		ImageLoader.getInstance().displayImage(contests.getStr_ContestImage(),
//				viewHolder.img_prevcontest_contestimage, options,
//				new SimpleImageLoadingListener() {
//					@Override
//					public void onLoadingStarted(String imageUri, View view) {
//
//					}
//
//					@Override
//					public void onLoadingFailed(String imageUri, View view,
//							FailReason failReason) {
//						viewHolder.img_prevcontest_contestimage
//								.setImageDrawable(context.getResources()
//										.getDrawable(
//												R.drawable.img_not_found_thumb));
//					}
//
//					@Override
//					public void onLoadingComplete(String imageUri, View view,
//							Bitmap loadedImage) {
//
//					}
//				});
//
		return convertView;
	}

	private class ViewHolderOpenContest {

		ImageView img_prevcontest_contestimage;
		ImageView img_precontest_contesttype;
		TextView txt_prevcontest_contestname;
		TextView txt_prevcontest_contestprizes;
		TextView txt_prevcontest_contestdesc;
	}
}
