package com.prajaval.fragment.previouscontest;

import java.util.List;

import com.prajaval.core.Contest;

public interface IPreviousContestList {

	void PrevContestTaskCompleted(List<Contest> listPrevContest);
	void PrevContestTaskCompleted(Contest PrevContest);
}
