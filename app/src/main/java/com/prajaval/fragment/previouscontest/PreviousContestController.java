package com.prajaval.fragment.previouscontest;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.json.classes.JSONArray;
import com.prajaval.core.Contest;
import com.prajaval.database.HallaDataSource;
import com.prajaval.utility.CustomProgressDialog;

import common.prajaval.common.CommonURL;

public class PreviousContestController extends
		AsyncTask<Void, Void, List<Contest>> {

	public String PREV_CONTESTLIST = "http://halla.in/api/prev_contest/";

	Context context;
	int count;
	IPreviousContestList iPreviousContestList;

	HallaDataSource dataSource;

	ProgressDialog dialog;
	boolean isFirst;

	public PreviousContestController(Context _context, int count,
			IPreviousContestList iPreviousContestList, boolean isFirsttime) {
		this.context = _context;
		this.count = count;
		this.iPreviousContestList = iPreviousContestList;
		this.isFirst = isFirsttime;

	}

	@Override
	protected void onPreExecute() {

		if (isFirst) {
			dialog = CustomProgressDialog.ctor(this.context);
			dialog.show();
		}

	}

	@Override
	protected List<Contest> doInBackground(Void... params) {

		List<Contest> list = new ArrayList<Contest>();

		PREV_CONTESTLIST = PREV_CONTESTLIST + count;

		String json_prev_contest = CommonURL.Execute(PREV_CONTESTLIST, null,
				context);

		if (json_prev_contest != null) {

			list = get_prevcontest(json_prev_contest);

			return list;
		}
		return list;
	}

	@Override
	protected void onPostExecute(List<Contest> result) {

		iPreviousContestList.PrevContestTaskCompleted(result);
		if (isFirst) {
			if (dialog != null) {
				dialog.hide();
			}
		}
		super.onPostExecute(result);

	}

	private List<Contest> get_prevcontest(String JsonString) {

		List<Contest> list = new ArrayList<Contest>();

		if (JsonString != null) {
			try {
				JSONArray jsonArray = new JSONArray(JsonString);
				for (int i = 0; i < jsonArray.length(); i++) {
					Contest contest = Contest.GetContest(jsonArray
							.getJSONObject(i));

					list.add(contest);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return list;
	}
}
