package com.prajaval.youtubevideo;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import com.google.android.youtube.player.YouTubePlayer.PlaybackEventListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;
import common.prajaval.common.Common;

public class YouTubeVideo extends YouTubeBaseActivity implements
		YouTubePlayer.OnInitializedListener {

	final String YOUTUBE_KEY = "AIzaSyC3yl35T_nyFBLn0NaSUNxHdmzfysFkYrg";
	private String VIDEO_ID = "";

	YouTubePlayer youTubePlayer;
	YouTubePlayerView youTubePlayerView;

	@Override
	protected void onCreate(Bundle arg0) {

		super.onCreate(arg0);
		setContentView(R.layout.youtubevideo_activty);
		VIDEO_ID = getIntent().getStringExtra(Common.VIDEO_ID);
		youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player);
		try {
			youTubePlayerView.initialize(YOUTUBE_KEY, this);
		} catch (Exception e) {
			e.toString();
		}

		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	protected void onDestroy() {

		super.onDestroy();
		//YouTubeVideo.this.finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);

	}

	@Override
	public void onInitializationFailure(Provider arg0,
			YouTubeInitializationResult arg1) {
		Toast.makeText(this, "Failured to Initialize!", Toast.LENGTH_LONG)
				.show();

	}

	@Override
	public void onInitializationSuccess(Provider arg0, YouTubePlayer player,
			boolean wasRestored) {
		youTubePlayer = player;
		youTubePlayer.setFullscreen(true);
		youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
		youTubePlayer.setPlaybackEventListener(playbackEventListener);

		/** Start buffering **/
		if (!wasRestored) {
			youTubePlayer.cueVideo(VIDEO_ID);
		}

	}

	private PlaybackEventListener playbackEventListener = new PlaybackEventListener() {

		@Override
		public void onBuffering(boolean arg0) {

		}

		@Override
		public void onPaused() {

		}

		@Override
		public void onPlaying() {

		}

		@Override
		public void onSeekTo(int arg0) {

		}

		@Override
		public void onStopped() {

		}

	};

	private PlayerStateChangeListener playerStateChangeListener = new PlayerStateChangeListener() {

		@Override
		public void onAdStarted() {

		}

		@Override
		public void onError(ErrorReason arg0) {

		}

		@Override
		public void onLoaded(String arg0) {
			youTubePlayer.play();
		}

		@Override
		public void onLoading() {

		}

		@Override
		public void onVideoEnded() {

		}

		@Override
		public void onVideoStarted() {

		}
	};

}
