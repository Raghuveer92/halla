package com.prajaval;

import java.util.regex.Pattern;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.prajaval.facebooklogin.FaceBookLogin;

public class Sociallogin extends FragmentActivity implements OnClickListener {

	EditText etEmail, etPassword;
	Button btnSingIn;
	Button btnfacebook, btnMail;
	// Button btnGoogleplus, btnTwitter;
	TextView txtforgotpassword;

	@Override
	protected void onCreate(Bundle bundle) {

		super.onCreate(bundle);
		setContentView(R.layout.sociallogin_activity);

		Initialise_widgets();
	}

	private void Initialise_widgets() {

		etEmail = (EditText) findViewById(R.id.etEmail);
		etPassword = (EditText) findViewById(R.id.etPassword);
		btnSingIn = (Button) findViewById(R.id.btnSingIn);
		btnfacebook = (Button) findViewById(R.id.btnfacebook);
		// btnGoogleplus = (Button) findViewById(R.id.btnGoogleplus);
		// btnTwitter = (Button) findViewById(R.id.btnTwitter);
		btnMail = (Button) findViewById(R.id.btnMail);

		txtforgotpassword = (TextView) findViewById(R.id.txtforgotpassword);

		btnfacebook.setOnClickListener(this);
		// btnGoogleplus.setOnClickListener(this);
		// btnTwitter.setOnClickListener(this);
		btnMail.setOnClickListener(this);
		btnSingIn.setOnClickListener(this);
		txtforgotpassword.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnfacebook:
			Intent intentFaceBookLogin = new Intent(Sociallogin.this,
					FaceBookLogin.class);
			Sociallogin.this.finish();
			startActivity(intentFaceBookLogin);
			break;
		// case R.id.btnGoogleplus:
		// Intent intentGooglePlusLogin = new Intent(Sociallogin.this,
		// GooglePlusLogin.class);
		// startActivity(intentGooglePlusLogin);
		// break;
		// case R.id.btnTwitter:
		// break;
		case R.id.btnMail:
			break;
		case R.id.btnSingIn:
			break;
		case R.id.txtforgotpassword:
			break;

		default:
			break;
		}
	}

	void signin() {
		try {
			
		} catch (Exception e) {

		}
	}

	private String PrimaryEmailID() {
		String possibleEmail = "";
		Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
		Account[] accounts = AccountManager.get(Sociallogin.this).getAccounts();
		for (Account account : accounts) {
			if (emailPattern.matcher(account.name).matches()) {
				possibleEmail = account.name;

			}
		}
		return possibleEmail;
	}
}
