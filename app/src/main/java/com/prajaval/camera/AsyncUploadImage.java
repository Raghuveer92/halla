package com.prajaval.camera;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.Toast;

public class AsyncUploadImage extends AsyncTask<Void, Void, String> {

	Context context;
	int icontestID, iUserID;
	String title;
	String desc;
	Bitmap bitmap;
	public final String UPLOAD = "http://halla.in/api/upload";
	IUploadSuccess iUploadSuccess;
	ProgressDialog dialog;

	public AsyncUploadImage(Context context, int iUserID, int contestid,
			String title, String desc, Bitmap bitmap,
			IUploadSuccess iUploadSuccess) {
		this.context = context;
		this.icontestID = contestid;
		this.title = title;
		this.desc = desc;
		this.bitmap = bitmap;
		this.iUploadSuccess = iUploadSuccess;
		this.iUserID = iUserID;
	}

	@Override
	protected void onPreExecute() {
		//dialog = ProgressDialog.show(context, "", "Uploading...", true);
	}

	@Override
	protected String doInBackground(Void... params) {

		try {

			HttpPost httppost = new HttpPost(UPLOAD);
			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			HttpClient httpClient = new DefaultHttpClient();
			String sname = "TestImage";

			if (bitmap != null) {
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.JPEG, 75, bos);
				byte[] data = bos.toByteArray();
				ByteArrayBody bab = new ByteArrayBody(data, sname + ".jpg");
				entity.addPart("HALLA_FILE", bab);
			}

			try {
				StringBody name = new StringBody(sname);
				StringBody sTitle = new StringBody(title);
				StringBody sDesc = new StringBody(desc);
				StringBody sUserID = new StringBody(String.valueOf(iUserID));
				StringBody sContestID = new StringBody(
						String.valueOf(icontestID));
				entity.addPart("name", name);
				entity.addPart("title", sTitle);
				entity.addPart("desc", sDesc);
				entity.addPart("userid", sUserID);
				entity.addPart("contestid", sContestID);

			} catch (UnsupportedEncodingException e1) {

				e1.printStackTrace();
			}

			httppost.setEntity(entity);

			try {
				HttpResponse response = httpClient.execute(httppost);
				BufferedReader in = null;
				try {
					in = new BufferedReader(new InputStreamReader(response
							.getEntity().getContent(), "UTF-8"));
					StringBuffer sb = new StringBuffer("");
					String line = "";
					String NL = System.getProperty("line.separator");
					while ((line = in.readLine()) != null) {
						sb.append(line + NL);
					}
					in.close();
					return sb.toString();
				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}

		} catch (Exception e) {

		}
		return "Oops..! Something went wrong.";
	}

	@Override
	protected void onPostExecute(String result) {

		//if (dialog.isShowing()) {
		//	dialog.dismiss();
		//}
		iUploadSuccess.UploadSuccessful(result);
	}

}
