package com.prajaval.camera;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.adapters.MediaFragment;
import com.prajaval.BaseActivity;
import com.prajaval.core.Contest;

import common.prajaval.common.Common;

public class CameraActivity extends BaseActivity implements OnClickListener {

    public static final int MEDIA_TYPE_IMAGE = 1;
    private static int RESULT_LOAD_IMAGE = 1;
    private static int CAMERA_PIC_REQUEST = 2;
    private Uri fileUri;
    private Bitmap bitmap;
    public final int IMAGE_UPLOAD_SIZE = 1024;// 1 byte
    public final int IMAGE_DOWNLOAD_SIZE = 1024;// 1 bytes
    Contest contest;
    String contest_type;
    Point size;
    Button btnUpload;
    ImageView img_camera;

    EditText et_title, et_description;
    final String FILE_URI = "fileuri";
    Display display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_activity);
        initialise_widgets();

        try {
            Intent intent = getIntent();

            display = getWindowManager().getDefaultDisplay();
            size = new Point();
            display.getSize(size);

            contest = (Contest) intent
                    .getSerializableExtra(Common.SELECTED_CONTEST);

            contest_type = (String) intent.getStringExtra(Common.MESSAGE);

            getActionBar().setDisplayHomeAsUpEnabled(true);
            btnUpload.setOnClickListener(this);

            if (contest_type.equals("camera")) {
                Intent intentcamera = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                intentcamera.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                // start the image capture Intent
                startActivityForResult(intentcamera, CAMERA_PIC_REQUEST);

            } else {
//                Intent intentgallery = new Intent(Intent.ACTION_PICK);
//                intent.setType("image/*");
                Intent intentgallery = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(intentgallery, RESULT_LOAD_IMAGE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        try {
            outState.putSerializable(Common.SELECTED_CONTEST, contest);
            outState.putString(FILE_URI, fileUri.toString());
        } catch (Exception e) {

        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        try {
            contest = (Contest) savedInstanceState
                    .getSerializable(Common.SELECTED_CONTEST);
            fileUri = Uri.parse(savedInstanceState.getString(FILE_URI));

        } catch (Exception e) {

        }

        super.onRestoreInstanceState(savedInstanceState);
    }

    private void initialise_widgets() {
        img_camera = (ImageView) findViewById(R.id.img_camera);
        btnUpload = (Button) findViewById(R.id.btnUpload);

        et_title = (EditText) findViewById(R.id.et_title);
        et_description = (EditText) findViewById(R.id.et_description);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Bitmap unsclaledbitmap;
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                && null != data) {

            Uri selectedImage = data.getData();

            bitmap = MediaFragment.getBitmapFromUri(this, selectedImage);

//            String filePath = getPath(selectedImage);
//
//            if (filePath != null) {
//
//                unsclaledbitmap = decodeFile(filePath, 800);
//
//                bitmap = Bitmap.createScaledBitmap(unsclaledbitmap,
//                        unsclaledbitmap.getWidth(),
//                        unsclaledbitmap.getHeight(), true);

//            } else {
//                Toast.makeText(getApplicationContext(), "Unknown Path",
//                        Toast.LENGTH_LONG).show();
//            }
        } else if (requestCode == CAMERA_PIC_REQUEST) {

            if (resultCode == RESULT_OK) {

                if (fileUri != null) {

                    try {

                        unsclaledbitmap = decodeFile(fileUri.getPath(),
                                display.getWidth());
                        bitmap = Bitmap.createScaledBitmap(unsclaledbitmap,
                                unsclaledbitmap.getWidth(),
                                unsclaledbitmap.getHeight(), true);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                CameraActivity.this.finish();
            }
        } else {
            CameraActivity.this.finish();

        }

        if (bitmap != null) {

            // redirect to next page. with image and contest id.
            img_camera.setImageBitmap(bitmap);

        }

    }

    public Bitmap decodeFile(String filePath, int desiredwidth) {
        // Decode image size
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        BitmapFactory.decodeFile(filePath, options);

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = options.outWidth, height_tmp = options.outHeight;

        int inSampleSize = 1;
        while (width_tmp / 2 > desiredwidth) {
            width_tmp /= 2;
            height_tmp /= 2;
            inSampleSize *= 2;
        }

        float desiredScale = (float) desiredwidth / width_tmp;

        // Decode with inSampleSize
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inSampleSize = inSampleSize;
        options.inScaled = false;

        Bitmap sampledSrcBitmap = BitmapFactory.decodeFile(filePath, options);
        if (sampledSrcBitmap != null) {
            // // Resize
            Matrix matrix = new Matrix();
            matrix.postScale(desiredScale, desiredScale);
            Bitmap scaledBitmap = Bitmap.createBitmap(sampledSrcBitmap, 0, 0,
                    sampledSrcBitmap.getWidth(), sampledSrcBitmap.getHeight(),
                    matrix, true);
            sampledSrcBitmap = null;
            return scaledBitmap;
        }
        return null;
    }

    public String getPath(Uri uri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePathColumn, null,
                null, null);

        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "HALLA");
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Halla", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "Halla_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnUpload) {
            upload_photo();
        }
    }

    private void upload_photo() {

        try {

            if (et_title.getText().length() != 0) {

                if (bitmap != null && Common.USER.getiUserID() > 0) {

                    new AsyncUploadImage(CameraActivity.this,
                            Common.USER.getiUserID(), contest.getiContestId(),
                            et_title.getText().toString(), et_description
                            .getText().toString(), bitmap,
                            new IUploadSuccess() {

                                @Override
                                public void UploadSuccessful(final String result) {
                                    // runOnUiThread(new Runnable() {
                                    // public void run() {
                                    // Toast.makeText(CameraActivity.this,
                                    // result, 1200).show();
                                    // }
                                    // });

                                }
                            }).execute();

                    Toast.makeText(
                            CameraActivity.this,
                            "Photo under moderation.., \nWill Approve it soon.",
                            Toast.LENGTH_LONG).show();

                    CameraActivity.this.finish();
                } else {
                    Toast.makeText(
                            CameraActivity.this,
                            "Oops.! Something went wrong \nCheck your Internet Connection.",
                            Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(CameraActivity.this, "Enter title.",
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
