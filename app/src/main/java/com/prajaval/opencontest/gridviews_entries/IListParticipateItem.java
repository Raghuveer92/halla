package com.prajaval.opencontest.gridviews_entries;

import java.util.List;

import com.prajaval.core.ParticipateItem;

public interface IListParticipateItem {

	void TaskCompleted(List<ParticipateItem> listParticipateItem);
}
