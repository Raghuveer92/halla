package com.prajaval.opencontest.gridviews_entries;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.core.ParticipateItem;
import com.prajaval.utility.SquareImageView;

@SuppressLint("InflateParams")
public class ParticipateItemAdapter extends BaseAdapter {

    List<ParticipateItem> listItems;
    Context context;
    DisplayImageOptions options;
    ViewHolder viewHolder = null;

    public ParticipateItemAdapter(Context context, List<ParticipateItem> list) {

        this.context = context;
        listItems = list;

//        options = new DisplayImageOptions.Builder()
//
//                .resetViewBeforeLoading(true).cacheOnDisk(true)
//                .imageScaleType(ImageScaleType.EXACTLY)
//                .bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
//                .displayer(new FadeInBitmapDisplayer(300, true, true, true))
//                .build();

        options = new DisplayImageOptions.Builder()

                .resetViewBeforeLoading(true).cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.ARGB_8888).considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        // .displayer(new FadeInBitmapDisplayer(300))
    }

    @Override
    public int getCount() {

        return listItems.size();
    }

    @Override
    public ParticipateItem getItem(int position) {

        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.participate_entry_item,
                    null);
            viewHolder = new ViewHolder();

            viewHolder.imgParticaipate = (SquareImageView) convertView
                    .findViewById(R.id.img_gv_entry);
            viewHolder.txt_gv_contestname = (TextView) convertView
                    .findViewById(R.id.text_gv_contesttitle);

            viewHolder.txt_gv_username = (TextView) convertView
                    .findViewById(R.id.txt_gv_username);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ParticipateItem item = listItems.get(position);
        viewHolder.txt_gv_contestname.setText(item.getStrTitle());
        viewHolder.txt_gv_username.setText(item.getStrUsername());

        ImageLoader.getInstance().displayImage(item.getStrThumbURL(),
                viewHolder.imgParticaipate, options,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        viewHolder.imgParticaipate.setBackgroundColor(context
                                .getResources().getColor(R.color.img_bg));
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view,
                                                FailReason failReason) {

                        viewHolder.imgParticaipate.setImageDrawable(context
                                .getResources().getDrawable(
                                        R.drawable.img_not_found_thumb));

                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view,
                                                  Bitmap loadedImage) {

                        viewHolder.imgParticaipate.setBackgroundColor(context
                                .getResources().getColor(R.color.img_bg));

                    }
                });
        return convertView;
    }

    private class ViewHolder {
        SquareImageView imgParticaipate;
        TextView txt_gv_contestname;
        TextView txt_gv_username;

    }

}
