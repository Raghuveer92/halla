package com.prajaval.opencontest.gridviews_entries;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.prajaval.app_dev.framwordk_classes.BaseActivity;
import com.prajaval.camera.CameraActivity;
import com.prajaval.core.Contest;
import com.prajaval.core.ParticipateItem;
import com.prajaval.core.User;
import com.prajaval.participateimagedetail.ParticipateImageDetailActivity;
import com.prajaval.video.VideoActivity;

import common.prajaval.common.Common;
import de.hdodenhof.circleimageview.CircleImageView;

public class ParticipateEntryListActivity extends com.prajaval.BaseActivity implements
        OnItemClickListener {

    Contest selectedContest;
    CircleImageView btn_EntryUpload;
    GridView gv_ParticipateEntries;
    ParticipateItemAdapter participateItemAdapter;
    List<ParticipateItem> listParticipateItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.participateentrylist_activity);

        Intent intent = getIntent();
        selectedContest = (Contest) intent
                .getSerializableExtra(Common.SELECTED_CONTEST);

        initialise_widgets();
        btn_EntryUpload.setOnClickListener(btnupload);
        callservice();
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("Entries");

    }

    OnClickListener btnupload = new OnClickListener() {

        @Override
        public void onClick(View v) {
            SelectImage();
        }
    };

    @Override
    protected void onResume() {
        updatelist();
        super.onResume();
    }

    @Override
    protected void onRestart() {

        super.onRestart();
    }

    private void updatelist() {

        try {
            if (listParticipateItem != null) {
                if (listParticipateItem.size() != 0) {

                    for (int i = 0; i < listParticipateItem.size(); i++) {
                        ParticipateItem item = listParticipateItem.get(i);

                        if (Common.PARTICIPATE.getiparticiapteID() == item
                                .getiparticiapteID()) {
                            item.setiVote(Common.PARTICIPATE.getiVote());
                            break;
                        }
                    }

                    participateItemAdapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void SelectImage() {
        final CharSequence[] items = {"camera", "gallery"};

        AlertDialog.Builder builder = new Builder(
                ParticipateEntryListActivity.this);
        builder.setCancelable(true);
        builder.setTitle("Upload your entry");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                try {

                    if (items[item].equals("camera")) {

                        if (selectedContest.getStr_ContestType()
                                .equals("image")) {
                            Intent intent = new Intent(
                                    ParticipateEntryListActivity.this,
                                    CameraActivity.class);
                            intent.putExtra(Common.SELECTED_CONTEST,
                                    selectedContest);
                            intent.putExtra(Common.MESSAGE, "camera");
                            startActivity(intent);
                        } else {
                            // video
                            Intent intent = new Intent(
                                    ParticipateEntryListActivity.this,
                                    VideoActivity.class);
                            intent.putExtra(Common.SELECTED_CONTEST,
                                    selectedContest);
                            intent.putExtra(Common.MESSAGE, "camera");
                            startActivity(intent);
                        }

                        dialog.dismiss();

                    } else if (items[item].equals("gallery")) {

                        if (selectedContest.getStr_ContestType()
                                .equals("image")) {
                            Intent intent = new Intent(
                                    ParticipateEntryListActivity.this,
                                    CameraActivity.class);
                            intent.putExtra(Common.SELECTED_CONTEST,
                                    selectedContest);
                            intent.putExtra(Common.MESSAGE, "gallery");
                            startActivity(intent);
                        } else {
                            // video
                        }
                        dialog.dismiss();

                    }
                } catch (Exception e) {
                    e.toString();
                }

            }
        });
        builder.show();
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(Common.SELECTED_CONTEST, selectedContest);
        outState.putSerializable(Common.USER_VALUE, Common.USER);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        try {
            selectedContest = (Contest) savedInstanceState
                    .getSerializable(Common.SELECTED_CONTEST);

            Common.USER = (User) savedInstanceState
                    .getSerializable(Common.USER_VALUE);
            callservice();
        } catch (Exception e) {

        }

    }

    private void callservice() {

        ParticipateEntryController controller = new ParticipateEntryController(
                ParticipateEntryListActivity.this,
                selectedContest.getiContestId(), Common.USER.getiUserID(),
                ilistParticipateItem);
        controller.execute();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void initialise_widgets() {
        gv_ParticipateEntries = (GridView) findViewById(R.id.gv_ParticipateEntries);
        gv_ParticipateEntries.setOnItemClickListener(this);
        btn_EntryUpload = (CircleImageView) findViewById(R.id.btn_EntryUpload);
    }

    IListParticipateItem ilistParticipateItem = new IListParticipateItem() {

        @Override
        public void TaskCompleted(List<ParticipateItem> _listParticipateItem) {

            if (_listParticipateItem != null) {

                listParticipateItem = _listParticipateItem;

                participateItemAdapter = new ParticipateItemAdapter(
                        ParticipateEntryListActivity.this, listParticipateItem);
                gv_ParticipateEntries.setAdapter(participateItemAdapter);

            } else {

                Toast.makeText(ParticipateEntryListActivity.this,
                        "No entries in contest", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,
                            int position, long lng) {
        ParticipateItem participateItem = participateItemAdapter
                .getItem(position);

        Intent intent = new Intent(ParticipateEntryListActivity.this,
                ParticipateImageDetailActivity.class);
        intent.putExtra(Common.PARTICIPATE_ITEM, participateItem);
        startActivity(intent);
    }
}
