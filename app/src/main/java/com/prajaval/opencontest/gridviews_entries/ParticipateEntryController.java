package com.prajaval.opencontest.gridviews_entries;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.json.classes.JSONArray;
import com.prajaval.core.ParticipateItem;
import com.prajaval.utility.CustomProgressDialog;

import common.prajaval.common.Common;

public class ParticipateEntryController extends
		AsyncTask<Void, Void, List<ParticipateItem>> {

	int ContestID;
	private final String PARTICIPATE_ENTRIES = "http://halla.in/api/v1/entries";
	IListParticipateItem iListParticipateItem;
	Context context;
	ProgressDialog dialog;
	int UserID;

	public ParticipateEntryController(Context context, int iContestID,
			int iUserID, IListParticipateItem iListparticipateItem) {

		this.context = context;
		ContestID = iContestID;
		this.iListParticipateItem = iListparticipateItem;
		UserID = iUserID;

	}

	@Override
	protected void onPreExecute() {
		dialog = CustomProgressDialog.ctor(this.context);
		dialog.show();
	}

	@Override
	protected List<ParticipateItem> doInBackground(Void... params) {

		String entry_list = GetVote(String.valueOf(ContestID));
		// String entry_list = CommonURL.Execute(participate_entry, null,
		// context);
		return getEntry_list(entry_list);

	}

	@Override
	protected void onPostExecute(List<ParticipateItem> result) {
		iListParticipateItem.TaskCompleted(result);
		if (dialog != null) {
			dialog.hide();
		}
	}

	public String GetVote(String ContestID) {

		// comment to server
		HttpPost httppost = new HttpPost(PARTICIPATE_ENTRIES);

		MultipartEntity entity = new MultipartEntity(
				HttpMultipartMode.BROWSER_COMPATIBLE);
		HttpClient httpClient = new DefaultHttpClient();

		try {

			StringBody userID = new StringBody(String.valueOf(UserID));
			StringBody photoID = new StringBody(String.valueOf(ContestID));

			entity.addPart("user_id", userID);
			entity.addPart("contest_id", photoID);

		} catch (UnsupportedEncodingException e1) {

			e1.printStackTrace();
		}
		httppost.setEntity(entity);
		String sResponce = "";
		try {
			HttpResponse sResponse = httpClient.execute(httppost);
			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(sResponse
						.getEntity().getContent(), "UTF-8"));
				StringBuffer sb = new StringBuffer("");
				String line = "";
				String NL = System.getProperty("line.separator");
				while ((line = in.readLine()) != null) {
					sb.append(line + NL);
				}
				in.close();
				sResponce = sb.toString();
				return sResponce;
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return sResponce;

	}

	private List<ParticipateItem> getEntry_list(String entrylist) {

		List<ParticipateItem> participateentry = null;
		try {
			JSONArray array = new JSONArray(entrylist);
			for (int i = 0; i < array.length(); i++) {
				ParticipateItem item = ParticipateItem.getParticipateItem(array
						.getJSONObject(i));
				if (participateentry == null) {
					participateentry = new ArrayList<ParticipateItem>();
				}
				participateentry.add(item);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return participateentry;

	}
}
