package com.prajaval.participateimagedetail;

import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.core.Comment;
import com.prajaval.utility.SquareImageView;


public class CommentListAdapter extends BaseAdapter {

	List<Comment> listComment;
	Context context;
	DisplayImageOptions optionuser;

	@SuppressLint("NewApi")
	public CommentListAdapter(List<Comment> list, Context context) {
		listComment = list;
		this.context = context;

		optionuser = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new RoundedBitmapDisplayer(50)).build();

	}

	@Override
	public int getCount() {

		if (listComment == null) {
			return 0;
		}
		if (listComment.size() > 0) {
			return listComment.size();
		} else {
			return 0;
		}

	}

	@Override
	public Comment getItem(int position) {

		return listComment.get(position);
	}

	@Override
	public long getItemId(int position) {

		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final CommentHolder commentHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.comment_list_item, null);

			commentHolder = new CommentHolder();
			commentHolder.txt_comment = (TextView) convertView
					.findViewById(R.id.txt_comment);
			commentHolder.txt_comment_username = (TextView) convertView
					.findViewById(R.id.txt_comment_username);
			commentHolder.img_comment_user = (SquareImageView) convertView
					.findViewById(R.id.img_comment_user);

			convertView.setTag(commentHolder);
		} else {
			commentHolder = (CommentHolder) convertView.getTag();
		}

		if (listComment != null) {
			Comment comment = listComment.get(position);
			commentHolder.txt_comment.setText(comment.getStrComment());
			commentHolder.txt_comment_username
					.setText(comment.getStrUserName());

			ImageLoader.getInstance().displayImage(comment.getStrUserimage(),
					commentHolder.img_comment_user, optionuser,
					new SimpleImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {
							commentHolder.img_comment_user
									.setImageDrawable(context.getResources()
											.getDrawable(R.drawable.user));
						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {

							commentHolder.img_comment_user
									.setImageDrawable(context.getResources()
											.getDrawable(R.drawable.user));
						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {
							commentHolder.img_comment_user
									.setImageBitmap(loadedImage);
						}
					});
		}

		return convertView;
	}

	private class CommentHolder {
		TextView txt_comment;
		TextView txt_comment_username;
		SquareImageView img_comment_user;
	}

}
