package com.prajaval.participateimagedetail;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.core.Comment;
import com.prajaval.core.ParticipateItem;
import com.prajaval.utility.SquareImageView;
import common.prajaval.common.Common;

public class CommentActivity extends Activity implements OnClickListener {

	EditText et_comment;
	Button btnComment;
	ListView lv_CommentList;
	int photoid;
	CommentListAdapter commListAdapter;
	private List<Comment> listComment;
	ParticipateItem participateItem;
	SquareImageView img_comment_user;
	TextView txt_comment_username, txt_comment_contest_title, txt_comment_desc;
	DisplayImageOptions optionuser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.comment_activity);

		inilialisewidgets();

		optionuser = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.showImageForEmptyUri(
						getResources().getDrawable(R.drawable.user))
				.showImageOnFail(getResources().getDrawable(R.drawable.user))
				.cacheOnDisc(true).imageScaleType(ImageScaleType.NONE)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new RoundedBitmapDisplayer(50)).build();

		photoid = getIntent().getIntExtra(Common.PHOTO_ID, 0);

		participateItem = (ParticipateItem) getIntent().getSerializableExtra(
				Common.PARTICIPATE_ITEM);

		new AsyncGetComment(photoid, iListComment).execute();

		btnComment.setOnClickListener(this);

		bind_data();

		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	private void bind_data() {

		txt_comment_username.setText(participateItem.getStrUsername());
		txt_comment_contest_title.setText(participateItem.getStrTitle());
		txt_comment_desc.setText(participateItem.getStrDescription());

		ImageLoader.getInstance().displayImage(
				participateItem.getStrUserImage(), img_comment_user,
				optionuser, new SimpleImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {
						img_comment_user.setImageDrawable(getResources()
								.getDrawable(R.drawable.user));
					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						img_comment_user.setImageDrawable(getResources()
								.getDrawable(R.drawable.user));
					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {

					}
				});

	}

	IListComment iListComment = new IListComment() {

		@Override
		public void TaskCompleted(List<Comment> listcomment) {
			if (listcomment != null) {
				listComment = listcomment;
				commListAdapter = new CommentListAdapter(listComment,
						CommentActivity.this);

				lv_CommentList.setAdapter(commListAdapter);

			}
		}

		@Override
		public void TaskCompleted(Comment listcomment) {
			// TODO Auto-generated method stub

		}
	};

	private void inilialisewidgets() {

		et_comment = (EditText) findViewById(R.id.et_comment);
		btnComment = (Button) findViewById(R.id.btnComment);
		lv_CommentList = (ListView) findViewById(R.id.lv_CommentList);

		img_comment_user = (SquareImageView) findViewById(R.id.img_comment_user);

		txt_comment_username = (TextView) findViewById(R.id.txt_comment_username);
		txt_comment_contest_title = (TextView) findViewById(R.id.txt_comment_contest_title);
		txt_comment_desc = (TextView) findViewById(R.id.txt_comment_desc);

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnComment) {
			if (et_comment.getText().length() != 0) {
				new AsyncInsertComment(String.valueOf(photoid), et_comment
						.getText().toString(), Common.USER.getiUserID(),
						new IListComment() {

							@Override
							public void TaskCompleted(Comment listcomment) {
								Comment comment = listcomment;
								addcommnet(comment);
								et_comment.setText("");
							}

							@Override
							public void TaskCompleted(List<Comment> listcomment) {

							}
						}).execute();
			} else {

				Toast.makeText(CommentActivity.this, "comment empty",
						Toast.LENGTH_LONG).show();
			}
		}

	}

	private void addcommnet(Comment comment) {

		if (comment != null) {
			comment.setiUserID(Common.USER.getiUserID());
			comment.setStrUserName(Common.USER.getUserName());
			comment.setStrUserimage(Common.USER.getStrUser_image());
		}
		if (listComment == null) {
			listComment = new ArrayList<Comment>();
		}
		listComment.add(0, comment);

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				commListAdapter.notifyDataSetChanged();
			}
		});

	}
}
