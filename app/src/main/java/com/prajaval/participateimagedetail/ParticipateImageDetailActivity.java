package com.prajaval.participateimagedetail;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.app_dev.framwordk_classes.BaseActivity;
import com.prajaval.core.Comment;
import com.prajaval.core.ParticipateItem;
import com.prajaval.core.User;
import com.prajaval.profile_activity.GuestProfileActivity;
import com.prajaval.twittershare.TwitterWebView;
import com.prajaval.utility.RateTextCircularProgressBar;
import com.prajaval.utility.SquareImageView;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Feed;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnPublishListener;
import common.prajaval.common.Common;

public class ParticipateImageDetailActivity extends com.prajaval.BaseActivity implements
		OnClickListener {

	ParticipateItem participateItem;
	SquareImageView img_participate_entry, img_profile;
	TextView txt_participate_name, txt_title;
	TextView txt_participate_entry_title;
	DisplayImageOptions options;
	DisplayImageOptions optionuser;
	Button btnVote, btnShare;
	ProgressBar participate_progressbar;
	EditText et_comment;

	private RateTextCircularProgressBar mRateTextCircularProgressBar;

	private SimpleFacebook mSimpleFacebook;

	ListView lv_CommentList;

	List<Comment> listcomment;

	CommentListAdapter commentListAdapter;

	final String VOTE_SUCCESSFUL = "1\n";

	boolean isVote = false;

	private int progress = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.participateimagedetail_activity);

		Intent intent = getIntent();
		participateItem = (ParticipateItem) intent
				.getSerializableExtra(Common.PARTICIPATE_ITEM);

		initialise_widgets();

		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.ARGB_8888)
				.displayer(new FadeInBitmapDisplayer(300)).build();

		optionuser = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.showImageForEmptyUri(
						getResources().getDrawable(R.drawable.user))
				.showImageOnFail(getResources().getDrawable(R.drawable.user))
				.cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.ARGB_8888)
				.displayer(new RoundedBitmapDisplayer(50)).build();

		btnVote.setOnClickListener(this);

		btnShare.setOnClickListener(this);
		img_profile.setOnClickListener(this);
		txt_participate_name.setOnClickListener(this);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		GetComment();
		initFacebook();

		et_comment.setOnTouchListener(etCommentClickListener);

		// bind();

	}

	@Override
	protected void onResume() {
		bind();
		super.onResume();
	}

	@Override
	protected void onRestart() {

		super.onRestart();
	}

	OnTouchListener etCommentClickListener = new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			final int DRAWABLE_LEFT = 0;
			final int DRAWABLE_TOP = 1;
			final int DRAWABLE_RIGHT = 2;
			final int DRAWABLE_BOTTOM = 3;

			et_comment.requestFocus();

			if (event.getAction() == MotionEvent.ACTION_UP) {
				if (event.getRawX() >= (et_comment.getRight() - et_comment
						.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds()
						.width())) {

					addComment();

					return true;
				}
			}
			return false;
		}
	};

	private void GetComment() {

		new AsyncGetComment(participateItem.getiparticiapteID(), iListComment)
				.execute();

	}

	IListComment iListComment = new IListComment() {

		@Override
		public void TaskCompleted(Comment _comment) {

		}

		@Override
		public void TaskCompleted(List<Comment> _listComment) {

			listcomment = _listComment;

			adjustListViewHeight(listcomment);

		}
	};

	void adjustListViewHeight(List<Comment> list) {
		int count = 0;
		if (list == null) {
			count = 0;
		} else {
			count = list.size();
		}
		lv_CommentList.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, count * 150));

		commentListAdapter = new CommentListAdapter(list,
				ParticipateImageDetailActivity.this);

		lv_CommentList.setAdapter(commentListAdapter);
		InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(et_comment.getWindowToken(), 0);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		outState.putSerializable(Common.PARTICIPATE_ITEM, participateItem);
		outState.putSerializable(Common.USER_VALUE, Common.USER);

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		try {
			participateItem = (ParticipateItem) savedInstanceState
					.getSerializable(Common.PARTICIPATE_ITEM);
			Common.USER = (User) savedInstanceState
					.getSerializable(Common.USER_VALUE);
			bind();
		} catch (Exception e) {

		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	private void bind() {

		if (participateItem != null) {

			try {

				mRateTextCircularProgressBar.setMax(100);
				mRateTextCircularProgressBar.clearAnimation();
				mRateTextCircularProgressBar.getCircularProgressBar()
						.setCircleWidth(2);

				getActionBar().setTitle(participateItem.getStrTitle());

				img_profile.setTag(participateItem.getiUserID());
				txt_participate_name.setTag(participateItem.getiUserID());

				ImageLoader.getInstance().displayImage(
						participateItem.getStrImage(), img_participate_entry,
						options, new SimpleImageLoadingListener() {

							@Override
							public void onLoadingStarted(String imageUri,
									View view) {

							}

							@Override
							public void onLoadingFailed(String imageUri,
									View view, FailReason failReason) {

								img_participate_entry
										.setImageDrawable(getResources()
												.getDrawable(
														R.drawable.img_not_found_thumb));

							}

							@Override
							public void onLoadingComplete(String imageUri,
									View view, Bitmap loadedImage) {

								img_participate_entry
										.setImageBitmap(loadedImage);
								mRateTextCircularProgressBar
										.setVisibility(View.INVISIBLE);
								// participate_progressbar
								// .setVisibility(View.GONE);
							}
						}, new ImageLoadingProgressListener() {

							@Override
							public void onProgressUpdate(String arg0,
									View arg1, int arg2, int arg3) {

								progress = ((int) (arg2 * 100) / arg3);

								mHandler.sendEmptyMessageDelayed(progress, 100);
							}
						});

				ImageLoader.getInstance().displayImage(
						participateItem.getStrUserImage(), img_profile,
						optionuser, new SimpleImageLoadingListener() {
							@Override
							public void onLoadingStarted(String imageUri,
									View view) {

							}

							@Override
							public void onLoadingFailed(String imageUri,
									View view, FailReason failReason) {

							}

							@Override
							public void onLoadingComplete(String imageUri,
									View view, Bitmap loadedImage) {

							}
						});

				txt_participate_name.setText(participateItem.getStrUsername());

				if (participateItem.getStrDescription().length() > 0) {
					txt_participate_entry_title.setText(participateItem
							.getStrDescription());
				} else {
					txt_participate_entry_title.setVisibility(View.GONE);
				}
				txt_title.setText(participateItem.getStrTitle());

				if (participateItem.getiVote() != 0) {
					btnVote.setBackgroundResource(R.drawable.button_voted_red);
					btnVote.setTextColor(ParticipateImageDetailActivity.this
							.getResources().getColor(R.color.white));
					btnVote.setText("Voted");
					isVote = true;

				} else {
					btnVote.setBackgroundResource(R.drawable.button_transparent);
					btnVote.setTextColor(ParticipateImageDetailActivity.this
							.getResources().getColor(R.color.primaryColor));
					btnVote.setText("Vote");
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			int me = msg.what;
			mRateTextCircularProgressBar.setProgress(msg.what);

			if (progress >= 100) {
				img_participate_entry.setVisibility(View.VISIBLE);
			}
			mHandler.sendEmptyMessageDelayed(progress, 100);
			super.handleMessage(msg);
		}

	};

	private void initialise_widgets() {
		img_participate_entry = (SquareImageView) findViewById(R.id.img_participate_entry);

		img_profile = (SquareImageView) findViewById(R.id.img_profile);
		txt_participate_name = (TextView) findViewById(R.id.txt_participate_name);
		txt_participate_entry_title = (TextView) findViewById(R.id.txt_participate_entry_title);
		txt_title = (TextView) findViewById(R.id.txt_title);
		btnVote = (Button) findViewById(R.id.btnVote);
		// participate_progressbar = (ProgressBar)
		// findViewById(R.id.participate_progressbar);

		mRateTextCircularProgressBar = (RateTextCircularProgressBar) findViewById(R.id.participate_progressbar);
		btnShare = (Button) findViewById(R.id.btnShare);

		lv_CommentList = (ListView) findViewById(R.id.lv_CommentList);

		et_comment = (EditText) findViewById(R.id.et_comment);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btnVote:
			vote_entry();
			break;

		case R.id.btnShare:
			pop_up_share(v);
			break;
		case R.id.img_profile:
			int tag = (int) img_profile.getTag();
			redirect_guest_profile(tag);
			break;
		case R.id.txt_participate_name:
			int _tag = (int) txt_participate_name.getTag();
			redirect_guest_profile(_tag);

			break;
		default:
			break;
		}
	}

	void redirect_guest_profile(int value) {

		Intent intent = new Intent(ParticipateImageDetailActivity.this,
				GuestProfileActivity.class);
		intent.putExtra(Common.USER_ID, value);
		startActivity(intent);
	}

	private void addComment() {

		if (et_comment.getText().length() != 0) {

			if (!et_comment.getText().equals("")) {

				new AsyncInsertComment(String.valueOf(participateItem
						.getiparticiapteID()), et_comment.getText().toString(),
						Common.USER.getiUserID(), new IListComment() {

							@Override
							public void TaskCompleted(Comment listcomment) {
								Comment comment = listcomment;
								addcommentList(comment);
								et_comment.setText("");
							}

							@Override
							public void TaskCompleted(List<Comment> listcomment) {

							}
						}).execute();
			} else {
				Toast.makeText(ParticipateImageDetailActivity.this,
						"Enter comment", Toast.LENGTH_LONG).show();
			}
		}

	}

	private void addcommentList(Comment comment) {
		try {

			if (comment != null) {
				comment.setiUserID(Common.USER.getiUserID());
				comment.setStrUserName(Common.USER.getUserName());
				comment.setStrUserimage(Common.USER.getStrUser_image());

			}
			if (listcomment == null) {
				listcomment = new ArrayList<Comment>();
			}
			listcomment.add(0, comment);

			adjustListViewHeight(listcomment);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void pop_up_share(View view) {
		try {

			final CharSequence[] items = { "facebook", "twitter", };

			AlertDialog.Builder builder = new Builder(
					ParticipateImageDetailActivity.this);
			builder.setCancelable(true);
			builder.setTitle("Share on");

			builder.setItems(items, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int item) {
					try {

						if (items[item].equals("facebook")) {

							share_facebook();

							dialog.dismiss();

						} else if (items[item].equals("twitter")) {

							send_tweet();
							dialog.dismiss();

						}
					} catch (Exception e) {
						e.toString();
					}

				}
			});
			builder.show();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void share_facebook() {
		try {

			mSimpleFacebook = SimpleFacebook.getInstance(this);
			mSimpleFacebook.login(onLoginListener);
//			if (mSimpleFacebook != null) {
//				public_facebookfeed();
//			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);

	}

	OnLoginListener onLoginListener = new OnLoginListener() {

		@Override
		public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
			shareFacebokFeed();
		}

		@Override
		public void onCancel() {

		}

		@Override
		public void onFail(String reason) {
			Toast.makeText(getBaseContext(), reason, Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onException(Throwable throwable) {
			Toast.makeText(getBaseContext(), throwable.getMessage(),
					Toast.LENGTH_SHORT).show();
		}


	};

	private void shareFacebokFeed() {
//		final Feed feed = new Feed.Builder()
//				.setMessage("Contest Entry")
//				.setName(participateItem.getUserName())
//				.setCaption("Halla Photo & Video Contest - Android app")
//				.setDescription(participateItem.getStrTitle())
//				.setPicture(participateItem.getStrImage())
//				.setLink(
//						"http://halla.in/contest_video.php?id="
//								+ participateItem.getiparticiapteID())
//				.build();

		if (ShareDialog.canShow(ShareLinkContent.class)) {
			ShareLinkContent linkContent = new ShareLinkContent.Builder()
					.setContentTitle("Contest Entry")
					.setImageUrl(Uri.parse(participateItem.getStrImage()))
					.setContentDescription(
							"Halla Photo & Video Contest - Android app "+participateItem.getStrTitle())
					.setContentUrl(Uri.parse("http://developers.facebook.com/android"))
					.build();

			shareDialog.show(linkContent);
		}
	}

	private CallbackManager callbackManager;
	private ShareDialog shareDialog;
	private void initFacebook(){
		FacebookSdk.sdkInitialize(getApplicationContext());
		callbackManager = CallbackManager.Factory.create();
		shareDialog = new ShareDialog(this);
		// this part is optional
		shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
			@Override
			public void onSuccess(Sharer.Result result) {

			}

			@Override
			public void onCancel() {

			}

			@Override
			public void onError(FacebookException error) {

			}
		});
	}

	private void public_facebookfeed() {
		try {

			final Feed feed = new Feed.Builder()
					.setMessage("Contest Entry")
					.setName(participateItem.getStrUsername())
					.setCaption("Halla Photo & Video Contest - Android app")
					.setDescription(participateItem.getStrTitle())
					.setPicture(participateItem.getStrImage())
					.setLink(
							"http://halla.in/contest_video.php?id="
									+ participateItem.getiparticiapteID())
					.build();

			mSimpleFacebook.publish(feed, new OnPublishListener() {

				@Override
				public void onException(Throwable throwable) {

				}

				@Override
				public void onFail(String reason) {
					Toast.makeText(ParticipateImageDetailActivity.this, reason,
							Toast.LENGTH_SHORT).show();
				}

				@Override
				public void onThinking() {

				}

				@Override
				public void onComplete(String response) {
					Toast.makeText(ParticipateImageDetailActivity.this,
							"Successfully shared..", Toast.LENGTH_SHORT).show();
				}
			});

		} catch (Exception e) {

		}
	}

	private void send_tweet() {
		Intent intent = new Intent(ParticipateImageDetailActivity.this,
				TwitterWebView.class);
		intent.putExtra(Common.PARTICIPATE_ITEM, participateItem);
		startActivity(intent);

	}

	private void vote_entry() {

		try {

			if (isVote) {
				isVote = false;
			} else {
				isVote = true;
			}

			new AsyncVote(ParticipateImageDetailActivity.this, participateItem,
					new IVoteSuccessful() {

						@Override
						public void TaskCpmplted(String success) {
							if (success.equals(VOTE_SUCCESSFUL)) {

								participateItem.setiVote(1);
								Common.PARTICIPATE = participateItem;

							} else {
								participateItem.setiVote(0);
								Common.PARTICIPATE = participateItem;
							}

						}
					}).execute();

			if (isVote) {
				btnVote.setBackgroundColor(ParticipateImageDetailActivity.this
						.getResources().getColor(R.color.entry_red));
				btnVote.setTextColor(ParticipateImageDetailActivity.this
						.getResources().getColor(R.color.white));
				btnVote.setText("Voted");
			} else {
				btnVote.setBackgroundResource(R.drawable.button_transparent);
				btnVote.setTextColor(ParticipateImageDetailActivity.this
						.getResources().getColor(R.color.bg_yellow));
				btnVote.setText("Vote");
			}

			Animation animVote = AnimationUtils.loadAnimation(
					ParticipateImageDetailActivity.this, R.anim.vote_animatiom);
			btnVote.startAnimation(animVote);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
