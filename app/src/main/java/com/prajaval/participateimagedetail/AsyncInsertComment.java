package com.prajaval.participateimagedetail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;

import com.prajaval.core.Comment;

public class AsyncInsertComment extends
		AsyncTask<Void, Void, com.prajaval.core.Comment> {

	private int UserID = 0;
	public final String INSERT_COMMENT = "http://halla.in/api/comments_insert1/";
	String PhotoID;
	String Comment;
	IListComment iListComment;

	public AsyncInsertComment(String photoid, String comment, int iUserID,
			IListComment iUploadSuccess) {
		Comment = comment;
		PhotoID = photoid;
		UserID = iUserID;
		this.iListComment = iUploadSuccess;
	}

	@Override
	protected Comment doInBackground(Void... params) {

		return insertComment(Comment, PhotoID);
	}

	@Override
	protected void onPostExecute(Comment result) {
		iListComment.TaskCompleted(result);
	}

	@SuppressWarnings("static-access")
	private Comment insertComment(String COMMENT, String PHOTOID) {

		com.prajaval.core.Comment _comment = null;
		// comment to server
		HttpPost httppost = new HttpPost(INSERT_COMMENT);

		MultipartEntity entity = new MultipartEntity(
				HttpMultipartMode.BROWSER_COMPATIBLE);
		HttpClient httpClient = new DefaultHttpClient();

		try {

			StringBody Comment = new StringBody(COMMENT);
			StringBody userID = new StringBody(String.valueOf(UserID));
			StringBody photoID = new StringBody(String.valueOf(PHOTOID));

			entity.addPart("description", Comment);
			entity.addPart("user_id", userID);
			entity.addPart("entry_id", photoID);

		} catch (UnsupportedEncodingException e1) {

			e1.printStackTrace();
		}
		httppost.setEntity(entity);
		String sResponce = "";
		try {
			HttpResponse sResponse = httpClient.execute(httppost);
			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(sResponse
						.getEntity().getContent(), "UTF-8"));
				StringBuffer sb = new StringBuffer("");
				String line = "";
				String NL = System.getProperty("line.separator");
				while ((line = in.readLine()) != null) {
					sb.append(line + NL);
				}
				in.close();
				sResponce = sb.toString();
				return _comment.Getcomment(sResponce);
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return _comment;

	}

}
