package com.prajaval.participateimagedetail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.os.AsyncTask;

import com.prajaval.core.ParticipateItem;

public class AsyncVote extends AsyncTask<Void, Void, String> {

	public final String VOTE = "http://halla.in/api/v1/vote";

	Context context;
	ParticipateItem item;
	IVoteSuccessful ivoteSuccessful;

	public AsyncVote(Context context, ParticipateItem params,
			IVoteSuccessful voteSuccessful) {
		this.context = context;
		item = params;
		ivoteSuccessful = voteSuccessful;
	}

	@Override
	protected String doInBackground(Void... params) {

		HttpPost httppost = new HttpPost(VOTE);
		MultipartEntity entity = new MultipartEntity(
				HttpMultipartMode.BROWSER_COMPATIBLE);
		HttpClient httpClient = new DefaultHttpClient();

		try {

			StringBody userID = new StringBody(
					String.valueOf(common.prajaval.common.Common.USER
							.getiUserID()));
			StringBody photoID = new StringBody(String.valueOf(item
					.getiparticiapteID()));

			entity.addPart("user_id", userID);
			entity.addPart("entry_id", photoID);
		} catch (UnsupportedEncodingException e1) {

			e1.printStackTrace();
		}
		httppost.setEntity(entity);

		try {
			HttpResponse sResponse = httpClient.execute(httppost);
			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(sResponse
						.getEntity().getContent(), "UTF-8"));
				StringBuffer sb = new StringBuffer("");
				String line = "";
				String NL = System.getProperty("line.separator");
				while ((line = in.readLine()) != null) {
					sb.append(line + NL);
				}
				in.close();
				return sb.toString();
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		return null;
	}

	@Override
	protected void onPostExecute(String result) {

		ivoteSuccessful.TaskCpmplted(result);
	}

}
