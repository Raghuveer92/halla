package com.prajaval.participateimagedetail;

import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.util.Log;

import com.json.classes.JSONArray;
import com.prajaval.core.Comment;
import common.prajaval.common.CommonURL;

public class AsyncGetComment extends
		AsyncTask<Void, Void, List<com.prajaval.core.Comment>> {

	int photoid;
	IListComment iListComment;
	public final String COMMENT = "http://halla.in/api/comments/";

	public AsyncGetComment(int photoid, IListComment ilistComment) {
		this.photoid = photoid;
		iListComment = ilistComment;
	}

	@Override
	protected List<com.prajaval.core.Comment> doInBackground(Void... params) {

		String URL = CommonURL.Execute(COMMENT + photoid, null, null);

		return GetCommentList(URL);
	}

	protected void onPostExecute(
			java.util.List<com.prajaval.core.Comment> result) {

		iListComment.TaskCompleted(result);
	};

	private List<Comment> GetCommentList(String json) {

		List<Comment> listComment = null;
		if (json != null) {

			try {
				JSONArray array = new JSONArray(json);

				for (int i = 0; i < array.length(); i++) {

					Comment comment = Comment
							.GetComment(array.getJSONObject(i));
					if (listComment == null) {
						listComment = new ArrayList<Comment>();
					}
					listComment.add(comment);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return listComment;
	}

}
