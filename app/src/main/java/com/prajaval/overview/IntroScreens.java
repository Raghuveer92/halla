package com.prajaval.overview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.prajaval.app_dev.RegisterActivity;
import com.prajaval.facebooklogin.FaceBookLogin;
import com.viewpagerindicator.CirclePageIndicator;

public class IntroScreens extends FragmentActivity {

	IntroFragmentAdapter mAdapter;
	ViewPager mPager;
	CirclePageIndicator mIndicator;
	TextView txtSkip;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.introscreens_activity);

		mAdapter = new IntroFragmentAdapter(getSupportFragmentManager());
		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);

		mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(mPager);

		final float density = getResources().getDisplayMetrics().density;
		// indicator.setBackgroundColor(0xFFCCCCCC);
		mIndicator.setRadius(3 * density);
		// mIndicator.setPageColor(0xFF888888);
		mIndicator.setFillColor(0x88000000);
		mIndicator.setStrokeColor(0xFF000000);
		mIndicator.setStrokeWidth(1 * density);

		txtSkip = (TextView) findViewById(R.id.txtSkip);
		txtSkip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(IntroScreens.this,
						RegisterActivity.class);
				IntroScreens.this.finish();
				startActivity(intent);
			}
		});

	}
}
