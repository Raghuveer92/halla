package com.prajaval.overview;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.achieveee.hallaexpress.R;
import com.viewpagerindicator.IconPagerAdapter;

public class IntroFragmentAdapter extends FragmentPagerAdapter implements
        IconPagerAdapter {

    protected static final int[] ICONS = new int[]{R.drawable.tutorial_1,
            R.drawable.intoscreen_two, R.drawable.intoscreen_three,
            R.drawable.intoscreen_four, R.drawable.intoscreen_five};

    private String[] offerDesc = {"See all Open Contest to Play",
            "Get more details about \neach contest by tapping on it",
            "See all the awesome entries \nby people like you",
            "Upload your own entry easily \nfrom your phone itself",
            "All your pictures are stored \nneatly in your own profile"};

    private static String[] offerSteps = {"Step 1", "Step 2", "Step 3",
            "Step 4", "Step 5"};

    private int mCount = offerSteps.length;

    public IntroFragmentAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public int getIconResId(int index) {
        return ICONS[index % offerSteps.length];
    }

    @Override
    public Fragment getItem(int i) {

        return IntroScreenFragment.newInstance(ICONS[i], offerDesc[i],
                offerSteps[i]);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return IntroFragmentAdapter.offerSteps[position % offerSteps.length];
    }

    @Override
    public int getCount() {

        return mCount;
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }

}
