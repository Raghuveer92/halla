package com.prajaval.overview;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;

public class IntroScreenFragment extends Fragment {

	private final String KEY_CONTENT = "DRAWABLE";
	private int mContent;
	private String desc;
	private String step;

	public static IntroScreenFragment newInstance(int drawable, String desc,
			String step) {

		IntroScreenFragment fragment = new IntroScreenFragment();
		fragment.mContent = drawable;
		fragment.step = step;
		fragment.desc = desc;

		return fragment;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		if ((savedInstanceState != null)
				&& savedInstanceState.containsKey(KEY_CONTENT)) {
			mContent = Integer.parseInt(savedInstanceState
					.getString(KEY_CONTENT));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.introscreen_fragment,
				container, false);
		ImageView image = (ImageView) rootView.findViewById(R.id.image);
		TextView text = (TextView) rootView.findViewById(R.id.text);
		text.setText(desc);
		image.setImageResource(mContent);

		return rootView;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_CONTENT, mContent);
	}
}
