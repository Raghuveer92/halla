package com.prajaval.sponsorurl;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;

@SuppressLint("SetJavaScriptEnabled")
public class SponsorWebView extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.twittershare_activity);
		WebView webView = (WebView) findViewById(R.id.webView);

		Intent intent = getIntent();
		String sponsorurl = (String) intent.getSerializableExtra(getResources()
				.getString(R.string.SPONSORS));
		webView.getSettings().setJavaScriptEnabled(true);

		webView.setWebViewClient(new MyWebViewClient());
		webView.loadUrl(sponsorurl);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		getActionBar().setTitle(getResources().getString(R.string.SPONSORS));

	}

	class MyWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			Toast.makeText(SponsorWebView.this, "Tweet successfully.",
					Toast.LENGTH_SHORT).show();
			finish();
			return true;

		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}
}
