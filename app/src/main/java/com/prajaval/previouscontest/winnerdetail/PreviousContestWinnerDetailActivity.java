package com.prajaval.previouscontest.winnerdetail;

import java.util.List;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.core.Comment;
import com.prajaval.core.ContestWinnerUser;
import com.prajaval.core.User;
import com.prajaval.participateimagedetail.AsyncGetComment;
import com.prajaval.participateimagedetail.CommentListAdapter;
import com.prajaval.participateimagedetail.IListComment;
import com.prajaval.utility.RateTextCircularProgressBar;
import com.prajaval.utility.SquareImageView;
import common.prajaval.common.Common;

public class PreviousContestWinnerDetailActivity extends Activity {

	SquareImageView img_winner_profile, img_winner_entry, img_winner;
	TextView txt_winner_name, txt_winner_entrytitle, txt_winner_entry_desc,
			txt_tempComment;

	DisplayImageOptions options;
	DisplayImageOptions optionuser;

	ListView lv_prevCommentList;
	ContestWinnerUser contestWinnerUser;
	private int progress = 0;
	CommentListAdapter commentListAdapter;
	private RateTextCircularProgressBar mRateTextCircularProgressBar;

	final String YES = "yes";
	final String WINNER = "WINNER";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.previouscontestwinnerdetail_activity);
		initialise_widget();
		Intent intent = getIntent();
		contestWinnerUser = (ContestWinnerUser) intent
				.getSerializableExtra(Common.CONTESTWINNER_USER);

		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.ARGB_8888)
				.displayer(new FadeInBitmapDisplayer(300)).build();

		optionuser = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.showImageForEmptyUri(
						getResources().getDrawable(R.drawable.user))
				.showImageOnFail(getResources().getDrawable(R.drawable.user))
				.cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new RoundedBitmapDisplayer(50)).build();

		// bind_data(contestWinnerUser);

		GetComment();

		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	private void GetComment() {

		new AsyncGetComment(contestWinnerUser.getiContestID(), iListComment)
				.execute();

	}

	IListComment iListComment = new IListComment() {

		@Override
		public void TaskCompleted(Comment _comment) {

		}

		@Override
		public void TaskCompleted(List<Comment> listcomment) {
			if (listcomment != null) {
				if (listcomment.size() > 0) {

					commentListAdapter = new CommentListAdapter(listcomment,
							PreviousContestWinnerDetailActivity.this);

					lv_prevCommentList.setAdapter(commentListAdapter);

					lv_prevCommentList
							.setLayoutParams(new LinearLayout.LayoutParams(
									LayoutParams.FILL_PARENT,
									commentListAdapter.getCount() * 150));
					txt_tempComment.setVisibility(View.VISIBLE);
					txt_tempComment.setText("comments :");
				} else {
					txt_tempComment.setVisibility(View.GONE);
				}
			} else {
				txt_tempComment.setVisibility(View.GONE);
			}
		}
	};

	@Override
	protected void onResume() {
		bind_data(contestWinnerUser);
		super.onResume();
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putSerializable(Common.USER_VALUE, Common.USER);
		outState.putSerializable(Common.CONTESTWINNER_USER, contestWinnerUser);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		try {
			contestWinnerUser = (ContestWinnerUser) savedInstanceState
					.getSerializable(Common.CONTESTWINNER_USER);

			Common.USER = (User) savedInstanceState
					.getSerializable(Common.USER_VALUE);

		} catch (Exception e) {

		}

	}

	private void bind_data(ContestWinnerUser contest) {

		try {

			mRateTextCircularProgressBar.setMax(100);
			mRateTextCircularProgressBar.clearAnimation();
			mRateTextCircularProgressBar.getCircularProgressBar()
					.setCircleWidth(2);

			getActionBar().setTitle(contest.getStrTitle());
			txt_winner_name.setText(contest.getStrUsername());
			txt_winner_entry_desc.setText(contest.getStrDesc());
			txt_winner_entrytitle.setText(contest.getStrTitle());

			ImageLoader.getInstance().displayImage(contest.getStrImage(),
					img_winner_entry, options,
					new SimpleImageLoadingListener() {

						@Override
						public void onLoadingStarted(String imageUri, View view) {

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {

							img_winner_entry
									.setImageDrawable(getResources()
											.getDrawable(
													R.drawable.img_not_found_thumb));

						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {

							img_winner_entry.setImageBitmap(loadedImage);
							mRateTextCircularProgressBar
									.setVisibility(View.INVISIBLE);

						}
					}, new ImageLoadingProgressListener() {

						@Override
						public void onProgressUpdate(String arg0, View arg1,
								int arg2, int arg3) {

							progress = ((int) (arg2 * 100) / arg3);

							mHandler.sendEmptyMessageDelayed(progress, 100);
						}
					});

			ImageLoader.getInstance().displayImage(contest.getUserImage(),
					img_winner_profile, optionuser,
					new SimpleImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {

						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {

						}
					});

			if (contest.getStrWinner() != null) {
				if (contest.getStrWinner().equals(YES)) {
					img_winner.setImageDrawable(getResources().getDrawable(
							R.drawable.winner));
				} else {
					img_winner.setVisibility(View.GONE);
				}
			} else {
				img_winner.setVisibility(View.GONE);
			}

		} catch (Exception e) {

		}

	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			int me = msg.what;
			mRateTextCircularProgressBar.setProgress(msg.what);

			if (progress >= 100) {
				img_winner_entry.setVisibility(View.VISIBLE);
			}
			mHandler.sendEmptyMessageDelayed(progress, 100);
			super.handleMessage(msg);
		}

	};

	private void initialise_widget() {

		img_winner = (SquareImageView) findViewById(R.id.img_winner);
		txt_winner_name = (TextView) findViewById(R.id.txt_winner_name);
		txt_winner_entry_desc = (TextView) findViewById(R.id.txt_winner_entry_desc);
		txt_winner_entrytitle = (TextView) findViewById(R.id.txt_winner_entrytitle);
		txt_tempComment = (TextView) findViewById(R.id.txt_tempComment);
		img_winner_profile = (SquareImageView) findViewById(R.id.img_winner_profile);
		mRateTextCircularProgressBar = (RateTextCircularProgressBar) findViewById(R.id.participate_progressbar);

		img_winner_entry = (SquareImageView) findViewById(R.id.img_winner_entry);

		lv_prevCommentList = (ListView) findViewById(R.id.lv_prevCommentList);
	}
}
