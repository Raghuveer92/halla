package com.prajaval.previouscontest.gridviews_entries;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.achieveee.hallaexpress.R;
import com.prajaval.core.Contest;
import com.prajaval.core.ContestWinnerUser;
import com.prajaval.core.User;
import com.prajaval.previouscontest.winnerdetail.PreviousContestWinnerDetailActivity;
import com.prajaval.youtubevideo.YouTubeVideo;

import common.prajaval.common.Common;

public class PreviousContestEntriesActivity extends Activity implements
		OnItemClickListener {

	Contest contest;
	GridView gv_prevcontest_winnerlist;
	PreviousContestEntriesAdapter previousContestEntriesAdapter;
	List<ContestWinnerUser> llContestWinnerUsers;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.previouscontestentries_activity);
		initialise_widgets();

		Intent intent = getIntent();
		contest = (Contest) intent
				.getSerializableExtra(Common.SELECTED_CONTEST);

		getActionBar().setTitle(contest.getStr_ContestTitle());
		getActionBar().setDisplayHomeAsUpEnabled(true);
		callcontest();

	}

	@Override
	protected void onRestart() {

		super.onRestart();
	}

	@Override
	protected void onResume() {

		super.onResume();
	}

	@Override
	protected void onPause() {

		super.onPause();
	}

	private void callcontest() {

		PreviousContestEntryController contestEntryController = new PreviousContestEntryController(
				PreviousContestEntriesActivity.this, contest.getiContestId(),
				iListParticipateWinner);

		contestEntryController.execute();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		outState.putSerializable(Common.SELECTED_CONTEST, contest);
		outState.putSerializable(Common.USER_VALUE, Common.USER);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		try {
			contest = (Contest) savedInstanceState
					.getSerializable(Common.SELECTED_CONTEST);
			Common.USER = (User) savedInstanceState
					.getSerializable(Common.USER_VALUE);
			 callcontest();
		} catch (Exception e) {

		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	private void initialise_widgets() {
		gv_prevcontest_winnerlist = (GridView) findViewById(R.id.gv_prevcontest_winnerlist);
	}

	IListParticipateWinner iListParticipateWinner = new IListParticipateWinner() {

		@Override
		public void TaskCompleted(ContestWinnerUser _contestWinnerUser) {

		}

		@Override
		public void TaskCompleted(List<ContestWinnerUser> listContestWinnerUser) {
			try {

				if (listContestWinnerUser != null) {

					if (llContestWinnerUsers == null) {
						llContestWinnerUsers = new ArrayList<ContestWinnerUser>();
					}
					if (llContestWinnerUsers.size() == 0) {
						llContestWinnerUsers = listContestWinnerUser;
						bind_data();
					} else {
						for (int i = 0; i < listContestWinnerUser.size(); i++) {
							llContestWinnerUsers.add(listContestWinnerUser
									.get(i));
						}

						runOnUiThread(new Runnable() {
							public void run() {
								previousContestEntriesAdapter
										.notifyDataSetChanged();
							}
						});
					}

				}

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	};

	void bind_data() {
		previousContestEntriesAdapter = new PreviousContestEntriesAdapter(
				getBaseContext(), llContestWinnerUsers);

		gv_prevcontest_winnerlist.setAdapter(previousContestEntriesAdapter);

		gv_prevcontest_winnerlist.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long arg3) {
		try {

			ContestWinnerUser winnerUser = (ContestWinnerUser) previousContestEntriesAdapter
					.getItem(position);

			if (winnerUser.getStrType().equals("video")) {
				Intent intent = new Intent(PreviousContestEntriesActivity.this,
						YouTubeVideo.class);
				intent.putExtra(Common.VIDEO_ID, winnerUser.getStrY_link());
				startActivity(intent);
			} else {
				Intent intent = new Intent(PreviousContestEntriesActivity.this,
						PreviousContestWinnerDetailActivity.class);
				intent.putExtra(Common.CONTESTWINNER_USER, winnerUser);
				startActivity(intent);
			}

		} catch (Exception e) {

		}
	}
}
