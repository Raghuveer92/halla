package com.prajaval.previouscontest.gridviews_entries;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.json.classes.JSONArray;
import com.prajaval.core.Contest;
import com.prajaval.core.ContestWinnerUser;
import com.prajaval.fragment.previouscontest.IPreviousContestList;
import com.prajaval.utility.CustomProgressDialog;

import common.prajaval.common.CommonURL;

public class PreviousContestEntryController extends
		AsyncTask<Void, Void, List<ContestWinnerUser>> {

	Context context;
	int ContestID;
	IListParticipateWinner iListParticipateWinner;

	public String PREV_CONTEST_WINNER = "http://halla.in/api/prev_contests/";

	ProgressDialog dialog;

	public PreviousContestEntryController(Context con, int contestID,
			IListParticipateWinner params) {

		this.context = con;
		ContestID = contestID;
		iListParticipateWinner = params;
		
	}

	@Override
	protected void onPreExecute() {
		dialog = CustomProgressDialog.ctor(context);
		dialog.show();
	}

	@Override
	protected List<ContestWinnerUser> doInBackground(Void... params) {

		PREV_CONTEST_WINNER = PREV_CONTEST_WINNER + ContestID;
		String json = CommonURL.Execute(PREV_CONTEST_WINNER, null, null);

		return GetContestWinner(json);
	}

	@Override
	protected void onPostExecute(List<ContestWinnerUser> result) {

		super.onPostExecute(result);
		iListParticipateWinner.TaskCompleted(result);
		if (dialog != null) {
			dialog.hide();
		}
	}

	private List<ContestWinnerUser> GetContestWinner(String json) {

		List<ContestWinnerUser> list = new ArrayList<ContestWinnerUser>();
		try {

			JSONArray jsonArray = new JSONArray(json);
			for (int i = 0; i < jsonArray.length(); i++) {
				ContestWinnerUser winnerUser = ContestWinnerUser
						.GetWinnerUser(jsonArray.getJSONObject(i));

				list.add(winnerUser);
			}
		} catch (Exception e) {

		}
		return list;
	}

}
