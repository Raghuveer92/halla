package com.prajaval.previouscontest.gridviews_entries;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.achieveee.hallaexpress.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.prajaval.core.ContestWinnerUser;
import com.prajaval.utility.SquareImageView;

public class PreviousContestEntriesAdapter extends BaseAdapter {

	List<ContestWinnerUser> listContestWinnerUsers;
	Context context;
	DisplayImageOptions options;
	ViewHolder viewHolder = null;
	final String YES = "yes";

	public PreviousContestEntriesAdapter(Context context,
			List<ContestWinnerUser> list) {
		listContestWinnerUsers = list;
		this.context = context;

		options = new DisplayImageOptions.Builder()

		.resetViewBeforeLoading(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.ARGB_8888).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(5)).build();

	}

	@Override
	public int getCount() {

		return listContestWinnerUsers.size();
	}

	@Override
	public Object getItem(int position) {

		return listContestWinnerUsers.get(position);
	}

	@Override
	public long getItemId(int position) {

		return 0;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.previouscontestentry_item,
					null);
			viewHolder = new ViewHolder();

			viewHolder.img_prevcontest_winner = (SquareImageView) convertView
					.findViewById(R.id.img_prevcontest_winner);
			viewHolder.txt_prevcontest_contesttitle = (TextView) convertView
					.findViewById(R.id.txt_prevcontest_contesttitle);

			viewHolder.txt_prevcontest_username = (TextView) convertView
					.findViewById(R.id.txt_prevcontest_username);
			viewHolder.img_prevcontest_trophywinner = (ImageView) convertView
					.findViewById(R.id.img_prevcontest_trophywinner);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		ContestWinnerUser item = listContestWinnerUsers.get(position);
		viewHolder.txt_prevcontest_contesttitle.setText(item.getStrTitle());
		viewHolder.txt_prevcontest_username.setText(item.getStrUsername());

		if (item.getStrWinner() != null) {
			if (item.getStrWinner().equals(YES)) {
				viewHolder.img_prevcontest_trophywinner
						.setVisibility(View.VISIBLE);
				viewHolder.img_prevcontest_trophywinner
						.setImageDrawable(context.getResources().getDrawable(
								R.drawable.winner));
			} else {
				viewHolder.img_prevcontest_trophywinner
						.setVisibility(View.GONE);
			}
		} else {
			viewHolder.img_prevcontest_trophywinner.setVisibility(View.GONE);
		}

		ImageLoader.getInstance().displayImage(item.getStrImage(),
				viewHolder.img_prevcontest_winner, options,
				new SimpleImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {
					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						viewHolder.img_prevcontest_winner
								.setImageDrawable(context.getResources()
										.getDrawable(
												R.drawable.img_not_found_thumb));
					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {

					}
				});
		return convertView;
	}

	private class ViewHolder {
		SquareImageView img_prevcontest_winner;
		TextView txt_prevcontest_contesttitle;
		TextView txt_prevcontest_username;
		ImageView img_prevcontest_trophywinner;
	}
}
