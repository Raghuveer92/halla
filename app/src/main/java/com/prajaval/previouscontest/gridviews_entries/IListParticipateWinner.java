package com.prajaval.previouscontest.gridviews_entries;

import java.util.List;

import com.prajaval.core.ContestWinnerUser;

public interface IListParticipateWinner {

	void TaskCompleted(List<ContestWinnerUser> listContestWinnerUser);

	void TaskCompleted(ContestWinnerUser _contestWinnerUser);
}
