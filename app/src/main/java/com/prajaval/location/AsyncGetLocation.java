package com.prajaval.location;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.prajaval.camera.CameraActivity;

import common.prajaval.common.Common;

public class AsyncGetLocation extends AsyncTask<Void, Void, Location> {

	GPSTracker gpsTracker;
	LocationListener locationListener;
	Context context;
	ILocation iLocation;
	Location location;
	ProgressDialog dialog;

	public AsyncGetLocation(Context context, LocationListener locationListener,
			ILocation iLocation) {
		this.context = context;
		this.locationListener = locationListener;
		this.iLocation = iLocation;

	}

	@Override
	protected void onPreExecute() {
		dialog = ProgressDialog.show(context, "", "wait..", true);
	}

	@Override
	protected Location doInBackground(Void... params) {
		gpsTracker = new GPSTracker(context, locationListener);

		return gpsTracker.getLocation();

	}

	@Override
	protected void onPostExecute(Location result) {
		iLocation.ITaskCompleted(result);
		if (dialog != null) {
			dialog.dismiss();
		}
	}
}
