package com.prajaval.video;

import java.io.File;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.achieveee.hallaexpress.R;
import com.prajaval.camera.IUploadSuccess;
import com.prajaval.core.Contest;
import common.prajaval.common.Common;

public class VideoActivity extends Activity {
	Contest contest;
	EditText et_videotitle, et_videodescription;
	Button btnVideoUpload;

	private Uri fileUri;
	String selectedPath = "";
	public static final int MEDIA_TYPE_VIDEO = 2;
	private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.video_activity);
		initialisewidgets();

		btnVideoUpload.setOnClickListener(btnVideoOnclickListener);
		Intent contestintent = getIntent();
		contest = (Contest) contestintent
				.getSerializableExtra(Common.SELECTED_CONTEST);

		Intent intent = new Intent(
				android.provider.MediaStore.ACTION_VIDEO_CAPTURE);

		// create a file to save the video
		fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);

		if (fileUri != null) {
			// set the image file name
			intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		}
		// set the image file name
		intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 15);

		// set the video image quality to high
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

		// start the Video Capture Intent
		startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
	}

	private void initialisewidgets() {
		et_videotitle = (EditText) findViewById(R.id.et_videotitle);
		et_videodescription = (EditText) findViewById(R.id.et_videodescription);

		btnVideoUpload = (Button) findViewById(R.id.btnVideoUpload);

	}

	OnClickListener btnVideoOnclickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (fileUri != null) {
				if (et_videotitle.getText().length() > 0) {

					if (!et_videotitle.getText().equals("")) {

						String title = et_videotitle.getText().toString();
						String desc = et_videodescription.getText().toString();

						new UploadVideotask(VideoActivity.this, fileUri, title,
								desc, Common.USER.getiUserID(),
								contest.getiContestId(), uploadSuccess)
								.execute();

					} else {

						Toast.makeText(VideoActivity.this, "Enter title",
								Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(VideoActivity.this, "Enter title",
							Toast.LENGTH_LONG).show();
				}
			} else {

				Toast.makeText(VideoActivity.this, "Video capture failed.",
						Toast.LENGTH_LONG).show();
				VideoActivity.this.finish();
			}

		}
	};

	IUploadSuccess uploadSuccess = new IUploadSuccess() {

		@Override
		public void UploadSuccessful(String result) {
			Toast.makeText(VideoActivity.this, "Video successfully uploaded",
					Toast.LENGTH_LONG).show();
			VideoActivity.this.finish();
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		// After camera screen this code will excuted

		if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {

			if (resultCode == RESULT_OK) {
				// Video captured and saved to fileUri specified in the Intent

				if (fileUri != null) {
					selectedPath = fileUri.getPath();

				}

			} else if (resultCode == RESULT_CANCELED) {

				// User cancelled the video capture
				Toast.makeText(this, "User cancelled the video capture.",
						Toast.LENGTH_LONG).show();

				VideoActivity.this.finish();

			} else {

				Toast.makeText(this, "Video capture failed.", Toast.LENGTH_LONG)
						.show();
				VideoActivity.this.finish();
			}

		}
	}

	/** Create a file Uri for saving an image or video */
	private Uri getOutputMediaFileUri(int type) {

		return Uri.fromFile(getOutputMediaFile(type));
	}

	/** Create a File for saving an image or video */
	private File getOutputMediaFile(int type) {

		// Check that the SDCard is mounted
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"Halla");

		// Create the storage directory(MyCameraVideo) if it does not exist
		if (!mediaStorageDir.exists()) {

			if (!mediaStorageDir.mkdirs()) {

				Toast.makeText(VideoActivity.this,
						"Failed to create directory Halla.", Toast.LENGTH_LONG)
						.show();

				Log.d("Halla", "Failed to create directory Halla.");
				return null;
			}
		}

		// Create a media file name

		// For unique file name appending current timeStamp with file name
		java.util.Date date = new java.util.Date();
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date
				.getTime());

		File mediaFile;

		if (type == MEDIA_TYPE_VIDEO) {

			// For unique video file name appending current timeStamp with file
			// name
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "Halla_" + timeStamp + ".mp4");

		} else {
			return null;
		}

		return mediaFile;
	}
}
