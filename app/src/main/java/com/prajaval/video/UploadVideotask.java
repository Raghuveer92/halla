package com.prajaval.video;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import com.prajaval.camera.IUploadSuccess;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import common.prajaval.common.CommonURL;

public class UploadVideotask extends AsyncTask<Void, Void, String> {

	String videotitle;
	String videodesc;
	int UserID;
	int ContestID;
	Uri fileUri;
	Context context;
	private String UPLOAD = "http://halla.in/api/upload";
	IUploadSuccess iUploadSuccess;

	public UploadVideotask(Context cont, Uri uri, String title, String desc,
			int userid, int ContestID, IUploadSuccess iUploadSuccess) {

		context = cont;
		videotitle = title;
		videodesc = desc;
		UserID = userid;
		fileUri = uri;
		this.ContestID = ContestID;
		this.iUploadSuccess = iUploadSuccess;
	}

	@Override
	protected void onPreExecute() {
		// dialog = ProgressDialog.show(UploadVideoActivity.this, "",
		// "Uploading file...", true);
	}

	@Override
	protected String doInBackground(Void... params) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(UPLOAD);
			try {
				MultipartEntity entity = new MultipartEntity(
						HttpMultipartMode.BROWSER_COMPATIBLE);
				StringBody name = new StringBody("TestVideo");
				StringBody sTitle = new StringBody(videotitle);
				StringBody sDesc = new StringBody(videodesc);
				StringBody sUserID = new StringBody(String.valueOf(UserID));
				StringBody sContestID = new StringBody(
						String.valueOf(ContestID));
				entity.addPart("HALLA_FILE",
						new FileBody(new File(fileUri.getPath())));
				entity.addPart("name", name);
				entity.addPart("title", sTitle);
				entity.addPart("desc", sDesc);
				entity.addPart("userid", sUserID);
				entity.addPart("contestid", sContestID);
				httpPost.setEntity(entity);

				try {
					HttpResponse response = httpClient.execute(httpPost);
					BufferedReader in = null;
					try {
						in = new BufferedReader(new InputStreamReader(response
								.getEntity().getContent(), "UTF-8"));
						StringBuffer sb = new StringBuffer("");
						String line = "";
						String NL = System.getProperty("line.separator");
						while ((line = in.readLine()) != null) {
							sb.append(line + NL);
						}
						in.close();
						return sb.toString();
					} finally {
						if (in != null) {
							try {
								in.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}

				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {

					e.printStackTrace();
				}

				return "fail";

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {

		}
		return null;
	}

	@Override
	protected void onPostExecute(String sResponse) {
		iUploadSuccess.UploadSuccessful(sResponse);
	}
}
