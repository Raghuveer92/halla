package com.prajaval.error;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class ToastError {

	public ToastError(Context context, String Messsage, String err) {
		Toast toast = new Toast(context);
		toast.setText(Messsage);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.show();
		// Toast.makeText(context, Messsage, Toast.LENGTH_SHORT).show();
	}
}
