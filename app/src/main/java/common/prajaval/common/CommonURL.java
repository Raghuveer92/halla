package common.prajaval.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;

public class CommonURL {

	public static String Execute(String URL, Map<String, String> lstHeaders,
			Context context) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(PercentEncode(URL));
		String result = null;

		if (lstHeaders != null) {
			if (lstHeaders.size() > 0) {

				Iterator<?> objIterator = lstHeaders.entrySet().iterator();
				while (objIterator.hasNext()) {
					Map.Entry obj = (Map.Entry) objIterator.next();
					httpget.addHeader(String.valueOf(obj.getKey()),
							String.valueOf(obj.getValue()));

				}
			}
		}

		HttpResponse responce;
		try {
			responce = httpClient.execute(httpget);
			int statuscode = responce.getStatusLine().getStatusCode();
			HttpEntity entity = responce.getEntity();
			InputStream instream = entity.getContent();
			result = convertStreamToString(instream);

			switch (statuscode) {

			case 200:
				return result;

			case 204:
			case 400:
			case 404:
			case 500:
			case 502:
			case 503:
				return result;
				/**
				 * case 204: return "204"; case 400: return "400"; case 404:
				 * return "404"; case 500: case 502: case 503: return "503";
				 */
			default:
				break;
			}

		} catch (Exception e) {
			e.toString();
		}
		httpClient.getConnectionManager().shutdown();
		return result;
	}

	public static String Execute1(String URL, Map<String, byte[]> lstHeaders,
			Context context) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(PercentEncode(URL));
		String result = null;

		if (lstHeaders != null) {
			if (lstHeaders.size() > 0) {

				Iterator<?> objIterator = lstHeaders.entrySet().iterator();
				while (objIterator.hasNext()) {

					Map.Entry obj = (Map.Entry) objIterator.next();
					httpget.addHeader(String.valueOf(obj.getKey()),
							String.valueOf(obj.getValue()));

				}
			}
		}

		HttpResponse responce;
		try {
			responce = httpClient.execute(httpget);
			int statuscode = responce.getStatusLine().getStatusCode();
			HttpEntity entity = responce.getEntity();
			InputStream instream = entity.getContent();
			result = convertStreamToString(instream);

			switch (statuscode) {

			case 200:
				return result;

			case 204:
			case 400:
			case 404:
			case 500:
			case 502:
			case 503:
				return result;
				/**
				 * case 204: return "204"; case 400: return "400"; case 404:
				 * return "404"; case 500: case 502: case 503: return "503";
				 */
			default:
				break;
			}

		} catch (Exception e) {
			e.toString();
		}
		httpClient.getConnectionManager().shutdown();
		return result;
	}

	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public static String PercentEncode(String urlstring) {
		StringBuffer encoded = new StringBuffer();
		for (int i = 0; i < urlstring.length(); i++) {
			char ch = urlstring.charAt(i);
			switch (ch) {
			case ' ':
				encoded.append("%20");
				break;
			default:
				encoded.append(ch);
				break;
			}
		}
		return encoded.toString();
	}
}
