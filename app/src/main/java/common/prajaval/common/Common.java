package common.prajaval.common;

import com.prajaval.core.ParticipateItem;
import com.prajaval.core.User;

import android.location.Location;

public class Common {

	public static final java.lang.String LOGIN_WITH_FACEBOOK = "login_with_facebook";
	public static final int GOOGLE_SIGHN_IN = 10;
	public static Location LOCATION;
	public static User USER;
	public static final String SELECTED_CONTEST = "selected_contest";
	public static final String SELECTED_LOCATION_CONTEST = "selected_location_contest";
	public static final String ENTRY = "entry";
	public static final String USER_VALUE = "user";
	public static final String MESSAGE = "message";
	public static final String PARTICIPATE_ITEM = "participate_item";
	public static final String PHOTO_ID = "photo_id";
	public static final String VIDEO_ID = "video_id";
	public static final String USER_ID = "user_id";
	public static final String CONTESTWINNER_USER = "contestwinneruser";
	public static final String CLIENT_ID = "d775ced654fd44da825947713da76525";
	public static final String CLIENT_SECRET = "2b6e3458aa9c4d2ba965c2384ff9db40";
	public static final String CALLBACK_URL = "redirect uri here";

	public static final String SENDERID = "780052740600";
	public static ParticipateItem PARTICIPATE;
	public static int REQUESTCODE_GOOGLE_SIGHN_IN=10;

	public static class Config {
		public static final boolean DEVELOPER_MODE = false;
	}

	public static class Extra {
		public static final String FRAGMENT_INDEX = "com.nostra13.example.universalimageloader.FRAGMENT_INDEX";
		public static final String IMAGE_POSITION = "com.nostra13.example.universalimageloader.IMAGE_POSITION";
	}
}
